## Channel

### Условие
Вам нужно реализовать класс `Channel`, предоставляющий функционал, похожий на небуферизированный канал в Go.

Канал позволяет многим источникам одновременно писать в один канал и многим получателям получать данные из канала
без необходимости синхронизации со стороны приемников/источников, т.е. предоставляемые каналом функции можно безопасно
вызывать из многих потоков.

Нужно реализовать следующие функции:

* `send(value)` - поток блокируется до тех пор, пока принимающая сторона не получит соответствующее `value` через `recv`.

* `recv()` - поток блокируется до тех пор, пока не появится какой-либо `send`, который можно принять.

* `close()` - закрыть канал. Если при этом есть какие-то ожидающие `send` потоки, то их значения должны быть доставлены,
но все последующие попытки `send` должны завершаться с исключением `ChannelClosedException`. Вызов `recv`, когда не
останется недоставленных значений тоже должен выбрасывать `ChannelClosedException`.
Если сделали вызов `recv` до закрытия канала, канал закрылся и доступных сообщений не осталось, тоже нужно выбрасывать
`ChannelClosedException`.

### Пример
```python
In [1]: from channel import Channel

In [2]: channel = Channel()

In [3]: def sender():
   ...:     channel.send(1)
   ...:

In [4]: def receiver():
   ...:     print(channel.recv())
   ...:

In [5]: from threading import Thread

In [6]: s = Thread(target=sender)

In [7]: s.start()

In [8]: r = Thread(target=receiver)

In [9]: r.start()
1

In [10]: s.join()

In [11]: r.join()

In [12]: channel.close()

In [13]: from channel import ChannelClosedException

In [14]: try:
    ...:     channel.send(1)
    ...: except ChannelClosedException:
    ...:     print("got closed")
    ...:
got closed

In [15]: try:
    ...:     channel.recv()
    ...: except ChannelClosedException:
    ...:     print("got closed")
    ...:
got closed

In [16]:
```
