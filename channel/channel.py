from typing import Any


class ChannelClosedException(Exception):
    pass


class Channel:
    """
    Channel class for exchanging values between threads.
    """
    def send(self, value: Any) -> None:
        """
        Blocks until the value is received by another thread through recv's call.
        :param value: value to send
        """
        pass

    def recv(self) -> Any:
        """
        Blocks until any value is available through send's call.
        :return value: some value from a send's call
        """
        pass

    def close(self) -> None:
        """
        Closes the channel.
        Value for any `send` before close's call should be delivered
        but all subsequent send attempts must raise ChannelClosedException.
        After all values were delivered recv should also throw ChannelClosedException.
        """
        pass
