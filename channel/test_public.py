from threading import Thread
from types import TracebackType
from typing import List, Optional, Type, Callable

from pytest import raises

from .channel import Channel, ChannelClosedException


class ThreadJoiner:
    def __init__(self) -> None:
        self._threads: List[Thread] = []

    def add_thread(self, thread: Thread) -> None:
        # Must do this to prevent program's hang on exit in case of thread's hang
        thread.daemon = True
        thread.start()
        self._threads.append(thread)

    def __enter__(self) -> 'ThreadJoiner':
        return self

    def __exit__(self, exc_type: Optional[Type[BaseException]], exc_val: Optional[BaseException],
                 exc_tb: Optional[TracebackType]) -> None:
        exception: Optional[BaseException] = None
        for thread in self._threads:
            try:
                thread.join(timeout=5)
                if thread.is_alive():
                    raise RuntimeError('One of threads did not finished in 5 seconds, probably it hanged up.')
            except BaseException as e:
                exception = e

        if exception is not None:
            raise exception


class ExceptionPreserveThread(Thread):
    def __init__(self, target: Callable[[], None]) -> None:
        super().__init__(target=target)
        self._exception: Optional[BaseException] = None

    def run(self) -> None:
        try:
            super().run()
        except BaseException as e:
            self._exception = e

    def join(self, timeout: Optional[float] = None) -> None:
        super().join(timeout=timeout)
        if self._exception:
            raise self._exception


def test_channel_simple() -> None:
    channel = Channel()

    def _sender() -> None:
        channel.send(1)

    def _receiver() -> None:
        value = channel.recv()
        assert value == 1

    with ThreadJoiner() as joiner:
        joiner.add_thread(ExceptionPreserveThread(target=_sender))
        joiner.add_thread(ExceptionPreserveThread(target=_receiver))

    channel.close()

    with raises(ChannelClosedException):
        channel.send(1)

    with raises(ChannelClosedException):
        channel.recv()
