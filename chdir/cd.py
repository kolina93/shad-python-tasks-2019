def chdir(current_path: str, command_path: str) -> str:
    """
    Emulates cd command from bash
    :param current_path: user's current directory
    :param command_path: argument of cd command
    :return: user's directory after cd's execution
    """
