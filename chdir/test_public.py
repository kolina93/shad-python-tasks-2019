import pytest

from .cd import chdir

CASES = [
    ('/src', 'bin/etc', '/src/bin/etc'),
    ('/src', '..', '/'),
    ('/src/bin', '/', '/'),
    ('/src/bin', '.././etc//', '/src/etc'),
    ('/', 'bin/../../../A', '/A'),
    ('/src/bin', '/usr/docs/../lib', '/usr/lib'),
    ('/bin', 'usr/../docs/..//../lib', '/lib')
]


@pytest.mark.parametrize(('current_path', 'command_path', 'result'), CASES)
def test_chdir(current_path: str, command_path: str, result: str) -> None:
    assert chdir(current_path, command_path) == result
