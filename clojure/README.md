# clojure

В первом домашнем задании вам предстоит реализовать интерпретатор для подмножества языка [Clojure](https://clojure.org).
Нужное подмножество языка включает в себя примитивные типы (числа, строки, символы), составные типы-контейнеры
(массивы, хэш-таблицы, множества), переменные и функции. Синтаксис того множества языка, которое нужно реализовать,
описан в отдельном [документе](syntax.md).

Выполнение интерпретатора проходит через следующие стадии:

1. **Парсинг**: программы читает вход как последовательность символов, токенизирует символы на лексемы яызка,
проводит верификацию относительно синтаксических правил языка и преобразует во внутреннее представление в виде дерева,
называемое *абстрактным синтаксическим деревом* или [AST](https://en.wikipedia.org/wiki/Abstract_syntax_tree).

2. **Вычисление**: программа рекурсивно обходит AST и вычисляет его в соответствии с правилами языка.

Рассмотрим пример

```clojure
((and (= 1 1) +) 1 2 3)
=> 6
```

Он преобразуется в следующее AST:
![AST](clojure_ast.png)

## Обработка ошибок

Ваш интерпретатор должен различать следующие ошибки:

1. Ошибки синтаксиса: программа не соответствует формальному синтаксиса (не хватает скобок, неправильное количестство
аргументов у особых форм `if`, `when` и т.д.).

2. Ошибки времени исполнения: неправильное кол-во аргументов в функции, неправильные типы аргументов, доступ к
неинициализированной переменной и т.д.


## Правила сдачи

Ваша реализация в файле `clojure.py` должна предоставлять следующие интерфейсы:

* запуск `python clojure.py script.clj` должен читать команды из файла `script.clj` и писать результат их интерпретации
в стандартный поток вывода;

* запуск `python clojure.py` должен читать команды языка из стандартного потока ввода и писать результат их
интерпретации в стандартный поток вывода;

* функцию `run` можно вызвать с аргументом `input_` (который будет файловым объектом), будет читать из него команды
языка и писать результаты их интерпретации в стандартный поток вывода. Эта команда может выбросить исключение
`ClojureError` (или его наследника) в случае ошибок.

На ревью будет приветствоваться, если запуск `python clojure.py` в случае, если стандартный ввод будет связан
с терминалом, будет выдавать некоторое приглашение (prompt) для ввода очередной команды (как это сделано, если
запустить, к примеру, `python` или `lein repl`).

За задание можно получить 30 баллов. Оцениваться будет только последняя посылка. Составляющие:

* 15 баллов - прохождение приватных тестов на сервере (может быть частичная оценка)
* 5 баллов - покрытие кода тестами (может быть частичная оценка)
* 10 баллов - прохождение ревью

В приватных тестах будет использоваться динамическая система оценивания. Т.е. каждый тест будет получать динамический
вес, в зависимости от количества человек, решения которых прошли этот тест. Веса будут подсчитываться следующим образом:

* если тест прошло больше половины посылок, то его вес будет равен 1;
* если тест прошло больше четверти посылок, не не более половины посылок, то его вес будет равен 2;
* если тест прошло больше одной восьмой посылок, не не более четверти посылок, то его вес будет равен 4;
* и так далее.

Пусть есть `m` тестов и вес теста c номером $`i`$ получился равным $`w_i`$ и решение прошло тесты $`i_1`$, ..., $`i_n`$.
Тогда балл за прохождение приватных тестов будет равен $`15 * \sum_{k=1}^{n} w_{i_k} / \sum_{k=1}^{m} w_k`$.

Покрытие кода будет меряться по тем тестам, что вы напишете в файл `test_public.py`, померять его локально можно
с помощью плагина `pytest-cov` к `pytest`. Поставить его можно вот так `pip install pytest-cov` и запустить
`pytest --cov=. --cov-report=html test_public.py`. Будет сгенерирован html отчет, в котором будут подсвечены зеленым
покрытые строки и красным непокрытые. Балл за него будет ставиться по формуле $`5 * k`$, где `k` - доля покрытых
строк в решении.

Посылка решения происходит обычным образом через комманду `python3 ../submit.py` в директории с задачей.
В этом домашнем задании, в отличие от семинаров, stderr упавших тестов в тестирующей системе можно посмотреть на
странице пайплайна посылки.

Текущий свой балл можно смотреть на [странице](https://2019.shad-python-minsk.org/tasks/Homework1).

Также есть возможность запустить референсное решение с помощью скрипта `clojure_ref.py`. Для ее работы нужно поставить
библиотеку `requests` и запустить вот так `python3 clojure_ref.py --script script.clj`, где `script.clj` - файл с
командами в языке `clojure`. Ее запуск произведет запрос на сервер и распечатает в стандартный вывод результат
интерпретации команд в референсном решении.

Несколько важных замечаний:

* Для того, чтобы порядок вывода для хэш-таблиц и множеств работал консистентно, нужно использовать классы
`collections.OrderedDict` для реализации хэш-таблицы и класс `OrderedSet` из библиотеки `ordered-set` (библиотеку нужно
поставить через `pip`) для реализации множества.

* Будьте внимательны к представлению объектов в стандартном выводе как результата вычисления выражения, и к
представлению объектов в результате его передачи в функцию `println`, эти представления могут не совпадать. Эти
представления должны быть такими же, как описано в примерах к требованиям синтакса, можно сравниваться с запуском
`clojure_ref.py`. В частности представления символов должно быть `symbol <symbol-name>`, а представления функций
`function <function-name>`.

* Функция `run` должна выбрасывать исключения в случаях распространенных ошибок (синтаксические ошибки, обращение
к неопределенным символам и функциям, неправильные аргументы или их количество в функции и т.д.). В тестах проверяется,
что эта функция выбрасывает исключение `ClojureError` или его наследника в распространенных случаях ошибок. Продумайте
иерархию исключений, которую можно сделать в этом случае.

* Обратите внимание, что в нашей реализации не нужно реализовывать контейнер список из `clojure`. Методы по типу `map`,
`reduce` и т.д., которые в обычном `clojure` возвращают список, должны в нашей реализации возвращать вектор.

* Также обратите внимание, что в обычном `clojure` символ строки является отдельным объектом `character`. В нашей
реализации этого типа нет и символ отдельной строки должен являться строкой, как и в `python`.


## Ревью

Для прохождения процесса ревью, вам нужно сделать следующее: откройте страницу вашего репозитория на gitlab и зайдите
на страницу merge requests (ссылка с левой стороны). Далее нажмите на "New merge request" и создайте merge request из
ветки submits/closure в ветку initial (выберите ветки и нажмите "Compare branches and continue"). В assignee выберите
kolina93. Далее нажмите "Submit merge request". И ждите, пока вам что-нибудь напишут :)
