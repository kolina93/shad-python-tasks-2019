import sys

from typing import IO


class ClojureError(Exception):
    """
    Base class for all errors of code's interpreting.
    """


def run(input_: IO[str] = sys.stdin) -> None:
    """
    Reads clojure statements from file-like object and prints results of their execution to stdout.
    :param input_: file-like object to read clojure code from.
    """


if __name__ == '__main__':
    run()
