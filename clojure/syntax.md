## Синтаксис языка, который нужно поддержать

Весь код на Clojure пишется в однородной манере. Clojure понимает 2 вида структур:

* Определение некоторого типа (числа, строки, векторы, хэш-таблицы и т.д.)

* Операции над базовыми типами и результатами других операций

Будем называть валидный код на Clojure *формой*. Clojure *вычисляет* форму, чтобы получить некоторое значение.
Ниже приведены примеры определения примитивов, которые являются базовыми формами:

```clojure
clojurepy=> 1
1
clojurepy=> "a string"
"a string"
clojurepy=> ["a" "vector" "of" "strings"]
["a" "vector" "of" "strings"]
clojurepy=>
```

Операции имеют следующие составляющие: *открывающая скобка*, *оператор*, *операнды*, *закрывающая скобка*.

```clojure
(operator operand1 operand2 ... operandn)
```

```clojure
clojurepy=> (+ 1 2 3)
6
clojurepy=> (str "It was the panda " "in the library " "with a dust buster")
"It was the panda in the library with a dust buster"
clojurepy=>
```

Оператор и операнды разделяются пробелами (clojure также поддерживает запятые как разделители, но это поддерживать не
нужно). Выражаения для оператора и операндов могут быть произвольной вложенности.

```clojure
(my-operator (my-operator-2 arg1
                   arg2)
         (other-operator arg-a
                     (foo-bar arg-x
                              arg-y
                              (+ arg-xx
                                 arg-yy
                                 arg-zz))
                     arg-b))
```

#### if

Базовый синтаксис выражения с `if`:

```clojure
(if boolean-form
  then-form
  optional-else-form)
```

`boolean-form` это форма, которая вычисляется в истинное или ложное значение. Примеры:

```clojure
clojurepy=> (if true
  "By Zeus's hammer!"
  "By Aquaman's trident!")
"By Zeus's hammer!"
clojurepy=> (if false
  "By Zeus's hammer!"
  "By Aquaman's trident!")
"By Aquaman's trident!"
clojurepy=>
```

Можно пропустить `else` ветку, тогда, если `boolean-form` вычислится в ложь, то результатом будет `nil`:

```clojure
clojurepy=> (if false
  "By Odin's Elbow!")
nil
clojurepy=>
```

#### do

Оператор `do` позволяет вычислить сразу несколько форм за один раз:

```clojure
clojurepy=> (if true
  (do (println "Success!")
      "By Zeus's hammer!")
  (do (println "Failure!")
      "By Aquaman's trident!"))
Success!
"By Zeus's hammer!"
clojurepy=>
```

Результатом всего выражения будет результат вычисления последней формы.

#### when

`when` является комбинацией `if` и `do`, только без `else` ветки. Ниже пример:

```clojure
clojurepy=> (when true
  (println "Success!")
  "abra cadabra")
Success!
"abra cadabra"
clojurepy=>
```

Если условие `boolean-form` оказалось ложным, то результатом формы с `when` будет `nil`.

#### nil, true, false, оператор сравнения, логические выражения

В сlojure используются `true` и `false` для представления истинности и ложности. `nil` означает *отсутствие значения*.
Есть специальный оператор `nil?` для определения отсутсвия значения:

```clojure
clojurepy=> (nil? 1)
false
clojurepy=> (nil? nil)
true
clojurepy=>
```

`nil` и `false` представляют логическую ложь, все остальные значения явлются логической истиной.

```clojure
clojurepy=> (if "bears eat beets"
  "bears beets Battlestar Galactica")
"bears beets Battlestar Galactica"
clojurepy=> (if nil
  "This won't be the result because nil is falsey"
  "nil is falsey")
"nil is falsey"
clojurepy=>
```

Оператором сравнения является `=`:

```clojure
clojurepy=> (= 1 1)
true
clojurepy=> (= nil nil)
true
clojurepy=> (= 1 2)
false
clojurepy=>
```

Оператор `or` возвращает первое истинное выражения или последнее значение:

```clojure
clojurepy=> (or false nil :large_I_mean_venti :why_cant_I_just_say_large)
:large_I_mean_venti
clojurepy=> (or (= 0 1) (= "yes" "no"))
false
clojurepy=> (or nil)
nil
clojurepy=>
```

Оператор `and` возвращает первое ложное выражение или последнее истинное:


```clojure
clojurepy=> (and :free_wifi :hot_coffee)
:hot_coffee
clojurepy=> (and :feelin_super_cool nil false)
nil
clojurepy=>
```

#### def

Чтобы определить переменную со значением, используют оператор `def`. Его общий синтаксис `def symbol-name (expression)`:

```clojure
clojurepy=> (def failed-protagonist-names
  ["Larry Potter" "Doreen the Explorer" "The Incredible Bulk"])
symbol <failed-protagonist-names>
clojurepy=> failed-protagonist-names
["Larry Potter" "Doreen the Explorer" "The Incredible Bulk"]
clojurepy=> (def x (+ 1 2))
symbol <x>
clojurepy=> x
3
clojurepy=>
```

В имени символа разрешено использовать `a-zA-Z0-9_*+!/.:-?=<>`, но не разрешается начинать с цифры или двоеточия.

### Структуры данных

Все структуры данных в clojure являются иммутабельными, т.е. их нельзя менять inplace, и все операции над ними
возвращают новые структуры данных.

```clojure
clojurepy=> (def v   [:a :b :c])
symbol <v>
clojurepy=> (conj v  :d)
[:a :b :c :d]
clojurepy=> v
[:a :b :c]
clojurepy=>
```

#### Числа

```clojure
clojurepy=> 93
93
clojurepy=> 1.2
1.2
clojurepy=>
```

Другие форматы чисел (шестнадцатеричный формат, рациональные дроби и т.д.) поддерживать не обязательно.

#### Строки

```clojure
clojurepy=> "Lord Voldemort"
"Lord Voldemort"
clojurepy=> "\"He who must not be named\""
"\"He who must not be named\""
clojurepy=> "\"Great cow of Moscow!\" - Hermes Conrad"
"\"Great cow of Moscow!\" - Hermes Conrad"
clojurepy=> (if true "a
b" "c
d")
"a\nb"
clojurepy=> "\'"
ClojureRuntimeError: Invalid escape sequence
```

Специальные символы эскейпятся с помощью бэкслеша: `\t`, `\n`, `\r`, `\"`, `\\`. В случае, если после бэкслеша идет другой символ, то в нашей реализации требуется бросать исключение. Строковый литерал может быть разбит на много строк, т.е. внутри него могут быть переводы строки. Поддерживать синтаксис определения регулярных выражений (т.е. с решеткой в начале `#"^foo\d?$"`) не нужно.

С помощью функции `str` можно приводить объекты к строке (функция может принимать больше одного аргумента):

```clojure
clojurepy=> (str "\"Uggllglglglglglglglll\" - " name)
"\"Uggllglglglglglglglll\" - Chewbacca"
clojurepy=> (str :a 1 "hello")
":a1hello"
clojurepy=> (str ["a"] {"\"" 2})
"[\"a\"]{\"\\\"\" 2}"
clojurepy=> (str [["a"]])
"[[\"a\"]]"
clojurepy=>
```

Функция `split` возвращает вектор разделения строки по заданному разделителю,
`join` склеивает вектор строк по разделителю (можно поддерживать лишь вариант, где нужно обязательно указывать
разделитель), `replace` заменяет вхождения одной строки на другую, `first` возвращает первый элемент строки (
в clojure есть отдельное понятие символа, но в нашей реализации нужно возвращать первый элемент как строку,
как в python), `last` возвращает последний элемент, `subs` возвращает подстроку исходной строки (достаточно поддержать
вариант, где обязательно указывать и начало, и конец), `lower-case` и `upper-case` переводят все символы в нижний
и верхний регистр:

```clojure
clojurepy=> (split "hello there" " ")
["hello" "there"]
clojurepy=> (join " " ["hello" "there"])
"hello there"
clojurepy=> (replace "hello there" "ll" "LL")
"heLLo there"
clojurepy=> (first "hello")
"h"
clojurepy=> (last "hello")
"o"
clojurepy=> (subs "Clojure" 1 3)
"lo"
clojurepy=> (lower-case "MiXeD cAsE")
"mixed case"
clojurepy=> (upper-case "MiXeD cAsE")
"MIXED CASE"
clojurepy=>
```

Функция `format` делает стандартное форматирования строки с использованием `%`:

```clojure
clojurepy=> (format "Hello there, %s" "bob")
"Hello there, bob"
clojurepy=> (format "%5d" 3)
"    3"
clojurepy=>
```

#### Хэш-таблица

```clojure
clojurepy=> {:first-name "Charlie"
 :last-name "McFishwich"}
{:first-name "Charlie", :last-name "McFishwich"}
clojurepy=>
```

```clojure
clojurepy=> {"string-key" +}
{"string-key" function <+>}
clojurepy=>
```

Хэш-таблицы могут быть вложенными:

```clojure

```

Хэш-таблицы можно создавать с помощью оператора `hash-map`:

```clojure
clojurepy=> {:name {:first "John" :middle "Jacob" :last "Jingleheimerschmidt"}}
{:name {:first "John", :middle "Jacob", :last "Jingleheimerschmidt"}}
clojurepy=>
```

Для поиска элемента по ключу есть функция `get`:

```clojure
clojurepy=> (get {:a 0 :b 1} :b)
1
clojurepy=> (get {:a 0 :b {:c "ho hum"}} :b)
{:c "ho hum"}
clojurepy=>
```

`get` вернет `nil`, если ключа нет, и можно задать значение по умолчанию на такой случай:

```clojure
clojurepy=> (get {:a 0 :b 1} :c)
nil
clojurepy=> (get {:a 0 :b 1} :c "unicorns?")
"unicorns?"
clojurepy=>
```

Также хэш-таблица может стоять на месте функции в выражении, тогда результатом будет получение по ключу аргумента:

```clojure
clojurepy=> ({:a 1 :b 2} :a)
1
clojurepy=> (def m {:c 3 :d 4})
symbol <m>
clojurepy=> (m :c)
3
clojurepy=>
```

Функция `assoc` добавляет ключ-значение в хэш-таблицу (или обновляет существующий ключ), достаточно будет поддержать
возможность добавления или обновления одного ключа за один вызов `assoc`. Функция `dissoc` удяляет ключ из таблицы.

```clojure
clojurepy=> (def m {:a 1 :b 2})
symbol <m>
clojurepy=> (assoc m :c 3)
{:a 1, :b 2, :c 3}
clojurepy=> (dissoc m :b)
{:a 1}
clojurepy=> m
{:a 1, :b 2}
clojurepy=>
```

Чтобы получить вектор ключей и значений, существуют функции `keys` и `vals`:
```clojure
clojurepy=> (def m {:a 1 :b 2})
symbol <m>
clojurepy=> (keys m)
[:a :b]
clojurepy=> (vals m)
[1 2]
clojurepy=>
```

#### keywords

Основной целью их применения является быть ключами в хэш-таблицах. Ниже представлены примеры `keywords`:

```clojure
clojurepy=> :a
:a
clojurepy=> :rumplestiltsken
:rumplestiltsken
clojurepy=> :34
:34
clojurepy=> :_?
:_?
clojurepy=>
```

`keywords` можно использовать как функции, которые ищут себя в структуре данных:

```clojure
clojurepy=> (:a {:a 1 :b 2 :c 3})
1
clojurepy=>
```

Это эквивалентно:
```clojure
clojurepy=> (get {:a 1 :b 2 :c 3} :a)
1
clojurepy=>
```

Аналогично `get`, можно предоставить значение по умолчанию:
```clojure
clojurepy=> (:d {:a 1 :b 2 :c 3} "No gnome knows homes like Noah knows")
"No gnome knows homes like Noah knows"
clojurepy=>
```

### Векторы

```clojure
clojurepy=> [3 2 1]
[3 2 1]
clojurepy=>
```

Для получения элемента по индексу используется `get`:
```clojure
clojurepy=> (get [3 2 1] 0)
3
clojurepy=> (get ["a" {:name "Pugsley Winterbottom"} "c"] 1)
{:name "Pugsley Winterbottom"}
clojurepy=>
```

Вектор можно создать с помощью функции `vector`:
```clojure
clojurepy=> (vector "creepy" "full" "moon")
["creepy" "full" "moon"]
clojurepy=>
```

Для добавления элементов в конец вектора используется функция `conj`:
```clojure
clojurepy=> (conj [1 2 3] 4)
[1 2 3 4]
clojurepy=>
```

Для получения первого элемента используется функция `first`, последнего - функция `last`:
```clojure
clojurepy=> (def v [:a :b :c])
symbol <v>
clojurepy=> (first v)
:a
clojurepy=> (last v)
:c
clojurepy=>
```

Чтобы получить подвектор исходного вектора, есть функция `subvec` (достаточно поддержать возможность указывать
и нижнюю, и верхнюю границу):
```clojure
clojurepy=> (subvec [1 2 3 4 5] 2 4)
[3 4]
clojurepy=>
```

### Множества (хэш-сет)

```clojure
clojurepy=> #{"kurt vonnegut" 20 :icicle}
#{"kurt vonnegut" 20 :icicle}
clojurepy=>
```

Эквивалентный способ - через функцию `hash-set`:

```clojure
clojurepy=> (hash-set 1 1 2 2)
#{1 2}
clojurepy=>
```

Для добавления элементов в множество используем функцию `conj` (достаточно поддержать возможность добавления лишь
одного значения за раз). Аналогично есть функция `disj` для удаления:
```clojure
clojurepy=> (conj #{:a :b} :c)
#{:a :b :c}
clojurepy=> (conj #{:a :b} :b)
#{:a :b}
clojurepy=> (disj #{:a :b} :a)
#{:b}
clojurepy=>
```

Функция `set` создает множество из вектора или хэш-таблицы:

```clojure
clojurepy=> (set [3 3 3 4 4])
#{3 4}
clojurepy=>
```

В clojure функция `set` может также принимать хэш-таблицу как аргумент, но это поддерживать необязательно.

Аналогично, как с хэш-таблицей, можно использовать `keyword` как функцию:

```clojure
clojurepy=> (:a #{:a :b})
:a
clojurepy=>
```


Также можно использовать `get`:

```clojure
clojurepy=> (get #{:a :b} :a)
:a
clojurepy=> (get #{:a nil} nil)
nil
clojurepy=> (get #{:a :b} "kurt vonnegut")
nil
clojurepy=>
```

Для множеств и хэш-таблиц также работают функции `first` и `last`:
```clojure
clojurepy=> (def m {:a 2 :b 4})
symbol <m>
clojurepy=> (first m)
[:a 2]
clojurepy=> (last m)
[:b 4]
clojurepy=> (def s #{:a :b})
symbol <s>
clojurepy=> (first s)
:a
clojurepy=> (last s)
:b
clojurepy=>
```

Для пересечения, объединения и разности множеств есть соответствующие функции (достаточно поддержать лишь вариант
только с двумя аргументами):

```clojure
clojurepy=> (union #{1 2} #{2 3})
#{1 2 3}
clojurepy=> (difference #{1 2} #{2 3})
#{1}
clojurepy=> (intersection #{1 2} #{2 3})
#{2}
clojurepy=>
```

### Функции

#### Вызов функции

```clojure
clojurepy=> (+ 1 2 3 4)
10
clojurepy=> (* 1 2 3 4)
24
clojurepy=> (first [1 2 3 4])
1
clojurepy=>
```

Функция может возвращать другую функцию

```clojure
clojurepy=> (or + -)
function <+>
clojurepy=>
```

Функция в выражении может быть результатом вычисления другого выражения:

```clojure
clojurepy=> ((or + -) 1 2 3)
6
clojurepy=> ((and (= 1 1) +) 1 2 3)
6
clojurepy=> ((first [+ 0]) 1 2 3)
6
clojurepy=>
```

Числа и строки не являются валидными функциями:

```clojure
clojurepy=> (1 2 3 4)
ClojureRuntimeError: object 1 is not callable
clojurepy=> ("test" 1 2 3)
ClojureRuntimeError: object "test" is not callable
clojurepy=>
```

Функция может принимать в качестве аргументов результаты вычисления других выражений (вычисляются рекурсивно), которые в свою очередь могут оказаться функциями. Важное отличие функций от
специальных конструкций по типу `if`, `when` и т.д. состоит в том, что функция всегда вычисляют свои аргументы, в отличие от управляющих конструкций.

```clojure
clojurepy=> (+ (inc 199) (/ 100 (- 7 2)))
220
clojurepy=> (map inc [0 1 2 3])
[1 2 3 4]
clojurepy=>
```

#### Определение функции

Определение функции состоит из:
1. Ключевое слово `defn`
2. Имя функции
3. Параметры, перечисленные в квадратных скобках
4. Тело функции

В clojure есть возможность задать документацию, это поддерживать не нужно.

```clojure
clojurepy=> (defn too-enthusiastic
  [name]
  (str "OH. MY. GOD! " name " YOU ARE MOST DEFINITELY LIKE THE BEST "
  "MAN SLASH WOMAN EVER I LOVE YOU AND WE SHOULD RUN AWAY SOMEWHERE"))
function <too-enthusiastic>
clojurepy=> (too-enthusiastic "Zelda")
"OH. MY. GOD! Zelda YOU ARE MOST DEFINITELY LIKE THE BEST MAN SLASH WOMAN EVER I LOVE YOU AND WE SHOULD RUN AWAY SOMEWHERE"
clojurepy=> (defn my-func
  [a b]
  (println "adding them!")
  (+ a b))
function <my-func>
clojurepy=> (defn foo
  [x]
  [x (+ x 2) (* x 2)])
function <foo>
clojurepy=> (defn bar
  [x]
  (println x))
function <bar>
clojurepy=> (bar {:a 1 :b 2})
{:a 1, :b 2}
nil
clojurepy=> (bar [1 2 3])
[1 2 3]
nil
clojurepy=>
```

Функция может быть задана с произвольным числом аргументов произвольных типов:

```clojure
clojurepy=> (defn no-params
  []
  "I take no parameters!")
function <no-params>
clojurepy=> (defn one-param
  [x]
  (str "I take one parameter: " x))
function <one-param>
clojurepy=> (defn two-params
  [x y]
  (str "Two parameters! That's nothing! Pah! I will smoosh them "
  "together to spite you! " x y))
function <two-params>
clojurepy=>
```

В сlojure есть много форм задания вектора аргументов функции. Нужно поддержать лишь самый базовый, где все аргументы
явно перечислены в векторе (нет `&` и прочего). Поэтому перечисленные ниже варианты с распаковкой и упаковкой
аргументов поддерживать **НЕ НУЖНО**:

```clojure
(defn baz
  [a b & the-rest]
  (println a)
  (println b)
  (println the-rest))

(defn baz
  [& the-rest]
  (println a)
  (println b)
  (println the-rest))

(defn bar [a b & [c]]
         (if c
           (* a b c)
           (* a b 100)))

(defn baz [a b & {:keys [c d] :or {c 10 d 20}}]
         (* a b c d))

(defn boo [a b & {:keys [c d] :or {c 10 d 20} :as all-specified}]
          (println all-specified)
          (* a b c d))
```

Тело функции может содержать произвольное число форм, результатом вычисления функции будет результат вычисления
последней формы:
```clojure
clojurepy=> (defn illustrative-function
  []
  (+ 1 304)
  30
  "joe")
function <illustrative-function>
clojurepy=> (illustrative-function)
"joe"
clojurepy=>
```

```clojure
clojurepy=> (defn number-comment
  [x]
  (if (> x 6)
    "Oh my gosh! What a big number!"
    "That number's OK, I guess"))
function <number-comment>
clojurepy=> (number-comment 5)
"That number's OK, I guess"
clojurepy=> (number-comment 7)
"Oh my gosh! What a big number!"
clojurepy=>
```

#### Анонимные функции

Определяются так:

```clojure
(fn [param-list]
  function body)
```

```clojure
clojurepy=> (map (fn [name] (str "Hi, " name))
     ["Darth Vader" "Mr. Magoo"])
["Hi, Darth Vader" "Hi, Mr. Magoo"]
clojurepy=> ((fn [x] (* x 3)) 8)
24
clojurepy=>
```

Анонимную функцию можно связать с переменной:

```clojure
clojurepy=> (def my-special-multiplier (fn [x] (* x 3)))
symbol <my-special-multiplier>
clojurepy=> (my-special-multiplier 12)
36
clojurepy=> (def my-func
  (fn [a b]
    (println "adding them!")
    (+ a b)))
symbol <my-func>
clojurepy=> (my-func 10 20)
adding them!
30
clojurepy=>
```

#### Возращение функций как результат функции

Если функция возвращает другую функция, то эта функция замыкается на все переменные в области видимости, когда она была создана.

```clojure
clojurepy=> (defn inc-maker
  [inc-by]
  (fn [x] (+ x inc-by)))
function <inc-maker>
clojurepy=> (def inc3 (inc-maker 3))
symbol <inc3>
clojurepy=> (inc3 7)
10
clojurepy=>
```

#### Функции общего вида, которые нужно реализовать

* операторы `+`, `-`, `*`, `/`, `not`, `=`, `not=`, `>`, `>=`, `<`, `<=`, `rem`

* битовые операторы `bit-and`, `bit-and`, `bit-xor`

* функции преобразования в числа `int`, `float`

* функция `println` печатает переданные ей объекты и возвращает `nil`

```clojure
clojurepy=> (println 1 2)
1 2
nil
clojurepy=> (println [1 2 "a"])
[1 2 a]
nil
clojurepy=> (println #{:a :b "c"})
#{:a :b c}
nil
clojurepy=>
```

* `even?`, `odd?`, `zero?`, `pos?`, `neg?`, `number?`, `integer?`, `float?`, `fn?`

* функция определения, является ли объект ничем `nil?`

* `count` возвращает длину объекта (вектора, строки, хэш-таблицы и т.д.)

```clojure
clojurepy=> (count [1 2 3])
3
clojurepy=> (count "hello")
5
clojurepy=>
```

* `empty?` определяет, имеет ли структура данных нулевую длину (поддерживать возможность ее вызова не на структурах
данных необязательно)

```clojure
clojurepy=> (empty? [])
true
clojurepy=> (empty? {:a 2})
false
clojurepy=>
```

* `contains?` определяет, есть ли данный элемент в контейнере (для векторов, хэш-таблиц и множеств)

```clojure
clojurepy=> (contains? #{:a} :a)
true
clojurepy=> (contains? #{:a} :b)
false
clojurepy=> (contains? {:a 1 :b 1} :a)
true
clojurepy=> (contains? {:a 1 :b 1} :c)
false
clojurepy=> (contains? [1 2 3] 1)
true
clojurepy=> (contains? [1 2 3] 4)
false
clojurepy=>
```

* инкремент `inc` и декремент `dec`

* `range` (достаточно лишь поддержать возможность задания двух аргументов)

```clojure
clojurepy=> (range 3 8)
[3 4 5 6 7]
clojurepy=>
```

* Функции минимума и максимума `min`, `max` (обратите внимание на случай, когда аргументами оказываются коллекции):

```clojure
clojurepy=> (max 1 5 2 8 3)
8
clojurepy=> (max [1 5 2 8 3])
[1 5 2 8 3]
clojurepy=>
```

* Функция `pop` удаляет последний элемент из контейнера

```clojure
clojurepy=> (pop [1 2 3])
[1 2]
clojurepy=> (pop #{:a :b})
#{:a}
clojurepy=> (pop {:a 1 :b 2 :c 3})
{:a 1, :b 2}
clojurepy=>
```

* функциональное программирование: `map`, `filter`, `apply`, `reduce`

```clojure
clojurepy=> (map inc [10 20 30])
[11 21 31]
clojurepy=> (map (fn [x] (str "=" x "=")) [10 20 30])
["=10=" "=20=" "=30="]
clojurepy=> (map (fn [x y] (str x y)) [:a :b :c] [1 2 3])
[":a1" ":b2" ":c3"]
clojurepy=> (map (fn [x y] (str x y)) [:a :b :c] [1 2 3 4 5 6 7])
[":a1" ":b2" ":c3"]
clojurepy=> (filter odd? (range 0 10))
[1 3 5 7 9]
clojurepy=> (apply max [1 5 2 8 3])
8
clojurepy=> (apply max 4 55 [1 5 2 8 3])
55
clojurepy=> (reduce + [1 2 3 4 5])
15
clojurepy=> (reduce + 10 [1 2 3 4 5])
25
clojurepy=> (reduce (fn [accum x]
          (assoc accum
                 x
                 (str x "-" 53)))
        {}
        ["hi" "hello" "bye"])
{"hi" "hi-53", "hello" "hello-53", "bye" "bye-53"}
clojurepy=>
```

* `repeat`, `identity`, `reverse` (достаточно поддержать лишь для векторов)

```clojure
clojurepy=> (repeat 5 "x")
["x" "x" "x" "x" "x"]
clojurepy=> (identity 4)
4
clojurepy=> (reverse [1 2 3])
[3 2 1]
clojurepy=>
```
