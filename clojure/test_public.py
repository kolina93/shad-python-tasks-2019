import io

from pathlib import Path
from subprocess import Popen, PIPE
from sys import executable

import pytest

from . import clojure


CURRENT_DIRECTORY = Path(__file__).parent


def test_clojure_simple() -> None:
    process = Popen([executable, str(CURRENT_DIRECTORY / 'clojure.py')], stdin=PIPE, stdout=PIPE)
    output, _ = process.communicate('(+ 1 2)\n'.encode())
    assert output.decode() == '3\n'


def test_clojure_raiser_error() -> None:
    script = '('

    with pytest.raises(clojure.ClojureError):
        clojure.run(io.StringIO(script))
