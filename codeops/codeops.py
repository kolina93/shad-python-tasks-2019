from types import CodeType
from typing import Dict


def count_operations(source_code: CodeType) -> Dict[str, int]:
    """Count byte code operations in given source code.
    :param source_code: the bytecode operation names to be extracted from
    :return: operation counts
    """
