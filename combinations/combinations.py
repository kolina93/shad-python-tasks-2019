from typing import Iterable, Tuple


def generate_combinations(words: Iterable[str]) -> Iterable[Tuple[str, ...]]:
    """
    Generate all combinations of even length from given words without repetitions.
    :param words: array of strings consisting only from latin symbols
    :return: generator over all combinations (all words converted to lower case)
    """
    pass
