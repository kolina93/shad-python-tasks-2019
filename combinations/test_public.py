from dataclasses import dataclass
from inspect import isgeneratorfunction
from typing import List, Tuple

import pytest

from .combinations import generate_combinations


@dataclass
class Case:
    words: List[str]
    result: List[Tuple[str, ...]]


TEST_CASES = [
    Case(
        words=['World', 'Hello', 'hello', 'likeable', 'first'],
        result=[
            ('first', 'hello'),
            ('first', 'likeable'),
            ('first', 'world'),
            ('hello', 'likeable'),
            ('hello', 'world'),
            ('likeable', 'world'),
            ('first', 'hello', 'likeable', 'world'),
        ],
    ),
    Case(words=['bushwhack', 'cool'], result=[('bushwhack', 'cool')]),
    Case(words=['bushwhack', 'bushwhack', 'bushwhacs'], result=[('bushwhack', 'bushwhacs')]),
]


def test_function_is_generator() -> None:
    assert isgeneratorfunction(generate_combinations), "generate_combinations must be a generator"


@pytest.mark.parametrize('t', TEST_CASES)
def test_generate_combinations(t: Case) -> None:
    print(list(generate_combinations(t.words)))
    print(t.result)
    assert list(generate_combinations(t.words)) == t.result
