from typing import List, Mapping, Any, Set, Dict


def get_unique_page_ids(records: List[Mapping[str, Any]]) -> Set[int]:
    """
    Get unique visited web pages
    :param records: records of hit-log
    :return: Unique web pages
    """


def get_unique_page_ids_visited_after_ts(records: List[Mapping[str, Any]], ts: int) -> Set[int]:
    """
    Get unique web pages visited after some timestamp (not included)
    :param records: records of hit-log
    :param ts: timestamp
    :return: Unique web pages visited after some timestamp
    """


def get_unique_user_ids_visited_page_after_ts(records: List[Mapping[str, Any]], ts: int, page_id: int) -> Set[int]:
    """
    Get unique users who visited a given web page after some timestamp (not included)
    :param records: records of hit-log
    :param ts: timestamp
    :param page_id: web page identifier
    :return: Unique web pages visited after some timestamp
    """


def get_events_by_device_type(records: List[Mapping[str, Any]], device_type: str) -> List[Mapping[str, Any]]:
    """
    Filter for events with a given device type preserving order
    :param records: records of hit-log
    :param device_type: device type's name to filter by
    :return: filtered events
    """


DEFAULT_REGION_ID = 100500


def get_region_ids_with_none_replaces_by_default(records: List[Mapping[str, Any]]) -> List[int]:
    """
    Extract visited regions with order preservation. If region was not defined, replace it with default region id
    :param records: records of hit-log
    :return: region ids
    """


def get_region_id_if_not_none(records: List[Mapping[str, Any]]) -> List[int]:
    """
    Extract visited regions with order preservation if they were defined
    :param records: records of hit-log
    :return: region ids
    """


def get_keys_where_value_is_not_none(r: Mapping[str, Any]) -> List[str]:
    """
    Extract keys where corresponding values are defined
    :param r: record of hit-log
    :return: keys where values are defined
    """


def get_record_with_none_if_key_not_in_keys(r: Mapping[str, Any], keys: Set[str]) -> Dict[str, Any]:
    """
    Replace all values with None except for given keys
    :param r: record of hit-log
    :param keys: keys not
    :return: record with all values with None except for given keys
    """


def get_record_with_key_in_keys(r: Mapping[str, Any], keys: Set[str]) -> Dict[str, Any]:
    """
    Filter record by keys
    :param r: record of hit-log
    :param keys: keys to filter by
    :return: filtered record
    """


def get_keys_if_key_in_keys(r: Mapping[str, Any], keys: Set[str]) -> Set[str]:
    """
    Extract all keys from given set which are contained in record
    :param r: record of hit-log
    :param keys: keys to search for
    :return: all keys from given set which are contained in record
    """
