import pytest

from .decimal_to_roman import decimal_to_roman


@pytest.mark.parametrize('decimal_number, roman_number', [(1, 'I'), (5, 'V'), (849, 'DCCCXLIX')])
def test_decimal_to_roman(decimal_number: int, roman_number: str) -> None:
    assert decimal_to_roman(decimal_number) == roman_number
