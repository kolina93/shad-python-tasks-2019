from typing import Set


def walk(server_url: str, start_id: str, server_throughput: int) -> Set[str]:
    """
    Crawls all enities reachable from the start page.
    :param server_url: Address to crawl
    :param start_id: id of start entity
    :param server_throughput: server's limit for concurrent requests
    :return: all reachable entities
    """
    return set()
