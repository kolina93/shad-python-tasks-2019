from json import dumps
from time import sleep
from typing import List, Union

from cherrypy import request, expose, HTTPError, quickstart


class DependencyHolder:
    def __init__(self) -> None:
        self.dependencies = {
            'SCDjuMMEPp': [
                'MkcVESOVmz',
                'aLhIJRWzcB',
                'LkYldmVSaf',
                'tqBAKAWZTb',
                'hkmJtxwHxP',
                'FWbVIXSYRy'
            ],
            'gjsvHbjaUS': [
                'MkcVESOVmz',
                'aLhIJRWzcB',
                'LkYldmVSaf',
                'hkmJtxwHxP',
                'FWbVIXSYRy'
            ],
            'TlvNMAwKxV': [
                'pKhOyKqlYM',
                'lJKqmGZhrn'
            ],
            'tqBAKAWZTb': [
                'MkcVESOVmz',
                'VERmYMpXnK',
                'LkYldmVSaf',
                'SCDjuMMEPp',
                'gjsvHbjaUS',
                'FTmSrAiTaV'
            ],
            'hkmJtxwHxP': [
                'MkcVESOVmz',
                'VERmYMpXnK',
                'LkYldmVSaf',
                'SCDjuMMEPp',
                'gjsvHbjaUS',
                'FTmSrAiTaV'
            ],
            'FWbVIXSYRy': [
                'MkcVESOVmz',
                'LkYldmVSaf',
                'SCDjuMMEPp',
                'gjsvHbjaUS',
                'NxINrUjdpV',
                'lAZWvrlWnY',
                'kLWadKxEXB',
                'TlvNMAwKxV'
            ],
            'lAZWvrlWnY': [
                'MkcVESOVmz',
                'LkYldmVSaf',
                'FWbVIXSYRy',
                'pKhOyKqlYM'
            ],
            'MkcVESOVmz': [
                'FTmSrAiTaV',
                'FWbVIXSYRy',
                'NxINrUjdpV',
                'lAZWvrlWnY',
                'kLWadKxEXB',
                'pKhOyKqlYM'
            ],
            'lJKqmGZhrn': [
                'TlvNMAwKxV',
                'pKhOyKqlYM'
            ],
            'LkYldmVSaf': [
                'aLhIJRWzcB',
                'tqBAKAWZTb',
                'SCDjuMMEPp',
                'hkmJtxwHxP',
                'gjsvHbjaUS',
                'FTmSrAiTaV',
                'FWbVIXSYRy',
                'NxINrUjdpV',
                'lAZWvrlWnY',
                'kLWadKxEXB',
                'pKhOyKqlYM'
            ],
            'VERmYMpXnK': [
                'aLhIJRWzcB',
                'tqBAKAWZTb',
                'hkmJtxwHxP',
                'FTmSrAiTaV'
            ],
            'FTmSrAiTaV': [
                'MkcVESOVmz',
                'aLhIJRWzcB',
                'VERmYMpXnK',
                'LkYldmVSaf',
                'tqBAKAWZTb',
                'hkmJtxwHxP'
            ],
            'aLhIJRWzcB': [
                'MkcVESOVmz',
                'VERmYMpXnK',
                'LkYldmVSaf',
                'SCDjuMMEPp',
                'gjsvHbjaUS',
                'FTmSrAiTaV'
            ],
            'NxINrUjdpV': [
                'MkcVESOVmz',
                'LkYldmVSaf',
                'FWbVIXSYRy',
                'TlvNMAwKxV',
                'pKhOyKqlYM'
            ],
            'pKhOyKqlYM': [
                'NxINrUjdpV',
                'lAZWvrlWnY',
                'kLWadKxEXB',
                'TlvNMAwKxV'
            ],
            'kLWadKxEXB': [
                'MkcVESOVmz',
                'LkYldmVSaf',
                'FWbVIXSYRy',
                'TlvNMAwKxV',
                'pKhOyKqlYM'
            ]
        }
        self.delays = {
            'SCDjuMMEPp': 4,
            'gjsvHbjaUS': 2,
            'TlvNMAwKxV': 4,
            'tqBAKAWZTb': 4,
            'hkmJtxwHxP': 1,
            'FWbVIXSYRy': 4,
            'lAZWvrlWnY': 2,
            'MkcVESOVmz': 4,
            'lJKqmGZhrn': 2,
            'LkYldmVSaf': 3,
            'VERmYMpXnK': 2,
            'FTmSrAiTaV': 1,
            'aLhIJRWzcB': 3,
            'NxINrUjdpV': 2,
            'pKhOyKqlYM': 2,
            'kLWadKxEXB': 3
        }

    def _cp_dispatch(self, vpath: List[str]) -> Union[List[str], 'DependencyHolder']:
        if len(vpath) == 1:
            request.params['entity_id'] = vpath.pop()
            return self

        return vpath

    @expose
    def index(self, entity_id: str) -> str:
        if entity_id not in self.dependencies:
            raise HTTPError(status=404)
        sleep(self.delays[entity_id])
        return dumps(self.dependencies[entity_id])


if __name__ == '__main__':
    quickstart(DependencyHolder())
