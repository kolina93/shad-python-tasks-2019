from multiprocessing import Process
from os import kill
from signal import SIGTERM
from time import sleep
from typing import Set

from cherrypy import quickstart, server

from .dependency_walker import walk
from .simple_server import DependencyHolder


def run_server_app(host: str, port: int, workers: int) -> None:
    server.thread_pool = workers
    server.thread_pool_max = workers
    server.socket_host = host
    server.socket_port = port
    quickstart(DependencyHolder(), '', {'/': {}})


def run_dependency_walker(expected: Set[str], host: str, port: int, throughput: int) -> None:
    assert expected == walk(f'http://{host}:{port}', 'MkcVESOVmz', throughput)


def test_dependencies() -> None:
    host, port = 'localhost', 8080
    throughput = 4
    app_process = Process(target=run_server_app, args=(host, port, throughput))
    app_process.start()
    sleep(1)

    expected = {
        'FTmSrAiTaV',
        'FWbVIXSYRy',
        'LkYldmVSaf',
        'MkcVESOVmz',
        'NxINrUjdpV',
        'SCDjuMMEPp',
        'TlvNMAwKxV',
        'VERmYMpXnK',
        'aLhIJRWzcB',
        'gjsvHbjaUS',
        'hkmJtxwHxP',
        'kLWadKxEXB',
        'lAZWvrlWnY',
        'lJKqmGZhrn',
        'pKhOyKqlYM',
        'tqBAKAWZTb'
    }

    try:
        walker_process = Process(target=run_dependency_walker, args=(expected, host, port, throughput))
        walker_process.start()
        walker_process.join(timeout=25)
        if walker_process.is_alive():
            assert walker_process.pid is not None
            kill(walker_process.pid, SIGTERM)
            raise RuntimeError('Walker process did not end in 25 seconds.')
        if walker_process.exitcode != 0:
            raise RuntimeError('Walker process failed.')
    finally:
        assert app_process.pid is not None
        kill(app_process.pid, SIGTERM)
        app_process.join(timeout=5)
