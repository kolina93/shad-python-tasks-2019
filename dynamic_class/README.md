## Finding attributes

### Условие
В этой задаче нужно написать функцию `attributes_finder`, которая подбирала бы нужные атрибуты из общения с оракулом
и возвращала бы некоторый объект, в котором были определены найденные ей атрибуты. Более подробно: есть список
возможных атрибутов `attribute_choices` (все различные, атрибуты в этом списке не обязательно идут в сортированном
порядке, размер списка не превышает `1024`), и из них заранее было выбрано `attributes_count` некоторых атрибутов,
про которые знает оракул. Обозначим отсортированный лексикографически список этих атрибутов за `chosen_attributes`. Вы
можете выбирать некоторый атрибут `attr` из списка `attribute_choices` и значение этого атрибута у оракула будет равно
количеству атрибутов из списка `chosen_attributes`, лексикографически меньших `attr` (по модулю того, как сравниваются
строки в python). Ваша задача найти все атрибуты из списка `attribute_choices`, сделав не больше `150` запросов
на атрибут к оракулу. Если вы превысите это число, то будет выбрасываться исключение `ValueError`. В итоге вам нужно
вернуть объект, в котором атрибуты из списка `chosen_attributes` будут заданы в `1`, а все остальные атрибуты из списка
`attribute_choices` были не заданы, т.е. попытка их получения бросала бы исключение `AttributeError`. Посмотрите на
пример из `test_public.py`, если есть вопросы, какое поведение ожидается от вашей функции.

### Пример
```python
In [1]: class Checker:
   ...:     def __init__(self, tries_left: int = 150) -> None:
   ...:         self._tries_left = tries_left
   ...:
   ...:     def __getattr__(self, item: str) -> int:
   ...:         if self._tries_left == 0:
   ...:             raise ValueError
   ...:         self._tries_left -= 1
   ...:         if item == 'a':
   ...:             return 0
   ...:         elif item in ('b', 'c'):
   ...:             return 1
   ...:         raise AttributeError
   ...:

In [2]: from dynamic_class import attributes_finder

In [3]: oracle = Checker()

In [4]: result = attributes_finder(oracle, ['a', 'b', 'c'], 2)

In [5]: result.a
Out[5]: 1

In [6]: result.b
---------------------------------------------------------------------------
AttributeError                            Traceback (most recent call last)
<ipython-input-6-f1cd3b2d953e> in <module>()
----> 1 result.b

AttributeError: 'Shiny' object has no attribute 'b'

In [7]: result.c
Out[7]: 1

In [8]:
```
