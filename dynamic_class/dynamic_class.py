from typing import Any, List


def attributes_finder(oracle: Any, attribute_choices: List[str], attributes_count: int) -> Any:
    """
    Searches for all set attributes from passed list in oracle object.
    :param orcale: target object to ask for attribute
    :param attribute_choices: all possible attributes
    :param attributes_count: it's guaranteed that exactly attributes_count attributes will be present in oracle
    :return: object with state of attributes matching the state in oracle
    """
