import math

from typing import Optional, Dict

import pytest

from .dynamic_class import attributes_finder


class CheckerBase:
    def __init__(self, tries_left: int = 150, attributes: Optional[Dict[str, int]] = None) -> None:
        self._tries_left = tries_left
        self.attributes = attributes or {}

    def __getattr__(self, item: str) -> int:
        if self._tries_left == 0:
            raise ValueError
        return self.attributes[item]


class Checker(CheckerBase):
    def __init__(self) -> None:
        super().__init__(attributes={'a': 0, 'b': 1, 'c': 1})


class BigExampleChecker(CheckerBase):
    def __init__(self) -> None:
        attributes = {'a' * (i + 1): int(math.log2(i + 1)) for i in range(1024)}
        print(attributes)
        super().__init__(attributes=attributes)


def test_simple() -> None:
    oracle = Checker()

    result = attributes_finder(oracle, list(oracle.attributes.keys()), 2)

    assert result.a == 1
    assert result.c == 1
    with pytest.raises(AttributeError):
        _ = result.b


def test_big() -> None:
    oracle = BigExampleChecker()

    result = attributes_finder(oracle, list(oracle.attributes.keys()), 10)

    attributes_sorted = sorted(oracle.attributes.items())

    # It's guaranteed in this example that the last attribute isn't set
    for (attribute, value), (_, next_value) in zip(attributes_sorted, attributes_sorted[1:]):
        if value + 1 == next_value:
            assert getattr(result, attribute) == 1
        else:
            with pytest.raises(AttributeError):
                _ = getattr(result, attribute)
