from typing import Callable, TypeVar, Union, Tuple, Dict, Any


T = TypeVar('T')


def exception_info(func: Callable[[], T]) -> Union[T, Tuple[str, Dict[str, Any]]]:
    """
    Try to call function and return some information from call stack in case of exception.
    :param func: callable without arguments
    :return: In case of no exception from func's call must return result of call.
             In case of exception must return 2-element tuple:
                 1. the line of code that precedes the line from which the exception was thrown
                 2. mapping from argument name to argument value of function from which exception was thrown
    """
    return func()
