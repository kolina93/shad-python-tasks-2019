from .exception_info import exception_info


def func_with_exception(a: int, b: int) -> None:
    a += b
    raise ValueError()


def func_without_exception() -> int:
    return 42


def _test_func() -> None:
    return func_with_exception(1, 2)


def test_simple() -> None:
    call_result = exception_info(_test_func)
    assert call_result is not None
    line, results = call_result
    assert line.strip() == 'a += b'
    assert results == {'a': 3, 'b': 2}

    assert exception_info(func_without_exception) == 42
