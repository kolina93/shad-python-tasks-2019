from typing import Dict, Any


class ExtendedDict(Dict[str, Any]):
    """
    Extends dict functionality by allowing to modify values by working with attributes.
    """
