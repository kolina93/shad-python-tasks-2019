import pytest

from .extended_dict import ExtendedDict


def test_extended_dict() -> None:
    obj = ExtendedDict()

    obj['a'] = 1
    assert obj['a'] == 1
    assert obj.a == 1

    obj.a = 2
    assert obj['a'] == 2
    assert obj.a == 2

    obj.b = 3
    obj.aa = 4
    obj['v'] = 18
    obj.ffffff = 25

    assert obj.aa == 4
    assert obj.ffffff == 25

    del obj.b
    del obj['a']

    assert obj[1::2] == [('ffffff', 25)]
    assert obj[1:] == [('ffffff', 25), ('v', 18)]

    with pytest.raises(ValueError):
        del obj[1]
