import pytest

from .fibonacci_numbers import fibonacci_number


@pytest.mark.parametrize('n, result', [(0, 0), (3, 2), (5, 5)])
def test_fibonacci_numbers(n: int, result: int) -> None:
    assert fibonacci_number(n) == result
