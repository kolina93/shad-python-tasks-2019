class FieldInitializer(type):
    """
    Metaclass which supports setting arbitrary attributes
    when creating a class instance.
    """
