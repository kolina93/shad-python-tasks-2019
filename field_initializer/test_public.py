import pytest

from .field_initializer import FieldInitializer


def test_one_attribute() -> None:
    class A(metaclass=FieldInitializer):
        pass

    a = A(foo=42)  # type: ignore
    assert a.foo == 42  # type: ignore


def test_multiple_attrs() -> None:
    class B(metaclass=FieldInitializer):
        def __init__(self, a: int, b: int, c: int, d: int = 1) -> None:
            self.a = 1
            self.b = b
            self.d = d + 1

    b = B(1, 2, 3, foo=4, d=5)  # type: ignore
    assert b.a == 1
    assert b.b == 2
    assert b.foo == 4  # type: ignore
    with pytest.raises(AttributeError):
        _ = b.c  # type: ignore
