from typing import Iterable, Any


def flatten_it(iterable: Iterable[Any]) -> Iterable[Any]:
    """
    Flatten all nested elements of iterable recursively.
    :param iterable: some iterable
    :return: generate all elements of recursively flattened iterable
    """
    pass
