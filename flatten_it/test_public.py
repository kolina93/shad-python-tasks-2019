from typing import Iterator, Any, List

import pytest

from .flatten_it import flatten_it


def _simple_generator() -> Iterator[Any]:
    yield 1
    yield 1.5
    yield range(4)


def test_simple() -> None:
    assert list(flatten_it(_simple_generator())) == [1, 1.5, 0, 1, 2, 3]
    assert list(flatten_it([1, 2, 3, None, [], (1, 2)])) == [1, 2, 3, None, 1, 2]


def test_cycle() -> None:
    a: List[Any] = []
    a.append(a)
    with pytest.raises(ValueError):
        _ = list(flatten_it(a))


def test_evil_string() -> None:
    with pytest.raises(ValueError):
        _ = list(flatten_it("aba"))
