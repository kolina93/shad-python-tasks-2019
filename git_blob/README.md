## GIT BLOB

### Условие

Вам дана директория с внутренними объектами гита и вам нужно научиться декодировать хранящуюся там информацию, чтобы вытащить исходное состояние некоторого файла при первом коммите.

### Устройство гита

Для начала рекомендуется почитать, [как устроен гит внутри](https://git-scm.com/book/ru/v2/Git-изнутри-Объекты-Git).

#### Blob
Гит хранит все объекты в директории `.git/objects` (на самом деле не всегда, но в рамках данной задачи это не важно),
структура такова:
```bash
$ tree objects
objects
├── 00
│   └── 84676834ac99cd8610eae1e25e924f512f8223
├── 04
│   └── 139453c5ce7481f9dbeebcee6b4995ea5cbf05
├── 07
│   └── 32fedb0819967fe117247d41307ce1227b59ba
├── 0a
│   └── a4c14194a626fc6844a7a94f040fa48803f2bf
├── 0c
│   ├── 5fd1fa0dce1b3f3b070f992e87f1177bdac49c
│   ├── 658fd8c27ca2f1ee6974416e3b9a5ffec789dd
│   ├── ca0f8b90664e88a44c139136e103f807a65e3d
│   └── f127e22cd869e96828e6fa253f66404a207c53
...
```
Каждый объект имеет имя в виде хэша (если точнее, то SHA-1 от содержимого),
причем полный хэш объекта складывается из имени директории,
где он лежит (первые 2 символа) и самого имени файла, итого 40 символов.

Некоторые из этих хэшей являются хэшами коммитов
(которые можно обычно видеть при работе с гитом, например, в `git log`).

Итак, все хранилище гита представляет собой объекты трех типов: `tree` / `commit` / `blob`, причем термин `blob`
(Binary Large Object) можно отнести ко всем трем, поэтому последний следует понимать, как
просто файл в бинарном формате, не имеющий внутри никакой четкой структуры.

Для того, чтобы работать с содержимым блоба, его достаточно разархивировать. Для сжатия используется `zlib`,
[доступная в питоне](https://docs.python.org/3/library/zlib.html#zlib.decompress) из коробки.

Если распаковать произвольный блок, то можно увидеть, что его содержимое человекочитаемое:
```bash
$ zlib-flate -uncompress < objects/13/7eb3a044c05f7333c00b3cba8be3f40fb68bf4
blob 503grade:
  image: gcr.io/shad-minsk-python/grader
  only:
    - /^submits/.*$/
  variables:
    LC_CTYPE: "en_US.UTF-8"
  script:
    - export CI_TASK_NAME=$(echo $CI_BUILD_REF_NAME | sed -e 's/submits\///g')
    - cp -r $CI_TASK_NAME /opt/shad/
    - cp test_utils.py /opt/shad
    - cd /opt/shad/
    - find private/ -name '*.py' | xargs chmod o-rwx
    - find private/ -name '*.a' | xargs chmod o-rwx
    - chmod o+r private/*.py
    - chmod -R 777 /opt/shad/$CI_TASK_NAME
    - python3 grade.py grade
```

Каждый блоб гита имеет следующий формат: `<type>\s<content length>\0<content>`, то есть сначала идет заголовок, где
первым указывается тип, потом 1 пробельный символ и длина содержимого блоба (без заголовка). Заголовок завершается
нулевым символом (символ с кодом 0, он же `\0` из C, в питоне его можно задать так: `b'\x00'`), далее идут данные
блоба, в зависимости от типа они могут как иметь какой-либо формат, так и нет (произвольные данные пользователя).

#### Commit
Рассмотрим устройство коммита:
```bash
$ zlib-flate -uncompress < objects/1b/d9ee3785043bb23af69523af7a59b43d1fe533
commit 247tree 13d2a3da88689c18f94ac806f08df680119ef156
parent 44790dc7fe0007a8ee725f96248bc1ceeae44848
author Nick <ni_kolya93@mail.ru> 1568884031 +0300
committer Nick <ni_kolya93@mail.ru> 1568884031 +0300

rename source file for fibonacci_numbers problem
```
Видно, что сначала идет заголовок, затем длина тела в символах (потом не отображаемый `\0`) и дальше тело
коммита.

Тело делится на заголовок и сообщение коммита, между ними 1 пустая строка.
Заголовок состоит из n строк в формате `<key>\s<value>\n`, т.е. ключ, пробел и значение до конца строки.
При этом поле `parent`, насколько мне известно, единственное может повторяться несколько раз
(например, у merge-коммитов 2 родителя, и тут будут указаны их хэши), или вовсе отсутствовать (первый коммит).

#### Tree
Каждый коммит содержит хэш дерева. Дерево - это специальный блоб, являющийся аналогом директории (папки, если угодно).
Дерево содержит список из имен и хэшей файлов в директории (а так же прав доступа, но они нам не понадобятся).
Если же в директории есть вложенная поддиректория, то она включается в список наравне с файлами, т.к. тоже имеет имя
и хэш, но этот хэш будет ссылаться на блоб-дерево. Таким образом блоб-дерево не является деревом сам по себе, но
содержит ссылки (в виде хэшей) на другие блобы - файлы (листья) и директории (узлы), отсюда и название.

Формат дерева более сложный для восприятия человеком (и не описан по ссылке выше).
Если попытаться вывести его в консоль, то получим практически нечитаемую кашу (остается в виде упражнения читателю).
Причина в том, что для оптимизации хэши-ссылки внутри дерева хранятся в бинарном формате.

В итоге тело блоба-дерева выглядит так: `<mode1>\s<name1>\0<20 x bytes><mode2>\s<name2>\0<20 x bytes>...`, т.е. без проблов и переносов строк подряд
указаны данные. Для каждого объекта сначала идет заголовок произвольной длины, завершающийся `\0` (в нем указаны
права на файл и его имя через пробел), а потом 20 байт хэша.

Почему же 20 байт, если хэш состоит из 40 символов? Дело в следующем: 1 беззнаковый байт может
принимать одно из 256 значений, что в шестнадцатеричной системе представляется в виде 2-х символов - от 00 до ff.
Т.е. 2 символа хэша в текстовом формате занимают 1 байт в бинарном. Текстовое представление 16-ричных данных носит
название HEX (hexadecimal).

В питоне получить текстовое представление из байт можно с помощью одноименного метода
байтовой строки: `b'\x00\xfa\xbe\x0'.hex() -> '00fabede0a'`. При этом иногда байты сами по себе могут
соответствовать печатному символу, поэтому если напечатать байтовую строку, то можно получить кашу из печатных
и непечатных символов:
```python
>>> b'\x68\x65\x6c\x6c\x6f\x00\x77\x6f\x72\x6c\x64'
b'hello\x00world'
```

В рамках задачи вам предлагается рекурсивно обойти дерево из первого коммита и найти хэш файла с определенным именем,
чтобы потом получить его содержимое.

#### Data
Кроме двух выше перечисленных типов блобов остаются только 1 тип для пользовательских данных. Эти данные будут
иметь обычный для всех блобов заголовок (с типом `blob` некоторая тавтология и длиной остального содержимого) и
пользовательские данные в том формате, в котором они были сохранены в гите.

Пример:
```bash
$ zlib-flate -uncompress < objects/fc/d8be914c8e22b0b230f0af49c0c44c2dfea02c
blob 208def collect_numbers(a: int, b: int) -> str:
    """
    :param a: begin of range
    :param b: length of range
    :return: 10th pow of concatenation of a, a+1, ..., a+b into one number
    """
    return ''
```

### Рекомендации по решению
В задаче вам уже будет дан набор типов для работы с основными объектами, которые вам придется обрабатывать:
`Blob` / `Commit` / `Tree`, а также вспомогательный тип `BlobType`, который поможет избежать сравнения типов в виде строк
(ниже риск ошибиться). Воспользуйтесь, пожалуйста данными типами.
```python
>>> blob = Blob(type_=BlobType.from_bytes(b'commit'), content=b'...')
>>> blob
Blob(type_=<BlobType.COMMIT: b'commit'>, content=b'...')
>>> blob.type_ is BlobType.COMMIT
True
```

Задача представляет из себя набор функций, которые вам необходимо реализовать.

Набор функций для реализации:
* `read_blob` - читает блоб из файла, парсит заголовок и возвращает тип `Blob`;
* `traverse_objects` - обходит файловое дерево объектов (лежат рядом с задачей в директории `objects`), парсит их
с помощью `read_blob` и собирает в маппинг, вида хэш -> блоб;
* `parse_commit` - принимает на вход `Blob` с типом `BlobType.COMMIT`, парсит его содержимое и возвращет тип `Commit`
* `parse_tree` - аналогично принимает блоб с типом `BlobType.TREE` и парсит его содержимое. Рекурсивный обход не делает!
Работает только с 1 уровнем вложенности, всех потомков возвращает в виде маппинга имя -> блоб,
никак не обрабатывая их тип. Для работы требует список всех доступных блобов, в виде маппинг по хэшу (из
`traverse_objects`).
* `find_initial_commit` - проходит все блобы-коммиты и находит самый первый коммит в репозиторий.
* `search_file` - рекурсивно обходит переданное в качестве корня дерево, ищет файл с заданным именем и возвращет его.
