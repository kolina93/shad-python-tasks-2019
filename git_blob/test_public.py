from pathlib import Path
from dataclasses import dataclass
from collections import Counter
from typing import Dict

import pytest

from .git_blob import (
    BlobType, Blob, Commit,
    read_blob, traverse_objects, parse_commit, parse_tree, find_initial_commit, search_file
)


OBJECTS_DIR = Path(__file__).parent / 'objects'


@dataclass
class ReadBlobCase:
    path: Path
    result: Blob

    def __str__(self) -> str:
        return str(self.path)


READ_BLOB_TEST_CASES = [
    ReadBlobCase(
        path=OBJECTS_DIR / 'b2' / '86fcd9d0c3c18c7cded9f8eebd49bf3bfb837f',
        result=Blob(type_=BlobType.COMMIT,
                    content=b'tree ea41d4163435d939fbe56251784c87ac5143a2b4\n'
                            b'parent 45a5f1c226e7620a248dc17e3aa0b063323545d1\n'
                            b'author Nick <ni_kolya93@mail.ru> 1568773233 +0300\n'
                            b'committer Nick <ni_kolya93@mail.ru> 1568773233 +0300\n'
                            b'\n'
                            b'add 1st seminar tasks\n',)
    ),
    ReadBlobCase(
        path=OBJECTS_DIR / '99' / 'e3407bc4f60924fd9ab67e6bee60678530368a',
        result=Blob(type_=BlobType.DATA,
                    content=b'grade:\n'
                            b'  image: gcr.io/shad-minsk-python/grader\n'
                            b'  only:\n'
                            b'    - /^submits/.*$/\n'
                            b'  variables:\n'
                            b'    LC_CTYPE: "en_US.UTF-8"\n'
                            b'  script:\n'
                            b'    - export CI_TASK_NAME=$(echo $CI_BUILD_REF_NAME | sed -e \'s/submits\\///g\')\n'
                            b'    - cp -r $CI_TASK_NAME /opt/shad/\n'
                            b'    - cp test_utils.py /opt/shad\n'
                            b'    - cd /opt/shad/\n'
                            b'    - find private/ -name \'*.py\' | xargs chmod o-rwx\n'
                            b'    - find private/ -name \'*.a\' | xargs chmod o-rwx\n'
                            b'    - chmod o+r private/*.py\n'
                            b'    - chmod -R 777 /opt/shad/$CI_TASK_NAME\n'
                            b'    - python3 grade.py grade\n',)
    ),
    ReadBlobCase(
        path=OBJECTS_DIR / '51' / 'ed5253d8622973e5618c55b68f072fd64f5c13',
        result=Blob(
            type_=BlobType.TREE,
            content=b'100644 .gitignore\x004\xde\xbc;\'\'\xectZ\xc6s\xa2r\xee\x9f\xefN \x18\xb1'
                    b'100644 .gitmodules\x00\x1dn\xbb\xd8\xa7\x01\xa2M\x10X\xd1\xc5\x00yx\x99\x87\x9c;R'
                    b'100644 .grader-ci.yml\x00P\x8a\x88\x88\x15\xf8\xbe\xc2`\x0cw\xcb\xbdN>\xbd\x14\xd0\xe6\xc5'
                    b'40000 .img\x00\x88dv\t\xd4G\xa5O\xe9\r\xdfILG\xfb\x84\xba\x8c\xdb\x1e'
                    b'100644 README.md\x00mV\x99c\xff\x80\xb7\xe2\x10r\xe0\xe3\x18@\x15T\xb1(b1'
                    b'100644 custom.css\x00j0$\x14\xb1\xc3\xfa0\xd7\xf8\x99-eO\x9fXr\xef\xa1,1'
                    b'60000 private\x00C\x83\xef\xbe\xeaP\xef\x83]\xf1\xb6D~\xa1\x94\x8d\x9a)\x97?'
                    b'100644 submit.py\x00\xeb\x07w\x89\xbax\xecc\xce\x8a\x80\xdcm\xf19,@\xf0\xbc\n'
                    b'100644 test_utils.py\x00\xaf\x1d"8\xe9\x8e\xa0\xacO7\xddF\xed\xdc\xe2zq\xeck\xfe',)
    )
]


@pytest.mark.parametrize('case', READ_BLOB_TEST_CASES, ids=str)
def test_read_blob(case: ReadBlobCase) -> None:
    answer = read_blob(case.path)

    assert answer == case.result


def test_traverse_objects() -> None:
    blobs = traverse_objects(OBJECTS_DIR)

    assert set(blobs.keys()) == {
        'd465f0f97e1a5648464fe7c41e196f621295eb83', 'b5b31e152f3bc85869a1850331ed59597a4dbae2',
        '0732fedb0819967fe117247d41307ce1227b59ba', '13d2a3da88689c18f94ac806f08df680119ef156',
        '5a02809c18c673023a9774a0de03f625e284e826', '5c231fee6e45c0768662a0de5d8d4d740d8db0fb',
        '17a61d0512c38dd7eadd513da3f3971f87387bd0', '0dc80e95033973e4bfc4b2cc076de4ab032edaef',
        '8a60b71b5d5c792d13a5e85521f21aef33970fc8', '7a97d52aec049ad42a1f0ae724f9002dec544b3b',
        'e69de29bb2d1d6434b8b29ae775ad8c2e48c5391', 'c2a1ddb234e6f5778d3f5f72f7cfaf8ff3315961',
        '6a360aae7d865b4a2dd438d2278dfc6ce0fa2d24', '51ed5253d8622973e5618c55b68f072fd64f5c13',
        '2dd5f2f490fbd77e53c4d8d95d35c7cf7fb27af7', '35f5d30df29c59890cb5d75a1b64ad2ce628f76d',
        '5294122a84af6a7c15a68653069460013520358e', '31478d0ba164452bb69823b9211a371696f4e38e',
        '3fd82d8e0dc0f8708d942b5303289193521ee4bb', '5436fb97b01b2020d6306250f6751fe24fe0464d',
        'dc91b54e3f720f21208059a7542598bd7f88043e', '6a302414b1c3fa30d7f8992d654f9f5872efa12c',
        '54b08a6adab70f987b1b7912d08b135faf49f126', '04139453c5ce7481f9dbeebcee6b4995ea5cbf05',
        '42d511ac173322f6f25dea13347e79238a9260a8', '4702e6c81cef16e1a2ebdcf9abe589478bda4e4c',
        '44790dc7fe0007a8ee725f96248bc1ceeae44848', '45a5f1c226e7620a248dc17e3aa0b063323545d1',
        '26d0cd5128130e8f6ccb8fecc150eb976150226e', '30e578b1a081ddc8cdbbf327a7d28ddec5e6db48',
        '508a888815f8bec2600c77cbbd4e3ebd14d0e6c5', 'b286fcd9d0c3c18c7cded9f8eebd49bf3bfb837f',
        '4f57265165152c6ad557999aeb8d8e6173d79ea8', '88647609d447a54fe90ddf494c47fb84ba8cdb1e',
        '0c5fd1fa0dce1b3f3b070f992e87f1177bdac49c', '5e27325cf5e6c437ccd90ba52adf839bfa3d12cc',
        '7ea982e585fd4e8cbfd4763aa9ade959afa47471', '4ea011dc489726a7115dc62be6cf21ab2879150e',
        'aaa9518cccf9308f2b7161b12343f0331a37cb67', '6619589e78bb5e9567552d5882f83e243016cd4f',
        'e189b22045e502269e71cf916f1f458ab0dc4222', '46aa7e276de39c3c5c92dd4cf6510e6201603162',
        'af1d2238e98ea0ac4f37dd46eddce27a71ec6bfe', '23cfdcc001d8d3a41893739e8385dc0d48a30c3f',
        '8293e6cfad403ca59ed130d7f8ad653424ce3d2d', 'c3f6c1f48b5f8723785179176ef6748d80d3131e',
        'd036d9ec8990b0dd15ff8ddfa0250f09a130788f', '9f70a9c0b693b9e24ee1ef444614ec09f4660b88',
        'd25e23f934f2cef67cc54dec41e222a70a5d704d', '0aa4c14194a626fc6844a7a94f040fa48803f2bf',
        '0cf127e22cd869e96828e6fa253f66404a207c53', '7d9538b6a89fadd341532d7b029cd1ab4676289f',
        '509ce4e545ff16b1c105d6c359a41dac57ed48d4', '6d569963ff80b7e21072e0e318401554b1286231',
        'e73f753c190cfc4c8197a5591a816a4e0b3b4a7c', '6980db94a6ca37f37ff07c967f29b0f615cd3130',
        '9749d161560dde88879b54013a5e4d38e4245aa7', 'ef10dfc421e731a9cc42d9ce26f0d56b45186f65',
        'ec7e3cba37f1ba4dcb6306eefcf272bf4fc94dac', '99e3407bc4f60924fd9ab67e6bee60678530368a',
        '34debc3b2727ec745ac673a272ee9fef4e2018b1', '7435d57f1258003eb80a2d4fcb9b7931319eeb31',
        '72f7c390a7edfa13e2e2dea38a5a6c877ec86883', '1d6ebbd8a701a24d1058d1c500797899879c3b52',
        '1b375957c3dbdd9e5177eb2572da0658addacfd3', '0cca0f8b90664e88a44c139136e103f807a65e3d',
        'ea41d4163435d939fbe56251784c87ac5143a2b4', '0084676834ac99cd8610eae1e25e924f512f8223',
        '9951baac1529c9c2d925e52c7f56c9300f33e2ed', '47e331189e389b6f97fbd4a611c561a3e49a5219',
        'e97abd9adf2ee21c0e9ddca1c896a75555dbc76e', 'fcd8be914c8e22b0b230f0af49c0c44c2dfea02c',
        'dc2b90450c443341f3dbe8c20f1b6821a1306145', 'b661e71998673ee0556c7cf04287ed633db37734',
        '987aa4ce2e1bc10aadcf9850114f94b983c7c167', '3f2fc08a58a1ca89d98f8ae6734b1c0227ff8bdb',
        'eb077789ba78ec63ce8a80dc6df1392c40f0bc0a',
    }

    blob_types_counter = Counter(blob.type_ for blob in blobs.values())
    assert dict(blob_types_counter) == {BlobType.COMMIT: 9, BlobType.TREE: 26, BlobType.DATA: 42}


@dataclass
class ParseCommitCase:
    path: Path
    result: Commit

    def __str__(self) -> str:
        return str(self.path)


PARSE_COMMIT_TEST_CASES = [
    ParseCommitCase(
        path=OBJECTS_DIR / '0c' / '5fd1fa0dce1b3f3b070f992e87f1177bdac49c',
        result=Commit(
            tree_hash='13d2a3da88689c18f94ac806f08df680119ef156',
            parents=['44790dc7fe0007a8ee725f96248bc1ceeae44848'],
            author='Nick <ni_kolya93@mail.ru> 1568884031 +0300',
            committer='Nick <ni_kolya93@mail.ru> 1568884031 +0300',
            message='rename source file for fibonacci_numbers problem',
        )
    ),
    ParseCommitCase(
        path=OBJECTS_DIR / '26' / 'd0cd5128130e8f6ccb8fecc150eb976150226e',
        result=Commit(
            tree_hash='4ea011dc489726a7115dc62be6cf21ab2879150e',
            parents=[],
            author='Nick <ni_kolya93@mail.ru> 1568495358 +0300',
            committer='Nick <ni_kolya93@mail.ru> 1568495358 +0300',
            message='setup repository for public tasks'
        )
    )
]


@pytest.mark.parametrize('case', PARSE_COMMIT_TEST_CASES, ids=str)
def test_parse_commit(case: ParseCommitCase) -> None:
    answer = parse_commit(read_blob(case.path))

    assert answer == case.result


@dataclass
class ParseTreeCase:
    path: Path
    result: Dict[str, BlobType]

    def __str__(self) -> str:
        return str(self.path)


PARSE_TREE_TEST_CASES = [
    ParseTreeCase(
        path=OBJECTS_DIR / '47' / 'e331189e389b6f97fbd4a611c561a3e49a5219',
        result={
            '.grader-ci.yml': BlobType.DATA,
            '.img': BlobType.TREE,
            'custom.css': BlobType.DATA,
            'submit.py': BlobType.DATA,
            'test_utils.py': BlobType.DATA,
            '.gitignore': BlobType.DATA,
            '.gitmodules': BlobType.DATA,
        }
    ),
    ParseTreeCase(
        path=OBJECTS_DIR / 'e1' / '89b22045e502269e71cf916f1f458ab0dc4222',
        result={
            '.grader-ci.yml': BlobType.DATA,
            '.img': BlobType.TREE,
            'custom.css': BlobType.DATA,
            '.gitignore': BlobType.DATA,
            'submit.py': BlobType.DATA,
            'test_utils.py': BlobType.DATA,
            'collect_numbers': BlobType.TREE,
            'hello_world': BlobType.TREE,
            'prime_factorization': BlobType.TREE,
            'roman_to_decimal': BlobType.TREE,
            '.gitmodules': BlobType.DATA,
            'decimal_to_roman': BlobType.TREE,
            'fibonacci_numbers': BlobType.TREE,
        }
    )
]


@pytest.mark.parametrize('case', PARSE_TREE_TEST_CASES, ids=str)
def test_parse_tree(case: ParseTreeCase) -> None:
    answer = parse_tree(traverse_objects(OBJECTS_DIR), read_blob(case.path))

    assert {k: v.type_ for k, v in answer.children.items()} == case.result


def test_find_initial_commit() -> None:
    answer = find_initial_commit(traverse_objects(OBJECTS_DIR))

    assert answer.parents == []
    assert answer.tree_hash == '4ea011dc489726a7115dc62be6cf21ab2879150e'


@dataclass
class SearchFileCase:
    tree_blob: Path
    filename: str
    content: bytes

    def __str__(self) -> str:
        return str(self.tree_blob)


SEARCH_FILE_TEST_CASES = [
    SearchFileCase(
        tree_blob=OBJECTS_DIR / '35' / 'f5d30df29c59890cb5d75a1b64ad2ce628f76d',
        filename='.gitignore',
        content=b'.idea/\nvenv/\n*/.pytest_cache/\nlectures/.ipynb_checkpoints/\n*.so\n'
                b'*.pyd\n__pycache__\nbuild/\ndist/\n*.egg-info/\n',
    ),
    SearchFileCase(
        tree_blob=OBJECTS_DIR / '69' / '80db94a6ca37f37ff07c967f29b0f615cd3130',
        filename='decimal_to_roman.py',
        content=b'def decimal_to_roman(number: int) -> str:\n'
                b'    """\n'
                b'    :param number: decimal number\n'
                b'    :return: representation of argument in roman system\n'
                b'    """\n'
                b'    return \'\'\n',
    ),
]


@pytest.mark.parametrize('case', SEARCH_FILE_TEST_CASES, ids=str)
def test_search_file(case: SearchFileCase) -> None:
    answer = search_file(traverse_objects(OBJECTS_DIR), read_blob(case.tree_blob), case.filename)

    assert answer.type_ is BlobType.DATA
    assert answer.content == case.content


def test_total() -> None:
    blobs = traverse_objects(OBJECTS_DIR)
    commit = find_initial_commit(blobs)
    commit_tree = blobs[commit.tree_hash]

    file_blob = search_file(blobs, commit_tree, 'login.png')

    assert file_blob.content == (Path(__file__).parent / 'login.png').read_bytes()
