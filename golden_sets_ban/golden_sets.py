import typing

import pandas


def golden_sets_ban(marks: pandas.DataFrame, golden_set: pandas.DataFrame) -> typing.Set[int]:
    """
    Filter assessors by their quality of answers on golden sets.
    :param marks: dataframe with worker_id, document_id and label columns.
    :param golden_set: dataframe with true marks for golden set tasks.
    :return: set of all workers which gave more right than wrong answers on golden sets
    """
    return set()
