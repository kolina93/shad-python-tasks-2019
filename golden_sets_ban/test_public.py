import pandas

from .golden_sets import golden_sets_ban


def test_golden_sets_simple() -> None:
    data = pandas.DataFrame([
        {'worker_id': 18, 'document_id': 1, 'label': 1},
        {'worker_id': 18, 'document_id': 2, 'label': 2},
        {'worker_id': 18, 'document_id': 4, 'label': 2},
        {'worker_id': 19, 'document_id': 1, 'label': 1},
        {'worker_id': 19, 'document_id': 2, 'label': 3},
        {'worker_id': 20, 'document_id': 1, 'label': 4},
        {'worker_id': 20, 'document_id': 2, 'label': 3},
        {'worker_id': 20, 'document_id': 3, 'label': 5},
        {'worker_id': 20, 'document_id': 4, 'label': 1},
        {'worker_id': 21, 'document_id': 2, 'label': 2},
        {'worker_id': 21, 'document_id': 4, 'label': 1},
    ])

    golden_sets = pandas.DataFrame([
        {'document_id': 1, 'label': 1},
        {'document_id': 1, 'label': 3},
        {'document_id': 3, 'label': 5},
        {'document_id': 2, 'label': 2},
    ])

    assert golden_sets_ban(data, golden_sets) == {20}
