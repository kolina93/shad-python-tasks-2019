## Средний балл студента

### Условие
В датафрейме `df` находится информация об успеваемости студентов. Колонки `First Name` и `Last Name` - имя и фамилия студента, все остальные колонки - успеваемость по предметам.
Нужно написать функцию `gpa_top`, принимающую на вход датафрейм `df` и модифицирующую его следующим образом:

* Нужно добавить в `df` столбец с именем 'GPA', содержащим среднюю оценка студента. Разные студенты могут брать разный
набор курсов, поэтому среди оценок студентов может встречаться `NaN` (это означает, что студент не брал соответствующий
курс). Среднее считается среди тех курсов, которые студент брал.

* Далее нужно отсортировать датафрейм по убыванию `GPA`.

* В итоге нужно вернуть только те строчки датафрейма, в которых `GPA` не меньше `4` баллов.

### Пример
```python
In [1]: from gpa_top import gpa_top

In [2]: import pandas

In [3]: input_ = pandas.DataFrame(
   ...:         [
   ...:             ['Сергей', 'Королев', 4.0, 3.0, 4.5, 5.0],
   ...:             ['Михаил', 'Петров', float('nan'), 4.0, 4.0, 4.6],
   ...:             ['Алексей', 'Петров', 2.0, 2.5, 3.0, 3.5],
   ...:         ],
   ...:         columns=['Last Name', 'First Name', 'Algorithms', 'Discrete Analysis', 'C++', 'Python'],
   ...:         index=[0, 1, 2],
   ...:     )

In [4]: gpa_top(input_)

Out[4]:
  Last Name First Name  Algorithms  Discrete Analysis  C++  Python    GPA
1    Михаил     Петров         NaN                4.0  4.0     4.6  4.200
0    Сергей    Королев         4.0                3.0  4.5     5.0  4.125

In [5]:
```
