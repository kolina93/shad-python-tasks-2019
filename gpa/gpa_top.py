import pandas


def gpa_top(data: pandas.DataFrame) -> pandas.DataFrame:
    """
    Filter students with gpa >= 4 and sorts them by decreasing order.
    :param data: dataframe with student names and their marks by subjects.
    :return: dataframe with selected students
    """
    return data
