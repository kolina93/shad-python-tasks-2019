import pandas

from .gpa_top import gpa_top


def test_gpa_top() -> None:
    input_ = pandas.DataFrame(
        [
            ['Сергей', 'Королев', 4.0, 3.0, 4.5, 5.0],
            ['Михаил', 'Петров', float('nan'), 4.0, 4.0, 4.6],
            ['Алексей', 'Петров', 2.0, 2.5, 3.0, 3.5],
        ],
        columns=['Last Name', 'First Name', 'Algorithms', 'Discrete Analysis', 'C++', 'Python'],
        index=[0, 1, 2],
    )

    expected = pandas.DataFrame(
        [
            ['Михаил', 'Петров', float('nan'), 4.0, 4.0, 4.6, 4.2],
            ['Сергей', 'Королев', 4.0, 3.0, 4.5, 5.0, 4.125],
        ],
        columns=['Last Name', 'First Name', 'Algorithms', 'Discrete Analysis', 'C++', 'Python', 'GPA'],
        index=[1, 0],
    )

    assert expected.equals(gpa_top(input_))
