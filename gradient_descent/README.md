## Градиентный спуск

### Условие

<p>При обучении моделей в машинном обучении важным этапом является подборка параметров выбранной модели по обучающей выборке с помощью методов оптимизации. Одним из самых используемых подходов является применение <a href="https://en.wikipedia.org/wiki/Gradient_descent">градиентного спуска</a>. Это итеративный метод, который по функции от нескольких аргументов, которую нужно оптимизировать, имея текущее приближение, на следующей итерации двигается в сторону, обратную вектору-градиенту в текущей точке.</p>
<p>Рассмотрим простую модель <a href="https://en.wikipedia.org/wiki/Linear_regression">линейной регрессии</a>. Пусть у нас есть числовая матрица <code>X</code>, в которой <code>n</code> строк и <code>m</code> столбцов (строки соответствуют объектам обучающей выборки, столбцы - какому-либу числовому признаку) и вектор <code>y</code> размера <code>n</code> - вектор наблюдений объектов из обучения. В модели линейной регрессии предполагаем, что наблюдения и признаки связаны следующим законом: <img src="https://tex.s2cms.ru/svg/y%20%3D%20Xw%20%2B%20w_0%20%2B%20%5Cepsilon" alt="y = Xw + w_0 + \epsilon" />, где <img src="https://tex.s2cms.ru/svg/w" alt="w" /> - столбец размера <code>m</code>, <img src="https://tex.s2cms.ru/svg/w_0" alt="w_0" /> - смещение, а <img src="https://tex.s2cms.ru/svg/%5Cepsilon" alt="\epsilon" /> - случайный шум. Нужно подобрать <img src="https://tex.s2cms.ru/svg/w" alt="w" /> и <img src="https://tex.s2cms.ru/svg/w_0" alt="w_0" /> таким образом, чтобы сумма <img src="https://tex.s2cms.ru/svg/%5Cfrac%7B1%7D%7Bn%7D%5Csum_%7Bi%3D1%7D%5En%20(y_i-%3Cx_i%2Cw%3E-w_0)%5E2" alt="\frac{1}{n}\sum_{i=1}^n (y_i-&lt;x_i,w&gt;-w_0)^2" /> была минимальной, где <img src="https://tex.s2cms.ru/svg/%3Cx_i%2Cw%3E" alt="&lt;x_i,w&gt;" /> - скалярное произведение <code>i</code>-ой строки матрицы <code>X</code> и столбца <code>w</code>. Чтобы отдельно не рассматривать <img src="https://tex.s2cms.ru/svg/w_0" alt="w_0" />, бывает удобно считать, что в матрице <img src="https://tex.s2cms.ru/svg/X" alt="X" /> есть константный столбец, состоящий из одних единиц, тогда <img src="https://tex.s2cms.ru/svg/w_0" alt="w_0" /> можно занести внутрь столбца <img src="https://tex.s2cms.ru/svg/w" alt="w" />, что и предполагается ниже.</p>
<p>Воспользумся для решения методом градиентного спуска. Пусть <img src="https://tex.s2cms.ru/svg/w_1" alt="w_1" />, …, <img src="https://tex.s2cms.ru/svg/w_m" alt="w_m" /> - веса из столбца <img src="https://tex.s2cms.ru/svg/w" alt="w" />, которые нужно подобрать. Функция для оптимизации <img src="https://tex.s2cms.ru/svg/f(w_1%2C%20...%2C%20w_m)%3D%5Cfrac%7B1%7D%7Bn%7D%5Csum_%7Bi%3D1%7D%5En%20(%5Csum_%7Bj%3D1%7D%5Em%20x_%7Bij%7Dw_j%20-%20y_i)%5E2" alt="f(w_1, ..., w_m)=\frac{1}{n}\sum_{i=1}^n (\sum_{j=1}^m x_{ij}w_j - y_i)^2" />, где <img src="https://tex.s2cms.ru/svg/x_%7Bij%7D" alt="x_{ij}" /> - элемент матрицы <img src="https://tex.s2cms.ru/svg/X" alt="X" /> в <code>i</code>-ом ряду и <code>j</code>-ом столбце. Частная производная по <img src="https://tex.s2cms.ru/svg/w_k" alt="w_k" /> будет равна <img src="https://tex.s2cms.ru/svg/%5Cfrac%20%7B%5Cpartial%20f(w)%7D%7B%5Cpartial%20w_k%7D%3D%5Cfrac%7B2%7D%7Bn%7D%5Csum_%7Bi%3D1%7D%5En%20x_%7Bik%7D(%5Csum_%7Bj%3D1%7D%5Em%20x_%7Bij%7Dw_j%20-%20y_i)" alt="\frac {\partial f(w)}{\partial w_k}=\frac{2}{n}\sum_{i=1}^n x_{ik}(\sum_{j=1}^m x_{ij}w_j - y_i)" />. Если за <img src="https://tex.s2cms.ru/svg/h" alt="h" /> обозначить шаг градиента, то получим, что формулы для итерации шага градиентного спуска будут иметь вид <img src="https://tex.s2cms.ru/svg/w_%7Bk%7D%5E%7Bnew%7D%3Dw_k%5E%7Bold%7D%20-%20%5Cfrac%7B2h%7D%7Bn%7D%5Csum_%7Bi%3D1%7D%5En%20x_%7Bik%7D(%5Csum_%7Bj%3D1%7D%5Em%20x_%7Bij%7Dw_j%5E%7Bold%7D%20-%20y_i)" alt="w_{k}^{new}=w_k^{old} - \frac{2h}{n}\sum_{i=1}^n x_{ik}(\sum_{j=1}^m x_{ij}w_j^{old} - y_i)" />.</p>
<p>Вам нужно реализовать функцию <code>gradient_descent</code>, которая будет реализовывать описанные выше выкладки: функция принимает матрицу <code>x</code>, предсказания <code>y</code>, шаг градиентного спуска <code>step</code> (соответствует <img src="https://tex.s2cms.ru/svg/h" alt="h" /> в формулах выше) и количество итераций <code>iterations</code>. Переданный <code>x</code> не содержит константного столбца с единицами, его нужно будет добавить к матрице. Далее функция должна инициализировать <img src="https://tex.s2cms.ru/svg/w" alt="w" /> нулями (хоть это и не лучший способ начальной инициализации весов) и провести <code>iterations</code> итераций градиентного спуска с шагом <code>step</code> и вернуть получившийся вектор весов <img src="https://tex.s2cms.ru/svg/w" alt="w" /> (на первом месте в нем должен идти коэффициент, соответствующий <img src="https://tex.s2cms.ru/svg/w_0" alt="w_0" />).</p>
<p>Для реализации итерации градиентного спуска нужно воспользоваться векторными вычислениями над матрицами, предоставляемые <code>numpy</code>. Не нужно писать явно питоновские циклы, это будет работать гораздо медленнее, и, скорее всего, не уложится по времени на большом приватном тесте.</p>

### Пример

```python
In [1]: from gradient_descent import gradient_descent

In [2]: import numpy

In [3]: X = numpy.random.uniform(0, 1, 20).reshape((10, 2))

In [4]: X
Out[4]:
array([[0.16921653, 0.16751744],
       [0.19107484, 0.52132404],
       [0.95481319, 0.28139501],
       [0.55142274, 0.64295866],
       [0.51327395, 0.95568826],
       [0.04663014, 0.80912737],
       [0.31058601, 0.63755175],
       [0.44238771, 0.31666636],
       [0.54952749, 0.34149494],
       [0.76035986, 0.58320197]])

In [5]: y = (X @ numpy.array([2.0, 2.0]).reshape((2, 1))).reshape((10,)) + 3.0

In [6]: y = y + 0.05 * numpy.random.randn(10)

In [7]: y
Out[7]:
array([3.67908583, 4.40569794, 5.46989491, 5.36673902, 5.94976763,
       4.74302722, 4.9411189 , 4.52898721, 4.69163928, 5.69425422])

In [8]: gradient_descent(X, y, 0.001, 100000)
Out[8]: array([2.98574667, 1.97138774, 2.04734406])

In [9]:
```
