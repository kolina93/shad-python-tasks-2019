import numpy


def gradient_descent(x: numpy.ndarray, y: numpy.ndarray, step: numpy.float64, iterations: int) -> numpy.ndarray:
    """
    Implements gradient descent for linear regression weights selection.
    :param x: feature matrix
    :param y: ground truth values
    :param step: step of gradient descent iteration
    :param iterations: numbers of iterations
    :return: result weights of model
    """
