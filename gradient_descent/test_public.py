import numpy

from numpy.testing import assert_allclose

from .gradient_descent import gradient_descent


def test_gradient_descent() -> None:
    numpy.random.seed(42)
    N, W = 1000, 5
    generated = numpy.random.uniform(0, 1, N * W).reshape((N, W))
    weights = [2.5, 4.3, 7.6, 1.2, 1.5]
    intercept = 3.0

    y = (generated @ numpy.array(weights).reshape((W, 1))).reshape((N,)) + intercept
    y = y + 0.05 * numpy.random.randn(N)

    step = 0.001
    assert_allclose(gradient_descent(generated, y, step, 10), [0.2245, 0.115, 0.1197, 0.1221, 0.1136, 0.1148], rtol=1e-2)
    assert_allclose(
        gradient_descent(generated, y, step, 1000),
        [4.867, 2.47849, 2.8062, 3.23256, 2.29416, 2.3578],
        rtol=1e-2,
    )
    assert_allclose(gradient_descent(generated, y, step, 100000), [intercept] + weights, rtol=1e-2)
