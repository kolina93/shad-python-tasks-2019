import heapq
import sys

from typing import List, Tuple, Union, cast, Sequence, Deque


EdgeWithCost = Tuple[int, int, float]
Edge = Tuple[int, int]

VertexType = Union[int, Tuple[int, float]]


def graph_to_edges(n: int, graph: Sequence[Union[Edge, EdgeWithCost]]) -> List[List[VertexType]]:
    edges: List[List[VertexType]] = [[] for _ in range(n)]

    for item in graph:
        if len(item) == 2:
            u, v = cast(Edge, item)
            u -= 1
            v -= 1
            edges[u].append(v)
            edges[v].append(u)
        else:
            u, v, c = cast(EdgeWithCost, item)
            u -= 1
            v -= 1
            edges[u].append((v, c))
            edges[v].append((u, c))

    return edges


def bfs(n: int, graph: List[Edge], start_vertex: int) -> List[int]:
    result = [-1] * n
    edges = cast(List[List[int]], graph_to_edges(n, graph))

    vertexes_to_visit = Deque[int]()
    start_vertex -= 1
    vertexes_to_visit.append(start_vertex)
    result[start_vertex] = 0

    while vertexes_to_visit:
        current_vertex = vertexes_to_visit.popleft()

        for next_vertex in edges[current_vertex]:
            if result[next_vertex] != -1:
                continue
            vertexes_to_visit.append(next_vertex)
            result[next_vertex] = result[current_vertex] + 1

    return result


def dfs(n: int, graph: List[Tuple[int, int]], start_vertex: int) -> List[int]:
    sys.setrecursionlimit(100500)

    result = [-1] * n
    edges = cast(List[List[int]], graph_to_edges(n, graph))

    start_vertex -= 1

    order = []
    result[start_vertex] = 0

    def dfs_inner(vertex: int) -> None:
        order.append(vertex)

        for next_vertex in edges[vertex]:
            if result[next_vertex] == -1:
                result[next_vertex] = len(order)
                dfs_inner(next_vertex)

    dfs_inner(start_vertex)

    return result


def dijkstra(n: int, graph: List[Tuple[int, int, float]], start_vertex: int) -> List[float]:
    result: List[float] = [-1] * n
    edges = cast(List[List[Tuple[int, float]]], graph_to_edges(n, graph))

    start_vertex -= 1
    distances: List[Tuple[float, int]] = [(0, start_vertex)]
    values: List[float] = [-1] * n
    values[start_vertex] = 0

    while distances:
        value: float
        vertex: int
        value, vertex = heapq.heappop(distances)

        if result[vertex] != -1:
            continue
        result[vertex] = value

        for next_vertex, cost in edges[vertex]:
            if values[next_vertex] == -1:
                values[next_vertex] = value + cost
                heapq.heappush(distances, (value + cost, next_vertex))
            elif values[next_vertex] > value + cost:
                values[next_vertex] = value + cost
                heapq.heappush(distances, (value + cost, next_vertex))

    return result
