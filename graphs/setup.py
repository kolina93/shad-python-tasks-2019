from setuptools import setup, Extension

module1 = Extension('graph', sources=['graphmodule.cpp'], extra_compile_args=['-std=c++17'])

setup(name='Graph Module',
      version='1.0',
      description='This is a graph module with DFS, BFS and Dijkstra algorithms',
      ext_modules=[module1])
