import importlib
import os
import subprocess
import sys

from typing import List, Tuple, cast
from typing_extensions import Protocol

import pytest


class GraphModuleType(Protocol):
    def bfs(self, n: int, graph: List[Tuple[int, int]], start_vertex: int) -> List[int]: pass
    def dfs(self, n: int, graph: List[Tuple[int, int]], start_vertex: int) -> List[int]: pass
    def dijkstra(self, n: int, graph: List[Tuple[int, int, float]], start_vertex: int) -> List[float]: pass


@pytest.fixture(scope='module')
def graph_module() -> GraphModuleType:
    subprocess.run([sys.executable, 'setup.py', 'build_ext', '--inplace'], check=True)

    sys.path.append(os.getcwd())
    return cast(GraphModuleType, importlib.import_module('graph'))


def test_bfs(graph_module: GraphModuleType) -> None:
    assert graph_module.bfs(4, [(1, 2), (2, 3), (3, 4)], 1) == [0, 1, 2, 3]
    assert graph_module.bfs(4, [(1, 2), (2, 3), (3, 4), (1, 4)], 1) == [0, 1, 2, 1]
    assert graph_module.bfs(1, [], 1) == [0]
    assert graph_module.bfs(5, [(1, 2), (2, 5), (3, 4)], 1) == [0, 1, -1, -1, 2]
    assert graph_module.bfs(4, [(1, 2), (2, 3), (3, 4), (1, 4)], 3) == [2, 1, 0, 1]


def test_dfs(graph_module: GraphModuleType) -> None:
    assert graph_module.dfs(4, [(1, 2), (2, 3), (3, 4)], 1) == [0, 1, 2, 3]
    assert graph_module.dfs(4, [(1, 2), (2, 3), (3, 4), (1, 4)], 1) == [0, 1, 2, 3]
    assert graph_module.dfs(1, [], 1) == [0]
    assert graph_module.dfs(5, [(1, 2), (2, 5), (3, 4)], 1) == [0, 1, -1, -1, 2]
    assert graph_module.dfs(4, [(1, 2), (2, 3), (3, 4), (1, 4)], 3) == [2, 1, 0, 3]


def test_dijkstra(graph_module: GraphModuleType) -> None:
    assert graph_module.dijkstra(4, [(1, 2, 3), (2, 3, 4.3), (3, 4, 5)], 1) == [0, 3, 7.3, 12.3]
    assert graph_module.dijkstra(4, [(1, 2, 3.5), (2, 3, 4), (3, 4, 5), (1, 4, 6)], 1) == [0, 3.5, 7.5, 6]
    assert graph_module.dijkstra(1, [], 1) == [0]
    assert graph_module.dijkstra(5, [(1, 2, 1), (2, 5, 0.5), (3, 4, 10)], 1) == [0, 1, -1, -1, 1.5]
    assert graph_module.dijkstra(4, [(1, 2, 3.5), (2, 3, 4), (3, 4, 5), (1, 4, 6)], 3) == [7.5, 4, 0, 5]
