## HELLO WORLD

### Условие

Нужно написать функцию, которая будет возвращать строчку Hello world!

### Пример

```python
>>> get_hello_world()
Hello world!
```
