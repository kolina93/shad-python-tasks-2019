## Instance counter

### Условие
Реализуйте метакласс `InstanceSemaphore`, который бы следил за количеством инстансов классов, использующих его.
В свойстве класса, использующего этот метакласс, указывается свойство `__max_instance_count__`,
и при попытке инстациировать новый объект класса, если кол-во уже инстанциированных инстансов этого класса равно `__max_instance_count__`, должно выбрасываться исключение `TypeError`.  Обратите внимание, что число объектов для каждого класса регулируется отдельно. При удалении объекта счетчик не сбрасывается, т.е. нужно ограничивать число объектов за все время жизни каждого класса.

### Пример
```python
In [1]: from semaphore import InstanceSemaphore

In [2]: class A(metaclass=InstanceSemaphore):
   ...:     __max_instance_count__ = 1
   ...:

In [3]: a = A()

In [4]: a
Out[4]: <__main__.A at 0x7f2ac6309fd0>

In [5]: try:
   ...:     A()
   ...: except TypeError:
   ...:     print("Raise TypeError as expected")
   ...:
Raise TypeError as expected

In [6]:
```
