from typing import ClassVar
from typing_extensions import Protocol


class Property(Protocol):
    __max_instance_count__: ClassVar[int]


class InstanceSemaphore(type):
    """
    Metaclass which keeps track of the current number of instances.
    For every class using this metaclass number of instances must be restricted
    to __max_instance_count__ value for this class.
    If number of created instances is equal to __max_instance_count__
    attempt to instantiate an extra object will result in TypeError.
    """
