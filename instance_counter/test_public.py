import pytest

from typing import Type

from .semaphore import InstanceSemaphore, Property


class A(metaclass=InstanceSemaphore):
    __max_instance_count__ = 2


class B(metaclass=InstanceSemaphore):
    __max_instance_count__ = 1


@pytest.mark.parametrize('cls', [A, B])
def test_semaphore(cls: Type[Property]) -> None:
    for _ in range(cls.__max_instance_count__):
        _ = cls()

    with pytest.raises(TypeError):
        cls()
