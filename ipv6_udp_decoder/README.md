## Разбор сетевого пакета

### Условие
Вам дан набор байт, представляющий собой [IPv6-пакет](https://en.wikipedia.org/wiki/IPv6_packet). Для данного задания
считаем, что пакет не содержит расширенных заголовков (Routing, Fragment и т.д.), т.е. там есть только фиксированный
заголовок и payload. Также известно, что payload этого пакет представляет собой
[UDP-пакет](https://en.wikipedia.org/wiki/User_Datagram_Protocol). UPD-пакет тоже, в свою очередь, содержит
фиксированный заголовок и payload. Вам нужно написать функцию `parse_udp`, которая принимает `bytes`-аргумент `dump` -
содержимое IPv6-пакета и возвращает `tuple` из 3 элементов: ipv6-адрес источника пакета, ipv6-адрес приемника пакета и
содержимое UDP-пакета. IPv6-адреса должны соответствовать этим
[правилам](https://en.wikipedia.org/wiki/IPv6_address#Recommended_representation_as_text).

### Пример
```python
In [1]: from decoder import parse_udp

In [2]: dump = b"`\x00\x00\x00\x00\x12\x11@*\x02\x06\xb8\x00\n\x00\x00\x00\x00\x00\x00\x00\x00\x00\n*\x04NB\x00\x1b\x00\x00\x00\x00\x00\x00\x00\x00\x02#\x08h\x08h\x00\nH\x1eI'm teapot"
   ...:

In [3]: parse_udp(dump)
Out[3]: ('2a02:6b8:a::a', '2a04:4e42:1b::223', b"I'm teapot")

In [4]:
```
