from typing import Tuple


def parse_udp(dump: bytes) -> Tuple[str, str, bytes]:
    """
    Decodes an IPv6 udp packet.
    :param dump: set of bytes of a valid IPv6 udp packet
    :return: tuple of 3 elements: (src IPv6 address, dst IPv6 address, udp content)
    """
    pass
