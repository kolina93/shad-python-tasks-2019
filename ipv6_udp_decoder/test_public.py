from .decoder import parse_udp


def test_simple() -> None:
    dump = (
            b"`\x00\x00\x00\x00\x12\x11@*\x02\x06\xb8\x00\n\x00\x00\x00\x00\x00\x00\x00\x00\x00\n*\x04NB\x00\x1b\x00" +
            b"\x00\x00\x00\x00\x00\x00\x00\x02#\x08h\x08h\x00\nH\x1eI'm teapot"
    )
    assert parse_udp(dump) == ('2a02:6b8:a::a', '2a04:4e42:1b::223', b"I'm teapot")
