## Путь в лабиринте

### Условие
Есть лабиринт, заданный клеточным полем, в котором `w` строк и `h` столбцов. Мышь изначально находится в клетке с координатами `(x,y)`. Она может передвигаться по границам клетки влево, вправо, вверх и вниз, если в клетке нет преграды и она не залазит на границу. Нужно для каждой клетки `(i,j)` лабиринта найти длину минимального пути от клетки `(x,y)` или определить, что до этой клетки добраться невозможно. Лабиринт задается как массив из `w` строк, каждая из которых имеет длину `h` (`1<=w,h<=100`). Символы в строках могут быть такими: `'.'` - свободная клетка, `'#'` - клетка с преградой, `'*'` - клетка, в которой находится мышка. Нужно вернуть двумерный массив `arr` размера `w` на `h`, в `arr[i][j]` должна быть записана длина кратчайшего пути от исходной клетки до `(i, j)` или `-1`, если такого пути не существует.

### Пример

```python
>>> labyrinth = ['...', '...', '..*', '.#.', '..#']
>>> shortest_path_lengths(labyrinth)
[[4, 3, 2], [3, 2, 1], [2, 1, 0], [3, -1, 1], [4, 5, -1]]
```
