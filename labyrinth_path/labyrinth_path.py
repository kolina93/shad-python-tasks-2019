from typing import List


def shortest_path_lengths(labyrinth: List[str]) -> List[List[int]]:
    """
    Find shortest pathes from a given point to all the others in a labyrinth
    :param labyrinth: description of the labyrinth as an array of strings (. - free points, # - occupied, * - start)
    :return: 2d-array of all lengths (-1 for unreachable points)
    """
    return []
