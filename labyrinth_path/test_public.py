from .labyrinth_path import shortest_path_lengths


def test_shortest_path_lengths() -> None:
    labyrinth = ['...', '...', '..*', '.#.', '..#']
    expected = [[4, 3, 2], [3, 2, 1], [2, 1, 0], [3, -1, 1], [4, 5, -1]]
    assert shortest_path_lengths(labyrinth) == expected
