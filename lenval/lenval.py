from typing import Iterable


def decode(value: bytes) -> Iterable[str]:
    """
    Decodes lenval-encoded sequence of bytes.
    :param value: sequence of bytes
    :return: iterator over all decoded strings
    """


def encode(values: Iterable[str]) -> bytes:
    """
    Applies lenval-encoding to a sequence of strings.
    :param values: iterator over strings
    :return: lenval-encoded bytes
    """
