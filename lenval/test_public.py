from .lenval import encode, decode


def test_encode() -> None:
    assert encode(['a', 'b']) == b'\x01a\x01b'

    assert encode([]) == b''

    expected = (
            b'\x0c\xd0\xbf\xd1\x80\xd0\xb8\xd0\xb2\xd0\xb5\xd1\x82\x0c\xd0' +
            b'\xbc\xd0\xb5\xd0\xb4\xd0\xb2\xd0\xb5\xd0\xb4'
    )
    assert encode(['привет', 'медвед']) == expected

    assert encode(('a' * 255,)) == b'\xff' + b'a' * 255


def test_decode() -> None:
    data = (
            b'\x0c\xd0\xbf\xd1\x80\xd0\xb8\xd0\xb2\xd0\xb5\xd1\x82\x0c\xd0' +
            b'\xbc\xd0\xb5\xd0\xb4\xd0\xb2\xd0\xb5\xd0\xb4'
    )
    assert list(decode(data)) == ['привет', 'медвед']

    assert list(decode(b'')) == []

    assert list(decode(b'\x01a\x01b')) == ['a', 'b']
