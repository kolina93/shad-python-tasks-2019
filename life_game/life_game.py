from typing import List, Tuple


class LifeEmulation:
    """
    Class for emulating a game called "Life"
    """
    def __init__(self, start_state: List[List[int]], maximum_value: int) -> None:
        """
        :param start_state: initial values of all cells
        :param maximum_value: restriction for cell's values
        """

    def __str__(self) -> str:
        pass

    def get_neighbours_indexes(self, i: int, j: int) -> List[Tuple[int, int]]:
        """
        Returns coordinates of all neighbours for a given cell
        :param i: row index
        :param j: column index
        :return: list of coordinates
        """

    def get_neighbours_count_with_positive_value(self, i: int, j: int) -> int:
        """
        Returns count of neighbours with current positive state
        :param i: row index
        :param j: column index
        :return: number
        """

    def get_next_state(self, i: int, j: int) -> int:
        """
        Returns next state for a given cell
        :param i: row index
        :param j: column index
        :return: next state
        """

    def iterate(self) -> None:
        """
        Makes one iteration of game
        """

    @property
    def current_state(self) -> List[List[int]]:
        """
        Returns current state of all cells
        :return: list of rows with all values
        """
