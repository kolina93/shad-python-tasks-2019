from .life_game import LifeEmulation


def test_simple() -> None:
    start_board = [[0, 0, 0, 1], [0, 1, 1, 1], [1, 0, 1, 0], [1, 1, 1, 0]]

    obj = LifeEmulation(start_board, 10)

    assert obj.get_neighbours_indexes(2, 3) == [(1, 2), (1, 3), (2, 2), (3, 2), (3, 3)]
    assert obj.get_neighbours_count_with_positive_value(2, 3) == 4
    assert obj.get_next_state(2, 3) == 0

    obj.iterate()

    assert obj.current_state == [[0, 0, 0, 2], [0, 2, 0, 2], [2, 0, 0, 0], [2, 0, 2, 0]]
    assert str(obj) == '0 0 0 2\n0 2 0 2\n2 0 0 0\n2 0 2 0'
