from typing import Sequence


def lower_bound(nums: Sequence[int], value: int) -> int:
    """
    Find lower bound of value in sorted sequence
    :param nums: sequence of integers. Could be empty
    :param value: integer to find
    :return: the greatest element from nums that is not less than value, or value if no such element exists
    """
    return value
