from copy import deepcopy
from dataclasses import dataclass
from typing import Sequence

import pytest

from .lower_bound import lower_bound


@dataclass
class Case:
    nums: Sequence[int]
    value: int
    result: int

    def __str__(self) -> str:
        return f'find_{self.value}_in_{self.nums}'


TEST_CASES = [
    Case(nums=[], value=2, result=2),
    Case(nums=[1], value=2, result=1),
    Case(nums=[1, 3, 5], value=0, result=0),
    Case(nums=[1, 3, 5], value=2, result=1),
    Case(nums=[1, 3, 5], value=4, result=3),
    Case(nums=[1, 3, 5], value=6, result=5),
    Case(nums=[1, 3, 5], value=1, result=1),
    Case(nums=[1, 3, 5], value=3, result=3),
    Case(nums=[1, 3, 5], value=5, result=5),
    Case(nums=range(1, 5, 2), value=4, result=3),
    Case(nums=range(1, 5, 2), value=5, result=3)
]


@pytest.mark.parametrize('t', TEST_CASES, ids=str)
def test_lower_bound(t: Case) -> None:
    nums_copy = deepcopy(t.nums)

    answer = lower_bound(nums_copy, t.value)

    assert t.nums == nums_copy, "You shouldn't change inputs"
    assert answer == t.result
