import typing

import pandas


def majority_vote(data: pandas.DataFrame) -> typing.Tuple[pandas.Series, pandas.Series]:
    """
    Calculates majority vote statistics for documents and assessors.
    :param data: dataframe with worker_id, document_id and label column.
    :return: first series - majority vote indexed by document ids,
             second series - accuracy of assessors indexed by worker ids
    """
    return pandas.Series(), pandas.Series()
