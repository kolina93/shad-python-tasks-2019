import math

import pandas

from .majority_vote import majority_vote


def test_majority_vote_public() -> None:
    data = pandas.DataFrame([
        {'worker_id': 18, 'document_id': 1, 'label': 1},
        {'worker_id': 18, 'document_id': 2, 'label': 2},
        {'worker_id': 18, 'document_id': 4, 'label': 2},
        {'worker_id': 19, 'document_id': 1, 'label': 1},
        {'worker_id': 19, 'document_id': 2, 'label': 3},
        {'worker_id': 20, 'document_id': 1, 'label': 4},
        {'worker_id': 20, 'document_id': 2, 'label': 3},
        {'worker_id': 20, 'document_id': 3, 'label': 5},
        {'worker_id': 20, 'document_id': 4, 'label': 1},
        {'worker_id': 21, 'document_id': 2, 'label': 2},
        {'worker_id': 21, 'document_id': 4, 'label': 1},
    ])
    results = majority_vote(data)

    assert results[0].to_dict() == {1: 1, 3: 5, 4: 1}

    workers_accuracy_expected = {18: 0.5, 19: 1.0, 20: 2.0 / 3, 21: 1.0}
    workers_accuracy = results[1].to_dict()
    assert len(workers_accuracy_expected) == len(workers_accuracy)
    assert all(
        math.isclose(value, workers_accuracy[key], rel_tol=0.001) for key, value in workers_accuracy_expected.items()
    )
