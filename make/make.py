from dataclasses import dataclass
from typing import List


@dataclass
class MakeTarget:
    '''Basic description for a makefile's target'''
    name: str
    dependencies: List[str]
    command: str


def resolve_make_order(targets: List[MakeTarget]) -> List[str]:
    """
    Search for any possible order in which targets can be executed.
    :param targets: descripton of targets
    :return: any possible order of commands if any exists otherwise ValueError must be thrown
    """
    return []
