from copy import deepcopy
from dataclasses import dataclass
from typing import List, Set

import pytest

from .make import MakeTarget, resolve_make_order


def is_execution_order_valid(targets: List[MakeTarget], commands: List[str]) -> bool:
    if len(commands) != len(targets):
        return False

    command_to_target = {target.command: target.name for target in targets}
    name_to_dependencies = {target.name: target.dependencies for target in targets}

    ready_targets: Set[str] = set()

    for command in commands:
        target_name = command_to_target[command]
        if any(d not in ready_targets for d in name_to_dependencies[target_name]):
            return False

        ready_targets.add(target_name)

    return True


@dataclass
class Case:
    targets: List[MakeTarget]
    must_throw: bool = False


TEST_CASES = [
    Case(
        targets=[],
    ),
    Case(
        targets=[
            MakeTarget(name='a', dependencies=[], command='ls /'),
            MakeTarget(name='b', dependencies=[], command='ls -la /'),
         ]
    ),
    Case(
        targets=[
            MakeTarget(name='a', dependencies=['b'], command='wc -l ~/test/test.txt'),
            MakeTarget(name='b', dependencies=['c'], command='touch ~/test/test.txt'),
            MakeTarget(name='c', dependencies=[], command='mkdir ~/test'),
        ]
    ),
    Case(
        targets=[
            MakeTarget(name='a', dependencies=['b'], command='ls /'),
            MakeTarget(name='b', dependencies=['a'], command='ls -la /'),
        ],
        must_throw=True,
    ),
]


@pytest.mark.parametrize('case', TEST_CASES)
def test_make_resolve_order(case: Case) -> None:
    if case.must_throw:
        with pytest.raises(ValueError):
            resolve_make_order(case.targets)
        return

    targets_copy = deepcopy(case.targets)
    commands = resolve_make_order(case.targets)

    assert targets_copy == case.targets
    assert is_execution_order_valid(case.targets, commands)
