from abc import abstractmethod
from typing import Iterable, TypeVar, List, Any
from typing_extensions import Protocol


C = TypeVar('C', bound='Comparable')


class Comparable(Protocol):
    @abstractmethod
    def __eq__(self, other: Any) -> bool:
        pass

    @abstractmethod
    def __lt__(self: C, other: C) -> bool:
        pass

    def __gt__(self: C, other: C) -> bool:
        return (not self < other) and self != other

    def __le__(self: C, other: C) -> bool:
        return self < other or self == other

    def __ge__(self: C, other: C) -> bool:
        return (not self < other)


T = TypeVar('T', bound=Comparable)


def merge_sort(iterable: Iterable[T]) -> Iterable[List[T]]:
    """
    Generates all intermediate results of MergeSort
    :param iterable: input elements
    :return: generator over all intermediate lists
    """
