from dataclasses import dataclass
from inspect import isgeneratorfunction
from typing import List

import pytest

from .merge_sort import merge_sort


@dataclass
class Case:
    input_: List[int]
    output: List[List[int]]


TEST_CASES = [
    Case([8, 6, 7, 9], [[6, 8, 7, 9], [6, 7, 8, 9]]),
    Case([2, 1], [[1, 2]]),
    Case(
        [2, 1, 5, 8, 11, 12, 0, 1],
        [[1, 2, 5, 8, 11, 12, 0, 1], [1, 2, 5, 8, 0, 1, 11, 12], [0, 1, 1, 2, 5, 8, 11, 12]],
    ),
]


def test_function_is_generator() -> None:
    assert isgeneratorfunction(merge_sort), "merge_sort must be a generator"


@pytest.mark.parametrize('case', TEST_CASES)
def test_merge_sort(case: Case) -> None:
    assert case.output == list(merge_sort(case.input_))
