## OpenStreetMap

### Условие

[OpenStreetMap](https://www.openstreetmap.org) - open-source проект, целью которого является создать подробную
online-карту мира силами сообщества. Для этого используются данные персональных GPS-трекеров, аэрофотографии,
видеозаписи, спутниковые снимки и панорамы улиц, предоставленные некоторыми компаниями, а также знания человека,
рисующего карту. В этой задаче вам предстоит научиться доставать информацию о местах с их
[поискового движка](https://nominatim.openstreetmap.org). Если там ввести в поиск описание некоторого места
(например, "Минск"), то покажется следующая информация:

![OSM search](osm_search.png)

Далее можно кликнуть на один из результатов и на клику по "Details" будет такая информация:

![OSM result](osm_result.png)

Нужно написать функцию `get_place_data`, которая принимает один параметр `place` - название местности (например,
"Минск") и возвращает следующую информацию для первых 5 первых результатов поиска `place` (если результатов меньше 5,
то возвращает информацию для всех результатов) в виде структуры `OsmResult` с полями:

* `name` - имя результата и `type` - тип результата (это будет "Минск, Беларусь" и "City" для первого результата ниже):

![Name and type](name_and_type.png)

* `lat`, `lon` - координаты результата в виде `float`, доступные по переходу на кнопку "Details" (это будет
`53.902334` и `27.5618791` для примера ниже)

![Coordinates](coordinates.png)

* `extra_tags`, `address_tags`: содержимое полей `Extra Tags` и `Address Tags` в виде `dict`, для примера ниже должно
быть:

```python
{
    'capital': 'yes',
    'name:prefix': 'город',
    'place': 'city',
    'population': '1982444',
    'website': 'http://minsk.gov.by/',
    'wikidata': 'Q2280',
    'wikipedia': 'be:Горад Мінск',
    'wikipedia:be': 'Мінск',
    'wikipedia:en': 'Minsk',
    'wikipedia:pl': 'Mińsk',
}

{'continent': 'Europe', 'country': 'BY'}
```

![Tags](tags.png)

* `admin_level`: административный уровень результата.

Полные примеры ответа можно посмотреть в тестовых файлах для публичных тестов.

### Пример
```python
In [1]: from osm_solution import get_place_data

In [2]: get_place_data('минск')
Out[2]:
[OsmResult(name='Минск, Беларусь', type_='City', lat=53.902334, lon=27.5618791, extra_tags={'capital': 'yes', 'name:prefix': 'город', 'place': 'city', 'population': '1982444', 'website': 'https://minsk.gov.by/', 'wikidata': 'Q2280', 'wikipedia': 'be:Мінск', 'wikipedia:be': 'Мінск', 'wikipedia:en': 'Minsk', 'wikipedia:pl': 'Mińsk'}, address_tags={'continent': 'Europe', 'country': 'BY'}, admin_level=4),
 OsmResult(name='Минск, Беларусь', type_='State', lat=53.8791852, lon=27.8995566219764, extra_tags={}, address_tags={}, admin_level=4),
 OsmResult(name='Minsk, Беларусь', type_='State', lat=53.8395964, lon=27.4711051089346, extra_tags={}, address_tags={}, admin_level=4),
 OsmResult(name='Minsk, кв. Модерно предградие, Люлин, Sofia City, София, 1324, Болгария', type_='Residential', lat=42.7227445, lon=23.2693328, extra_tags={'lanes': '2', 'maxspeed': '40', 'surface': 'asphalt'}, address_tags={}, admin_level=15),
 OsmResult(name='Minsk, Nueva Granada, Loja, Лоха, 110107, Эквадор', type_='Residential', lat=-3.9808378, lon=-79.1968247, extra_tags={}, address_tags={}, admin_level=15)]

In [3]:
```

### Примечание
**Для того, чтобы выдача совпадала с тем, что в тестах, вам может понадобиться поиграться с языковыми настройками вашего браузера, чтобы правильно выставлялся заголовок Accept-Language для запросов. Про это можно почитать [здесь](https://www.w3.org/International/questions/qa-lang-priorities).**

Для запросов нужно использовать библиотеку `requests`, для парсинга нужно использовать библиотеки
`BeautifulSoup` или `lxml`. Для понимания того, какие именно запросы делаются в браузере, могут быть полезны
[Developer Tools](https://en.wikipedia.org/wiki/Web_development_tools), как показывалось на лекции про работу с вебом.
