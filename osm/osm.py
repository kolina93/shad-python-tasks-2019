from dataclasses import dataclass
from typing import Dict, List


@dataclass
class OsmResult:
    name: str
    type_: str
    lat: float
    lon: float
    extra_tags: Dict[str, str]
    address_tags: Dict[str, str]
    admin_level: int


def get_place_data(place: str) -> List[OsmResult]:
    """
    Extracts place search results from OpenStreetMap.
    :param place: place to search
    :return: list of parsed results as OsmResult structure
    """
    return []
