from typing import List

import pytest

from .osm import get_place_data, OsmResult

CASES = {
    'минск': [
        OsmResult(
            name='Минск, Беларусь',
            type_='City',
            lat=53.902334,
            lon=27.5618791,
            extra_tags={
                'capital': 'yes',
                'name:prefix': 'город',
                'place': 'city',
                'population': '1982444',
                'website': 'https://minsk.gov.by/',
                'wikidata': 'Q2280',
                'wikipedia': 'be:Мінск',
                'wikipedia:be': 'Мінск',
                'wikipedia:en': 'Minsk',
                'wikipedia:pl': 'Mińsk',
            },
            address_tags={'continent': 'Europe', 'country': 'BY'},
            admin_level=4,
        ),
        OsmResult(
            name='Minsk, Беларусь',
            type_='State',
            lat=53.8395964,
            lon=27.4711051089346,
            extra_tags={},
            address_tags={},
            admin_level=4,
        ),
        OsmResult(
            name='Минск, Беларусь',
            type_='State',
            lat=53.8791852,
            lon=27.8995566219764,
            extra_tags={},
            address_tags={},
            admin_level=4,
        ),
        OsmResult(
            name='Minsk, кв. Модерно предградие, Люлин, Sofia City, София, 1324, Болгария',
            type_='Residential',
            lat=42.7227445,
            lon=23.2693328,
            extra_tags={
                'lanes': '2',
                'maxspeed': '40',
                'surface': 'asphalt',
            },
            address_tags={},
            admin_level=15,
        ),
        OsmResult(
            name='Minsk, Nueva Granada, Loja, Лоха, 110107, Эквадор',
            type_='Residential',
            lat=-3.9808378,
            lon=-79.1968247,
            extra_tags={},
            address_tags={},
            admin_level=15,
        ),
    ],
    'добромысленский переулок, 4': [
        OsmResult(
            name='4, Добромысленский переулок, Грушевка, Московский район, Минск, 220007, Беларусь',
            type_='Building',
            lat=53.8917074,
            lon=27.5387669018881,
            extra_tags={},
            address_tags={
                'city': 'Минск',
                'housenumber': '4',
                'postcode': '220007',
                'street': 'Добромысленский переулок',
            },
            admin_level=15,
        ),
    ],
    'москва': [
        OsmResult(
            name="Москва, Центральный административный округ, Москва, Центральный федеральный округ, Россия",
            type_="City",
            extra_tags={
                "capital": "yes",
                "place": "city",
                "population": "12630289",
                "wikidata": "Q649",
                "wikipedia": "ru:Москва"
            },
            address_tags={
                "country": "RU",
                "region": "Москва"
            },
            admin_level=15,
            lat=55.7504461,
            lon=37.6174943,
        ),
        OsmResult(
            name="Москва, Центральный федеральный округ, Россия",
            type_="State",
            extra_tags={
                "place": "state",
                "wikidata": "Q649",
                "wikipedia": "ru:Москва"
            },
            address_tags={
                "country": "RU",
                "region": "Москва"
            },
            admin_level=4,
            lat=55.4792046,
            lon=37.3273304,
        ),
        OsmResult(
            name="Москва, Западный административный округ, Москва, Центральный федеральный округ, 143059, Россия",
            type_="River",
            extra_tags={
                "wikidata": "Q175117",
                "wikipedia": "ru:Москва (река)"
            },
            address_tags={},
            admin_level=15,
            lat=55.7098009,
            lon=37.0536908,
        ),
        OsmResult(
            name="Москва, Ленинский район, Московская область, Центральный федеральный округ, 140091, Россия",
            type_="River",
            extra_tags={
                "wikidata": "Q175117",
                "wikipedia": "ru:Москва (река)"
            },
            address_tags={},
            admin_level=15,
            lat=55.6095218,
            lon=37.8089581,
        ),
        OsmResult(
            name="Москва, городской округ Воскресенск, Московская область, Центральный федеральный округ,"
                 " 140230, Россия",
            type_="River",
            extra_tags={
                "wikidata": "Q175117",
                "wikipedia": "ru:Москва (река)"
            },
            address_tags={},
            admin_level=15,
            lat=55.4033937,
            lon=38.5070172,
        ),
    ],
}


@pytest.mark.parametrize('place,expected', CASES.items())
def test_places_search(place: str, expected: List[OsmResult]) -> None:
    assert get_place_data(place) == expected
