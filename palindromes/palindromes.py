def complement_to_palindrome(line: str) -> int:
    """
    Determine a number of symbols to add to a line from left or right to complemnet it to palindrome.
    :param line: input string
    :return: a number of needed symbols
    """
