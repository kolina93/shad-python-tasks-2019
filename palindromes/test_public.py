import pytest

from .palindromes import complement_to_palindrome


@pytest.mark.parametrize('line,expected', [('AwI', 2), ('zCssCzYzCssC', 1)])
def test_palindromes(line: str, expected: int) -> None:
    assert expected == complement_to_palindrome(line)
