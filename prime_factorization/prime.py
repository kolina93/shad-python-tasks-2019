from typing import List


def factorize(number: int) -> List[int]:
    """
    Factorize an integer into a list of its prime dividers
    :param number: integer
    :return: list of its prime factors in non-descending order
    """
    return []
