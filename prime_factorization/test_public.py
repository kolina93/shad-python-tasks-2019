from typing import List

import pytest

from .prime import factorize


@pytest.mark.parametrize('n, factors', [(48, [2, 2, 2, 2, 3]), (19, [19]), (8, [2, 2, 2])])
def test_prime_factorization(n: int, factors: List[int]) -> None:
    assert factorize(n) == factors
