class PropertyMaker(type):
    """
    Metaclass which automatically creates property field if either set_field or get_field method exists.
    """
