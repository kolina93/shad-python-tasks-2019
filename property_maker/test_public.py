from typing import Any

from _pytest.capture import CaptureFixture

from .property_maker import PropertyMaker


def test_example(capsys: CaptureFixture) -> None:
    class C(metaclass=PropertyMaker):
        def get_x(self) -> Any:
            print("get x")
            return self._x

        def set_x(self, value: Any) -> None:
            print("set x")
            self._x = value

    c = C()
    c.x = 1  # type: ignore
    assert c.x == 1  # type: ignore
    stdout, _ = capsys.readouterr()
    assert stdout == "set x\nget x\n"
