from typing import Iterable, Sized, Iterator


class Range(Sized, Iterable[int]):
    """The range-like type, which represents an immutable sequence of numbers"""

    def __init__(self, *args: int) -> None:
        """
        :param args: either it's a single `stop` argument
            or sequence of `start, stop[, step]` arguments.
        If the `step` argument is omitted, it defaults to 1.
        If the `start` argument is omitted, it defaults to 0.
        If `step` is zero, ValueError is raised.
        """
        raise NotImplementedError

    def __iter__(self) -> Iterator[int]:
        raise NotImplementedError

    def __repr__(self) -> str:
        raise NotImplementedError

    def __str__(self) -> str:
        raise NotImplementedError

    def __contains__(self, item: int) -> bool:
        raise NotImplementedError

    def __getitem__(self, item: int) -> int:
        raise NotImplementedError

    def __len__(self) -> int:
        raise NotImplementedError
