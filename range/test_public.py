from .range import Range


def test_basic() -> None:
    n = 0
    for i in Range(10):
        n += i
    assert n == 45


def test_repr() -> None:
    assert 'range(10, 20, 2)' == str(Range(10, 20, 2))


def test_len() -> None:
    assert 100 == len(Range(100))


def test_contains() -> None:
    assert 10 in Range(11)
    assert 10 not in Range(10)


def test_access() -> None:
    assert 2 == Range(1, 3)[1]
