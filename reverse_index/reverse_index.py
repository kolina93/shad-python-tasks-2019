from typing import List, Tuple


def reverse_index(sentences: List[str], queries: List[str]) -> List[Tuple[int, ...]]:
    """
    For each query find all their occurrences in sentences.
    :param sentences: array of sentences (each sentence consists of latin letters, spaces and punctuation marks)
    :param queries: array of queries (latin words separated by spaces)
    :return: for each query return a tuple of indexes of sentences that contain all the words of the query
    """
    return []
