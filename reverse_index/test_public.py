from .reverse_index import reverse_index


def test_reverse_index() -> None:
    sentences = [
        ' It tells the day of the month, and doesn t tell what o clock it is!',
        ' It IS a long tail, certainly,  said Alice, looking down with wonder at the Mouse s '
        'tail   but why do you call it sad',
        'While she was trying to fix on one, the cook took the cauldron of soup off the fire, and at '
        'once set to work throwing everything within her reach at the Duchess and the baby  the fire '
        'irons came first  then followed a shower of saucepans, plates, and dishes.',
        'Alice folded her hands, and began:        You are old, Father William,  the young man '
        'said,      And your hair has become very white     And yet you incessantly stand on your '
        'head       Do you think, at your age, it is right',
        ' It s all about as curious as it can be,  said the Gryphon.',
        'He looked anxiously over his shoulder as he spoke, and then raised himself upon tiptoe, put '
        'his mouth close to her ear, and '
        'whispered  She s under sentence of execution.',
        ' Come, let s try the first figure!',
    ]
    requests = [
        'has old you and',
        'saucepans plates at fire within everything cauldron',
        'be',
        's let',
        'of upon anxiously whispered himself',
        'yet hair hair age age right your right',
        'o tells',
        'as',
        'he dishes at',
        'the the tells is are be william',
        's said gryphon it as it it',
        'fire the everything shower soup set',
        'a',
    ]
    expected = [(4,), (3,), (5,), (7,), (6,), (4,), (1,), (5, 6), (), (), (5,), (3,), (2, 3)]
    assert reverse_index(sentences, requests) == expected
