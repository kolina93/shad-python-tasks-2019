from typing import List, Optional


def parse_color(color: str) -> Optional[List[int]]:
    """
    Parses rgb components of color from different formats.
    :param color: representation of color in predefined format
    :return: list of 3 elements - intensity of red, green and blue component
    """
    pass
