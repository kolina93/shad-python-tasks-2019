import pytest

from .roman_to_decimal import roman_to_decimal


@pytest.mark.parametrize('roman_number, decimal_number', [('I', 1), ('V', 5), ('DCCCXLIX', 849)])
def test_decimal_to_roman(roman_number: str, decimal_number: int) -> None:
    assert roman_to_decimal(roman_number) == decimal_number
