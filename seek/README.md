## Seek and read

### Условие

Нужно написать функцию, которая будет смещаться в переданном файле на позицию `offset` и выводить в переданный выходной поток `lines_amount` строк в файле перед этой позицией. Поведение функции должно быть эквивалентно выполнению `head -c offset filename | tail -n lines_amount`.

Файл может быть очень большим, поэтому читать его полностью сначала может быть очень неэффективно. Поэтому нужно почитать про то, как файл можно читать
[непоследовательно (например, с конца)](https://docs.python.org/3/library/io.html?highlight=seek#io.IOBase.seek) и [определенными кусками (блоками)](https://docs.python.org/3/library/io.html?highlight=seek#io.BufferedIOBase.read).

Не забудьте, что в одном блоке может оказаться как больше строк, чем требуется, так и не оказаться вовсе (если строка очень длинная).

### Примеры
```python
>>> seek(filename=Path('access.log'), offset=30000, lines_amount=7)
35.195.248.120 - - [03/Oct/2019:15:24:42 +0000] "POST /api/report HTTP/1.1" 200 33 "-" "python-requests/2.22.0"
35.195.248.120 - - [03/Oct/2019:15:27:13 +0000] "POST /api/report HTTP/1.1" 200 33 "-" "python-requests/2.22.0"
217.21.43.95 - - [03/Oct/2019:15:27:16 +0000] "GET /results HTTP/1.1" 200 127339 "https://2019.shad-python-minsk.org/" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/77.0.3865.90 Chrome/77.0.3865.90 Safari/537.36"
217.21.43.95 - - [03/Oct/2019:15:27:36 +0000] "GET / HTTP/1.1" 200 6861 "https://2019.shad-python-minsk.org/results" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/77.0.3865.90 Chrome/77.0.3865.90 Safari/537.36"
217.21.43.95 - - [03/Oct/2019:15:27:36 +0000] "GET /static/favicon-py.png?q=1568990541 HTTP/1.1" 200 1150 "https://2019.shad-python-minsk.org/" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/77.0.3865.90 Chrome/77.0.3865.90 Safari/537.36"
35.195.248.120 - - [03/Oct/2019:15:29:19 +0000] "POST /api/report HTTP/1.1" 200 33 "-" "python-requests/2.22.0"
46.216.65.38
```
