from pathlib import Path
from typing import Optional, IO


def seek(filename: Path, offset: int, lines_amount: int = 1, output: Optional[IO[bytes]] = None) -> None:
    """
    :param filename: file to read lines from (can be very large)
    :param offset: position in file to seek to.
    :param lines_amount: number of lines to read before offset
    :param output: stream to write requested amount of lines (if nothing specified stdout will be used)
    """
    assert 0 <= offset <= filename.stat().st_size, "Invalid offset"
