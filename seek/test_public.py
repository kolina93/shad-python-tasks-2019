from collections import Counter
from contextlib import contextmanager
from dataclasses import dataclass
from io import BytesIO
from itertools import chain
from os import fdopen, dup, dup2
from pathlib import Path
from sys import stdout
from typing import List, Tuple, Iterable, Generator, TextIO

import pytest

from .seek import seek


@dataclass
class Case:
    content: List[str]
    offset: int
    lines_amount: int
    result: List[str]
    ends_with_empty_line: bool
    encoding: str = 'utf-8'


TEST_CASES = [
    Case(content=[], offset=0, lines_amount=2, result=[], ends_with_empty_line=True),

    Case(content=['one', 'two', 'three'], offset=14, lines_amount=1, result=['three'], ends_with_empty_line=True),
    Case(content=['one', 'two', 'three'], offset=13, lines_amount=1, result=['three'], ends_with_empty_line=False),
    Case(
        content=['one', 'two', 'three'],
        offset=12,
        lines_amount=2,
        result=['two', 'thre'],
        ends_with_empty_line=False,
    ),

    Case(
        content=['one', 'two', 'three'],
        offset=14,
        lines_amount=7,
        result=['one', 'two', 'three'],
        ends_with_empty_line=True,
    ),
    Case(content=['one', 'two', 'three'], offset=6, lines_amount=3, result=['one', 'tw'], ends_with_empty_line=False),
    Case(content=['one', 'two', 'three'], offset=0, lines_amount=3, result=[], ends_with_empty_line=True),

    Case(content=['one', 'two', 'three'], offset=14, lines_amount=0, result=[], ends_with_empty_line=True),
    Case(content=['one', 'two', 'three'], offset=2, lines_amount=0, result=[], ends_with_empty_line=True),
    Case(content=['one', 'two', 'three'], offset=0, lines_amount=0, result=[], ends_with_empty_line=True),

    Case(
        content=['один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять', 'десять'],
        offset=40,
        lines_amount=3,
        result=['три', 'четыре', 'пя'],
        ends_with_empty_line=False,
        encoding='utf-8',
    ),
    Case(
        content=['один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять', 'десять'],
        offset=22,
        lines_amount=3,
        result=['три', 'четыре', 'пя'],
        ends_with_empty_line=False,
        encoding='windows-1251',
    )
]


def generate_middle_content(size: int, string_size: int) -> Tuple[List[str], List[int]]:
    prefix_sums: List[int] = []
    parts: List[str] = []

    for i in range(size):
        parts.append(str(i) * string_size)
        prefix_sums.append(len(parts[-1]) + (0 if i == 0 else prefix_sums[i - 1]))

    return parts, prefix_sums


def generate_middle_cases() -> Iterable[Case]:
    for content, prefix_sums in map(generate_middle_content, [2000, 20000], [200, 2000]):
        yield Case(content, prefix_sums[-1] + len(content), len(content), content, True)
        yield Case(content, prefix_sums[-1] + len(content) - 1, len(content), content, False)

        yield Case(content, prefix_sums[-1] + len(content), 7, content[-7:], True)

        middle_index = len(content) // 2
        offset = prefix_sums[middle_index - 1] + middle_index
        yield Case(content, offset, middle_index, content[:middle_index], True)
        yield Case(content, offset, 10, content[middle_index - 10:middle_index], True)

        middle_string = str(middle_index)
        middle_half_length = len(middle_string) // 2
        cut_middle_string = middle_string[:middle_half_length]
        offset += middle_half_length
        yield Case(content, offset, 0, [], True)
        yield Case(content, offset, 1, [cut_middle_string], False)
        yield Case(content, offset, 501, content[middle_index - 500:middle_index] + [cut_middle_string], False)


@pytest.mark.parametrize('case', chain(TEST_CASES, generate_middle_cases()))
def test_seek(case: Case, tmp_path: Path) -> None:
    test_file = tmp_path / 'test.log'
    test_file.write_bytes('\n'.join(case.content).encode(case.encoding) + b'\n')
    output = BytesIO()

    seek(test_file, offset=case.offset, lines_amount=case.lines_amount, output=output)

    result = output.getvalue().decode(case.encoding).split('\n')

    if case.ends_with_empty_line:
        assert result[-1] == ''
        del result[-1]

    assert result == case.result


def test_real_log_file() -> None:
    log_file = Path(__file__).parent / 'access.log'
    output = BytesIO()

    seek(log_file, 30000, lines_amount=100, output=output)
    answer = output.getvalue().decode().split('\n')[:-1]

    ips = Counter(line.split()[0] for line in answer)
    handles = Counter(line.split()[6] for line in answer)

    assert dict(handles) == {
        '/': 36,
        '/api/report': 15,
        '/Pages/login.htm': 2,
        '/favicon.ico': 1,
        '/login': 3,
        '/results': 11,
        '/static/': 1,
        '/static/favicon-py.png?q=1568990541': 7,
        '/static/style.css?q=1568990541': 23,
    }
    assert dict(ips.most_common(5)) == {
        '178.127.235.19': 12,
        '217.21.43.95': 9,
        '35.195.248.120': 15,
        '79.170.111.48': 10,
        '86.57.155.118': 19,
    }


@contextmanager
def stdout_redirected(redirect_to: TextIO) -> Generator[None, None, None]:
    original_stdout_fd = stdout.fileno()
    with fdopen(dup(original_stdout_fd), 'w') as copied:
        stdout.flush()
        dup2(redirect_to.fileno(), original_stdout_fd)
        try:
            yield
        finally:
            stdout.flush()
            dup2(copied.fileno(), original_stdout_fd)


def test_stdout_by_default(tmp_path: Path) -> None:
    log_file = Path(__file__).parent / 'access.log'
    expected_line = ('46.216.65.38 - - [03/Oct/2019:15:29:44 +0000] "GET /results HTTP/1.1" 200 127338'
                     ' "https://2019.shad-python-minsk.org/" "Mozilla/5.0 (Linux; Android 7.1.2; Redmi 4A)'
                     ' AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Mobile Safari/537.36"')

    redirect_file = tmp_path / 'redirect.txt'
    with open(redirect_file, 'w') as redirect_to, stdout_redirected(redirect_to):
        seek(log_file, 30234, lines_amount=10)

    answer = redirect_file.read_text().split('\n')[:-1]

    assert len(answer) == 10
    assert answer[-1] == expected_line
