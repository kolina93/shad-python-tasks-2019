## Статистика покупок

### Условие
Есть датафрейм `df` с информацией о покупках товаров в некотором магазине. Пример:
```
#
#             customer       item  quantity  price
#     0       John Doe  chocolate         2   24
#     1       John Doe    cookies         1   50
#     2      Megan Foo  chocolate         3   15
#     3      Megan Foo    cookies         3   18
```

Один и тот же товар может продаваться по разным ценам.

Вам нужно написать функцию `check_table`, возвращающую тапл из двух словарей: в первом должно быть указано,
сколько денег оставил в магазине каждый покупатель, а во втором — сколько удалось выручить за продажу каждого товара.
Например, для приведенной выше таблицы должно быть возвращено:

```python
({'John Doe': 98, 'Megan Foo': 99}, {'chocolate': 93, 'cookies': 104})
```

### Пример
```python
In [1]: from shopping import check_table

In [2]: import pandas

In [3]: COLUMNS = ['customer', 'item', 'quantity', 'price']

In [4]: input_ = pandas.DataFrame(
   ...:         [
   ...:             ['John Doe', 'chocolate', 2, 24],
   ...:             ['John Doe', 'cookies', 1, 50],
   ...:             ['Megan Foo', 'chocolate', 3, 15],
   ...:             ['Megan Foo', 'cookies', 3, 18],
   ...:         ],
   ...:         columns=COLUMNS,
   ...:     )
   ...:

In [5]: check_table(input_)
Out[5]: ({'John Doe': 98, 'Megan Foo': 99}, {'chocolate': 93, 'cookies': 104})

In [6]:
```
