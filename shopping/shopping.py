from typing import Dict, Tuple

import pandas


def check_table(data: pandas.DataFrame) -> Tuple[Dict[str, int], Dict[str, int]]:
    """
    Calculates statistics on customer spendings and goods revenues.
    :param data: dataframe with information about shoppings with customer, item, quantity and price columns.
    :return: first dict - amount of money spent by each customer, second dict - amount of revenue by each item.
    """
    return {}, {}
