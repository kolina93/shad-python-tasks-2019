import pandas

from .shopping import check_table

COLUMNS = ['customer', 'item', 'quantity', 'price']


def test_shopping() -> None:
    input_ = pandas.DataFrame(
        [
            ['John Doe', 'chocolate', 2, 24],
            ['John Doe', 'cookies', 1, 50],
            ['Megan Foo', 'chocolate', 3, 15],
            ['Megan Foo', 'cookies', 3, 18],
        ],
        columns=COLUMNS,
    )

    assert check_table(input_) == ({'John Doe': 98, 'Megan Foo': 99}, {'chocolate': 93, 'cookies': 104})
