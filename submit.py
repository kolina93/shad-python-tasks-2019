#!/usr/bin/env python3

import os
import re
import shutil
import subprocess
import sys

from os import path
from pathlib import Path


SHADOW_REPO_DIR = '../.submit-repo'
IGNORE_FILE_PATTERNS = ['*.md', '*.pyd', '*.so', '.mypy_cache', '.pytest_cache', '__pycache__', 'build']
VERBOSE = False
ADD_HINT = False


def print_command(*args):
    print('>', *args, file=sys.stderr)
    sys.stderr.flush()


def git(*args):
    command_args = ['git'] + list(args)
    if args[0] == 'push':
        stdout = None
    else:
        stdout = subprocess.DEVNULL

    if VERBOSE:
        print_command(' '.join(command_args))
        if args[0] != 'ls-remote':
            stdout = None

    if args[0] == 'push' and not VERBOSE:
        command_args.append('-q')

    subprocess.run(command_args, cwd=SHADOW_REPO_DIR, stderr=subprocess.STDOUT, stdout=stdout, check=True)


def git_output(*args):
    command_args = ['git'] + list(args)

    if VERBOSE:
        print_command(' '.join(command_args))

    return subprocess.check_output(
        command_args
    ).strip().decode('utf-8')


def clean_up():
    try:
        shutil.rmtree(SHADOW_REPO_DIR)
    except PermissionError:
        if sys.platform.startswith('win'):
            # Windows related staff for correct deletion
            os.system('rmdir /s /q "{}"'.format(SHADOW_REPO_DIR))
        else:
            raise
    except FileNotFoundError:
        pass


def set_up_shadow_repo(task_name):
    print('Setting up repo...')
    clean_up()
    os.makedirs(SHADOW_REPO_DIR)

    git('init')
    git('remote', 'add', 'local', '..')
    try:
        git('fetch', 'local',
            '+refs/heads/submits/{0}:refs/heads/submits/{0}'.format(task_name))
    except subprocess.CalledProcessError:
        pass

    user_name = git_output('config', 'user.name')
    user_email = git_output('config', 'user.email')
    student_url = git_output('config', '--get', 'remote.student.url')
    git('remote', 'add', 'student', student_url)

    with open(path.join(SHADOW_REPO_DIR, '.git', 'config'),
              mode='a', encoding='utf-8') as config:
        config.write(
            '[user]\n\tname = {}\n\temail = {}\n'.format(user_name, user_email)
        )


def add_ci_utils():
    filename_mapping = {'.grader-ci.yml': '.gitlab-ci.yml'}
    for filename in ('.grader-ci.yml', 'test_utils.py', 'setup.cfg'):
        dst_filename = filename_mapping.get(filename, filename)
        shutil.copyfile(
            '../{}'.format(filename),
            path.join(SHADOW_REPO_DIR, dst_filename)
        )
        git('add', dst_filename)


def create_commits(task_name):
    git('checkout', '-b', 'initial')
    add_ci_utils()
    git('commit', '-m', 'initial')

    print('Creating submit commit...')
    try:
        git('checkout', 'submits/' + task_name)
    except subprocess.CalledProcessError:
        git('checkout', '-b', 'submits/' + task_name)
    git('rm', '-r', '.')
    add_ci_utils()

    os.makedirs(path.join(SHADOW_REPO_DIR, task_name), exist_ok=True)
    ignore_files = [
        file
        for ignore_pattern in IGNORE_FILE_PATTERNS
        for file in Path('.').glob(ignore_pattern)
    ]
    for file in Path('.').iterdir():
        if file in ignore_files:
            continue
        target_path = Path(SHADOW_REPO_DIR) / task_name / file
        target_path.parent.mkdir(parents=True, exist_ok=True)
        if not file.is_dir():
            shutil.copyfile(str(file), str(target_path))
        else:
            shutil.copytree(str(file), str(target_path))

        with open(path.join(SHADOW_REPO_DIR, task_name, '.hint'), 'w') as output:
            output.write('1' if ADD_HINT else '0')

    git('add', '--all')
    git('commit', '-m', task_name, '--allow-empty')


def push_branches(task_name):
    print('Pushing changes to remote repo...')
    git('push', 'local', 'submits/' + task_name)

    if not git_output('ls-remote', '--heads', 'student', 'initial'):
        git('push', '-f', 'student', 'initial')

    git('push', '-f', 'student', 'submits/' + task_name)


def main():
    if '-v' in sys.argv:
        global VERBOSE
        VERBOSE = True

    if '--hint' in sys.argv:
        global ADD_HINT
        ADD_HINT = True

    task_name = path.basename(path.realpath('.'))

    set_up_shadow_repo(task_name)
    create_commits(task_name)
    push_branches(task_name)
    print('Done.')

    student_url = git_output('config', '--get', 'remote.student.url')
    student_url = re.sub(r'^(git@gitlab.com)([^:])*(:.*)$', lambda m: m.group(1) + m.group(3), student_url)
    student_url = student_url.replace(':', '/') \
        .replace('.git', '/pipelines').replace('git@', 'https://')
    print('\nYou can track your submission at:\n{}'.format(
        student_url
    ))


if __name__ == '__main__':
    main()
