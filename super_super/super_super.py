from typing import Any


def super_super() -> Any:
    """
    Analogue of builtin super function that works in a context of class method definition
    and provides an access to inherited methods that have been overridden in the class.
    :return: some object that will delegate method calls to a parent or sibling class depending on current context.
    """
