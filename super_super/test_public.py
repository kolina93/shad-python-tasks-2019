from typing import Any


from .super_super import super_super as super


class A:
    def f(self) -> Any:
        return 'A'

    @classmethod
    def cm(cls) -> Any:
        return cls, 'A'

    def a(self) -> str:
        return 'A'


class D(A):
    def f(self) -> str:
        return super().f() + 'D'

    def cm(self) -> Any:  # type: ignore
        return self, super().cm(), 'D'

    @classmethod
    def cm_2(cls) -> Any:
        return super().cm() + ('D',)


class B(A):
    def f(self) -> Any:
        return super().f() + 'B'


class C(B, D):
    def f(self) -> Any:
        return super().f() + 'C'

    def a(self) -> Any:
        return super().a() + 'C'


def test_super() -> None:
    assert D().f() == 'AD'
    assert D.f(D()) == 'AD'

    d = D()
    assert d.cm() == (d, (D, 'A'), 'D')

    assert d.cm_2() == (D, 'A', 'D')
    assert D.cm_2() == (D, 'A', 'D')

    assert C().f() == 'ADBC'
    assert C().a() == 'AC'
