## Sweeper

### Условие
В машинном обучении часто бывают ситуации, когда нужно перебрать значения некоторой функции по многомерной сетке
параметров. В этой задаче вам нужно будет написать декоратор `sweep` со следующими свойствами: если функция `func`
принимает `n` аргументов, то навесив на нее декоратор `sweep`, можно будет передавать в функцию `n` итерируемых
объектов и получить результаты выполнения функции для всевозможных вариантов аргументов из декартова произведения этих
итераторов в виде структуры с полями `arguments` и `result`. Для большего понимания посмотрите пример ниже.

### Пример
```python
In [1]: from sweeper import sweep

In [2]: @sweep
   ...: def simple_func(a, b, c):
   ...:     return a + b + c
   ...:
   ...:

In [3]: for item in simple_func(range(3), range(4), range(3)):
   ...:     print(f"arguments: {item.arguments}, result: {item.result}")
   ...:
arguments: (0, 0, 0), result: 0
arguments: (0, 0, 1), result: 1
arguments: (0, 0, 2), result: 2
arguments: (0, 1, 0), result: 1
arguments: (0, 1, 1), result: 2
arguments: (0, 1, 2), result: 3
arguments: (0, 2, 0), result: 2
arguments: (0, 2, 1), result: 3
arguments: (0, 2, 2), result: 4
arguments: (0, 3, 0), result: 3
arguments: (0, 3, 1), result: 4
arguments: (0, 3, 2), result: 5
arguments: (1, 0, 0), result: 1
arguments: (1, 0, 1), result: 2
arguments: (1, 0, 2), result: 3
arguments: (1, 1, 0), result: 2
arguments: (1, 1, 1), result: 3
arguments: (1, 1, 2), result: 4
arguments: (1, 2, 0), result: 3
arguments: (1, 2, 1), result: 4
arguments: (1, 2, 2), result: 5
arguments: (1, 3, 0), result: 4
arguments: (1, 3, 1), result: 5
arguments: (1, 3, 2), result: 6
arguments: (2, 0, 0), result: 2
arguments: (2, 0, 1), result: 3
arguments: (2, 0, 2), result: 4
arguments: (2, 1, 0), result: 3
arguments: (2, 1, 1), result: 4
arguments: (2, 1, 2), result: 5
arguments: (2, 2, 0), result: 4
arguments: (2, 2, 1), result: 5
arguments: (2, 2, 2), result: 6
arguments: (2, 3, 0), result: 5
arguments: (2, 3, 1), result: 6
arguments: (2, 3, 2), result: 7

In [4]: @sweep
   ...: def simple_func_2(a, b, c):
   ...:     return a * b + c
   ...:
   ...:

In [5]: for item in simple_func_2(range(3), range(4), range(3)):
   ...:     print(f"arguments: {item.arguments}, result: {item.result}")
   ...:
arguments: (0, 0, 0), result: 0
arguments: (0, 0, 1), result: 1
arguments: (0, 0, 2), result: 2
arguments: (0, 1, 0), result: 0
arguments: (0, 1, 1), result: 1
arguments: (0, 1, 2), result: 2
arguments: (0, 2, 0), result: 0
arguments: (0, 2, 1), result: 1
arguments: (0, 2, 2), result: 2
arguments: (0, 3, 0), result: 0
arguments: (0, 3, 1), result: 1
arguments: (0, 3, 2), result: 2
arguments: (1, 0, 0), result: 0
arguments: (1, 0, 1), result: 1
arguments: (1, 0, 2), result: 2
arguments: (1, 1, 0), result: 1
arguments: (1, 1, 1), result: 2
arguments: (1, 1, 2), result: 3
arguments: (1, 2, 0), result: 2
arguments: (1, 2, 1), result: 3
arguments: (1, 2, 2), result: 4
arguments: (1, 3, 0), result: 3
arguments: (1, 3, 1), result: 4
arguments: (1, 3, 2), result: 5
arguments: (2, 0, 0), result: 0
arguments: (2, 0, 1), result: 1
arguments: (2, 0, 2), result: 2
arguments: (2, 1, 0), result: 2
arguments: (2, 1, 1), result: 3
arguments: (2, 1, 2), result: 4
arguments: (2, 2, 0), result: 4
arguments: (2, 2, 1), result: 5
arguments: (2, 2, 2), result: 6
arguments: (2, 3, 0), result: 6
arguments: (2, 3, 1), result: 7
arguments: (2, 3, 2), result: 8

In [6]:
```
