from typing import Callable, Any, List, Tuple, TypeVar
from typing_extensions import Protocol


T = TypeVar('T')


class SweepResult(Protocol[T]):
    arguments: Tuple[Any, ...]
    result: T


def sweep(func: Callable[..., T]) -> Callable[..., List[SweepResult[T]]]:
    """
    Tranform passed callable func with n arguments into a function which accepts n iterable arguments
    and call initial func for every possible combination of elements from cartesian product of iterables.
    :param func: some callable
    :return: new func with prescribed behaviour
    """
