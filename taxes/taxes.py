import numpy


def compute_progressive_taxes(monthly_income: numpy.ndarray) -> float:
    """
    Implements a logic of tax calculation by the rules of progressive scale.
    :param monthly_income: monthly income distribution (may not consist of 12 elements)
    :return: an amount of taxes to pay
    """
    return 0.0
