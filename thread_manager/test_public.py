import time

from threading import current_thread

from .thread_manager import ThreadManager


def test_threads_management() -> None:
    with ThreadManager() as manager:
        first_thread = manager.start_thread()
        assert first_thread.is_alive()

        second_thread = manager.start_thread()
        assert second_thread.is_alive()

        assert first_thread.ident is not None
        manager.stop_thread(first_thread.ident)
        assert not first_thread.is_alive()

    assert not second_thread.is_alive()


def test_task_execution() -> None:
    def _test_task(thread_id: int, throw: bool = False) -> int:
        assert current_thread().ident == thread_id

        if throw:
            raise ValueError

        time.sleep(2)

        return 42

    with ThreadManager() as manager:
        thread = manager.start_thread()

        assert thread.ident is not None
        manager.call_in_thread(thread.ident, _test_task, thread.ident)
        manager.call_in_thread(thread.ident, _test_task, thread.ident, throw=True)

        results = manager.stop_thread(thread.ident)
        assert results[0] == 42
        assert isinstance(results[1], ValueError)
