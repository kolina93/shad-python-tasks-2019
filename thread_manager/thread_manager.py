from threading import Thread
from types import TracebackType
from typing import Any, List, Callable, Optional, Type


class ThreadManager:
    """
    Class that manages threads and calls functions in specific threads.
    """
    def start_thread(self) -> Thread:
        """
        Starts a new thread.
        :return: instance of newly created thread
        """
        raise NotImplementedError

    def stop_thread(self, thread_id: int) -> List[Any]:
        """
        Stops a specific thread.
        :param thread_id: identificator of the thread
        :return: a list of function call results that were executed in the stopped thread.
        """
        raise NotImplementedError

    def call_in_thread(self, thread_id: int, func: Callable[..., Any], *args: Any, **kwargs: Any) -> None:
        """
        Call a function in a specific thread.
        :param thread_id: identificator of the thread
        :param func: callable
        :param *args, **kwargs: function arguments
        """
        raise NotImplementedError

    def __enter__(self) -> 'ThreadManager':
        return self

    def __exit__(self, exc_type: Optional[Type[BaseException]], exc_val: Optional[BaseException],
                 exc_tb: Optional[TracebackType]) -> None:
        """
        Must stop all currently active threads.
        """
