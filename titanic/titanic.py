from typing import Set, List, Tuple, Any, Union

from pandas import DataFrame, Series


def male_age(df: DataFrame) -> float:
    """
    Return mean age of survived men, embarked in Southampton with fare > 30
    :param df: dataframe
    :return: mean age
    """


def nan_columns(df: DataFrame) -> Set[str]:
    """
    Return list of columns containing nans
    :param df: dataframe
    :return: required columns
    """


def class_distribution(df: DataFrame) -> Series:
    """
    Return Pclass distrubution
    :param df: dataframe
    :return: series with ratios
    """


def families_count(df: DataFrame, k: int) -> float:
    """
    Compute number of families with more than k members
    :param df: dataframe,
    :param k: number of members,
    :return: number of families
    """


def mean_price(df: DataFrame, tickets: List[str]) -> float:
    """
    Return mean price for specific tickets
    :param df: dataframe,
    :param tickets: list of tickets,
    :return: mean fare for this tickets
    """


def max_size_group(df: DataFrame, columns: List[str]) -> Union[Tuple[Any, ...], Any]:
    """
    For given set of columns calculate most common combination of values of these columns
    :param df: dataframe,
    :param columns: columns for grouping,
    :return: list of most common combination
    """


def dead_lucky(df: DataFrame) -> float:
    """
    Calculate dead ratio of passengers with lucky tickets
    :param df: dataframe,
    :return: ratio of dead lucky passengers
    """
