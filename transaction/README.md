## Transaction state

### Условие
Вам необходимо написать контекстный менеджер, который будет эмулировать поведение транзакции для некоторого состояния.
Состоянием будем считать `dict`, для простоты ключи и значения будут только строками. Нужно написать класс
`TransactionalStorage`, который будет принимать в конструкторе стейт в виде переменной `data`. Далее внутри контекстного
менеджера нужно поддержать редактирование состояния через `[]` (удаление, обновление и вставку по ключу). При этом
результаты обновления состояния должны записываться в `data` только в том случае, если весь блок внутри контекстного
менеджера выполнился без исключений, иначе после выполнения блока его значение не должно меняться.

### Пример
```python
In [1]: from transaction import TransactionalStorage

In [2]: data = {'a': 'a', 'b': 'c'}

In [3]: with TransactionalStorage(data) as state:
   ...:     state['a'] = 'c'
   ...:     state['c'] = 'd'
   ...:     del state['b']
   ...:

In [4]: data
Out[4]: {'a': 'c', 'c': 'd'}

In [5]: data = {'a': 'a', 'b': 'c'}

In [6]: with TransactionalStorage(data) as state:
   ...:     state['a'] = 'c'
   ...:     state['c'] = 'd'
   ...:     del state['b']
   ...:     raise RuntimeError
   ...:
   ...:
---------------------------------------------------------------------------
RuntimeError                              Traceback (most recent call last)
<ipython-input-6-6e6b57a34d4b> in <module>()
      3     state['c'] = 'd'
      4     del state['b']
----> 5     raise RuntimeError

RuntimeError:

In [7]: data
Out[7]: {'a': 'a', 'b': 'c'}

In [8]:
```
