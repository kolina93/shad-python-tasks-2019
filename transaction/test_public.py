import pytest

from .transaction import TransactionalStorage


@pytest.mark.parametrize('raise_exception', [True, False])
def test_transaction(raise_exception: bool) -> None:
    start = {'a': 'a', 'b': 'c'}

    try:
        with TransactionalStorage(start) as state:
            state['a'] = 'c'
            state['c'] = 'e'
            del state['b']

            if raise_exception:
                raise RuntimeError
    except RuntimeError:
        assert start == {'a': 'a', 'b': 'c'}
    except Exception:
        assert False, 'Wrong exception was raised'
    else:
        assert start == {'a': 'c', 'c': 'e'}
