from types import TracebackType
from typing import Dict, Optional, Type


class TransactionalStorage:
    """
    Class emulating transaction-like semantics for modifying underlying data.
    """
    def __init__(self, data: Dict[str, str]) -> None:
        """
        :param data: underlying to data to modify.
        """

    def __enter__(self) -> 'TransactionalStorage':
        """
        Starts transaction for modifying data.
        """

    def __exit__(self, exc_type: Optional[Type[BaseException]], exc_val: Optional[BaseException],
                 exc_tb: Optional[TracebackType]) -> None:
        """
        Must commit all modifications inside context manager's block if no exception occurs
        or rollback otherwize.
        """

    def __delitem__(self, key: str) -> None:
        """
        Delete key from underlying data (rollback could occur inside a transaction).
        """

    def __setitem__(self, key: str, value: str) -> None:
        """
        Set key in underlying data (rollback could occur inside a transaction).
        """
