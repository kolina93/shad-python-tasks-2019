## Welcome to the static world!

### Условие

Python, как известно, является динамически типизируемым языком. Поэтому, если хочется иметь некоторые инварианты о типах
аргументов функции, их нужно реализовывать самостоятельно. Вам будет нужно:

* написать декоратор `annotated_func`, который аннотирует переданную ему функцию `func`, т.е. он принимает
в конструкторе список типов аргументов функции `func`, и записывает эту информацию в атрибуте `__types__` объекта
`func`. Можно считать, что `func` не содержит `*args` и `*kwargs`.
Нужно проверять, что аргументы, переданные в `annotated_func`, являются валидными типами и, что количество
аргументов совпадает с количеством аргументов функции (нужно бросать `TypeError`, если это не так). Валидными типами
являются объекты класса `type` и объекты, объявленные ниже;

* реализовать класс `TypedAny`, такой, что вызов `isinstance(obj, TypedAny())` всегда возвращает `True`;

* реализовать класс `TypedUnion`, в конструктор которого будет передаваться список типов `types`, такой, что вызов
`isinstance(obj, TypedUnion(types))` должен проверять, что `obj` является является типом или наследником какого-либо
элемента из `types`;

* реализовать класс `TypedList`, в конструктор которого будет передаваться тип `T`, такой, что вызов
`isinstance(obj, TypedList(T))` должен проверять, что `obj` является `list` или его наследником, а все элементы в `obj`
являются `T` или его наследником;

* реализовать класс `TypedTuple`, в конструктор которого будет передаваться тип `T`, такой, что вызов
`isinstance(obj, TypedTuple(T))` должен проверять, что `obj` является `tuple` или его наследником, а все элементы
в `obj` являются `T` или его наследником;

* реализовать класс `TypedDict`, в конструктор которого будут передаваться типы `K` и `V`, такой, что вызов
`isinstance(obj, TypedDict(K, V))` должен проверять, что `obj` является `dict` или его наследником, все ключи в `obj`
являются `K` или его наследником, все значения в `obj` являются `V` или его наследником;

* реализовать класс `TypedSet`, в конструктор которого будет передаваться тип `T`, такой, что вызов
`isinstance(obj, TypedSet(T))` должен проверять, что `obj` является `set` или его наследником, а все элементы в `obj`
являются `T` или его наследником;

* реализовать класс `TypedCallable`, такой, что вызов `isinstance(obj, TypedCallable())` должен проверять, что `obj`
является `callable`;

* реализовать класс `TypedAnnotatedCallable`, в конструктор которого будет передаваться список типов `types`, такой, что
вызов `isinstance(obj, TypedAnnotatedCallable(types))` должен проверять, что `obj` является `callable` и типы
в `obj.__types__` соответствуют `types`, т.е. списки в `obj.types` и `types` должны быть одинаковыми.
Также можно считать, что `func` не содержит `*args` и `*kwargs`;

* написать декоратор `type_check`, который осуществляет проверку аннотированной функции `func` и проверяет при ее
вызове, что переданные аргументы соответствуют аннотированным, бросая ошибку `TypeError`, если это не так.

### Примечание

Mypy может ругаться, если вторым аргументом `isinstance` передать объект какого-нибудь из классов `Typed...` ошибкой вида
```bash
Argument 2 to "isinstance" has incompatible type "Tuple[Union[type, TypedAny], ...]"; expected "Union[type, Tuple[Union[type, Tuple[Any, ...]], ...]]"
```

Чтобы обойти это ограничение, можно cделать следующее заклинание:
```python
from typing import cast

assert isinstance(1, cast(type, TypedAny()))
```

Тот же `cast` можно использовать внутри декораторов, если он будет ругаться на то, что тип возвращаемой функции не соответствует нужному
```python
def decorator(func: Function) -> Function:
    def wrapper(*args: Any) -> Any:
        print("Hello!")
        return func(*args)

    return cast(Function, wrapper)
```

Чтобы обойти ошибки `"Function" has no attribute "__types__"` при работе с `__types__` можно использовать `getattr` и `setattr`.

### Пример
```python
In [1]: from type_check import annotated_func

In [2]: @annotated_func([int, float, list])
   ...: def hello_func(x, y, z):
   ...:     print("Hello! Arguments are {0}, {1}, {2}".format(x, y, z))
   ...:

In [3]: hello_func(1, 1.5, ['a', 'b'])
Hello! Arguments are 1, 1.5, ['a', 'b']

In [4]: from type_check import TypedList

In [5]: isinstance([1, 2, 3], TypedList(int))
Out[5]: True

In [6]: isinstance([1, 2, 3, 'a', None], TypedList(int))
Out[6]: False

In [7]: from type_check import TypedCallable

In [8]: isinstance(print, TypedCallable())
Out[8]: True

In [9]: isinstance(1, TypedCallable())
Out[9]: False

In [10]: from type_check import TypedAnnotatedCallable

In [11]: isinstance(hello_func, TypedAnnotatedCallable([int, float, list]))
Out[11]: True

In [12]: isinstance(hello_func, TypedAnnotatedCallable([int, float, str]))
Out[12]: False

In [13]: @annotated_func([int, float, TypedList(str)])
    ...: def hello_func(x, y, z):
    ...:     print("Hello! Arguments are {0}, {1}, {2}".format(x, y, z))
    ...:

In [14]: isinstance(hello_func, TypedAnnotatedCallable([int, float, list]))
    ...:
Out[14]: False

In [15]: isinstance(hello_func, TypedAnnotatedCallable([int, float, TypedList(str)]))
Out[15]: True

In [16]: from type_check import type_check

In [17]: @type_check
    ...: @annotated_func([int, float, TypedList(str)])
    ...: def hello_func(x, y, z):
    ...:     print("Hello! Arguments are {0}, {1}, {2}".format(x, y, z))
    ...:

In [18]: hello_func(1, 1.5, ['a', 'b'])
Hello! Arguments are 1, 1.5, ['a', 'b']

In [19]: try:
    ...:     hello_func(1, 1.5, ['a', 'b', 1])
    ...: except TypeError:
    ...:     print("Raised TypeError!")
    ...:
Raised TypeError!
```
