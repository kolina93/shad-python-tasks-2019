from typing import Any, List, cast
from typing_extensions import Protocol

import pytest

from .type_check import annotated_func, TypedList, TypedCallable, TypedAnnotatedCallable, type_check, AcceptedTypes


class TypesAttributeVar(Protocol):
    __types__: List[AcceptedTypes]


def instance_check(obj: Any, type_: AcceptedTypes) -> bool:
    # Case type_ to make mypy not complain about second argument of instance_check
    return isinstance(obj, cast(type, type_))


def test_type_check() -> None:
    @annotated_func([int, float, list])
    def hello_func(x: Any, y: Any, z: Any) -> None:
        print(f"Hello! Arguments are {x}, {y}, {z}")

    assert cast(TypesAttributeVar, hello_func).__types__ == [int, float, list]

    assert instance_check([1, 2, 3], TypedList(int))
    assert not instance_check([1, 2, 3, 'a', None], TypedList(int))

    assert instance_check(print, TypedCallable())
    assert not instance_check(1, TypedCallable())

    assert instance_check(hello_func, TypedAnnotatedCallable([int, float, list]))
    assert not instance_check(hello_func, TypedAnnotatedCallable([int, float, str]))

    @annotated_func([int, float, TypedList(str)])
    def hello_func_2(x: Any, y: Any, z: Any) -> None:
        print(f"Hello! Arguments are {x}, {y}, {z}")

    assert not instance_check(hello_func_2, TypedAnnotatedCallable([int, float, list]))
    assert instance_check(hello_func_2, TypedAnnotatedCallable([int, float, TypedList(str)]))

    @type_check
    @annotated_func([int, float, TypedList(str)])
    def hello_func_3(x: Any, y: Any, z: Any) -> None:
        print(f"Hello! Arguments are {x}, {y}, {z}")

    with pytest.raises(TypeError):
        hello_func_3(1, 1.5, ['a', 'b', 1])
