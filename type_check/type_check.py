from typing import List, Callable, Any, TypeVar, Union


class TypedAny:
    """
    Basic class for checking type of objects. Accept any given object.
    """


AcceptedTypes = Union[type, TypedAny]


class TypedUnion(TypedAny):
    """
    Accepts types from fixed list.
    """


class TypedContainer(TypedAny):
    """
    Accepts containers with elements of given type.
    """


class TypedList(TypedContainer):
    """
    Accepts lists with elements of given type.
    """


class TypedTuple(TypedContainer):
    """
    Accepts tuples with elements of given type.
    """


class TypedDict(TypedContainer):
    """
    Accepts dicts with keys and values of given types.
    """


class TypedSet(TypedContainer):
    """
    Accepts sets with elements of given type.
    """


class TypedCallable(TypedAny):
    """
    Accepts callable arguments.
    """


class TypedAnnotatedCallable(TypedCallable):
    """
    Accepts callable arguments with annotated arguments.
    """


Function = TypeVar('Function', bound=Callable[..., Any])



def annotated_func(args: List[AcceptedTypes]) -> Callable[[Function], Function]:
    """
    Decorator for annotating function arguments
    :param args: list of types to annotate functions.
                 If any element of args is not subclass of type or TypedAny TypeError must be thrown.
    :return: decorator that will set __types__ attribute of passed function to args.
             Returned decorator must throw TypeError for functions with quantity of arguments not equal to len(args).
    """


def type_check(func: Function) -> Function:
    """
    Check types of arguments for annotated func in runtime.
    :param func: callable which must be annotated by annotated_func decorator
    :return: wrapped function which will check types of arguments passed to func.
            Must throw TypeError for any inconcistency.
    """
