## Утилита uniq

### Условие
Нужно реализовать аналог консольной утилиты uniq. Эта утилита выводит только первую из идущих подряд повторяющихся строк. Программа должна уметь принимать ключи:
1. -c, –count, выводить число повторов в начале каждой строки;
2. -d, –repeat, выводить только повторяющиеся строки;
3. -i, –ignore-case, игнорировать регистр при сравнении;
4. -u, –unique, выводить только неповторяющиеся строки.

Обратите внимание, что ключей может быть несколько и всегда указан хотя бы один ключ. Гарантируется, что оба ключа -d, –repeat и -u, –unique не могут быть указаны одновременно.

Количество строк `n` для обработки будет подчиняться условию `1 <=n <= 10000`. Все строки будут состоять из строчных и прописных букв латинского алфавита. Длина каждой строки не превосходит `100` символов.

### Пример

```python
>>> list(uniq('-i --unique', ['hRcWbYrIDUzKoabTvfyNELjQOBbmQtpkELGuMwMuhSkhjHOUsZEAnANHzMxiKOXXsPSMPPv', 'HRcwBYRiduzkoaBTvFyNEljQoBbmQTPkeLGuMwMuhSKhJhoUSzeaNANHZmxIKOXXspsMppV', 'GRiVKe', 'jtMoGnmBauAtDGoIOPzGdnDwfsfLJslVHsxQMgKUEFouvModghDOmptPSJWCrbiaoXaFrAennsnJuKHeIQTXB', 'mkbDzjQfADEMnWKhEYPGNHSvcTiWjkmSScNuYiBBvdhxLrtbNIltTkhK', 'NMqq', 'nmQq', 'jTCbFCrckJXtDwWNQrrooJBI', 'kbKbkvuqxrrcQOJNyxWwmUzCkdVjGAuUgtrVvwrsJfhrHzLWuHidKmLnStqRhZgHdmtfgSdZkosXpxatuiHmNWVwtj', 'hSgojOuLuVZjjdPrSpXBvcndUCfRhmp', 'HsgOJouLuVzJjDPRSpXbVCnDUcFRHMP', 'umfWDFqXAiHwGT', 'BvEtICVHMUMWmzizuliJQBxbSujSRaDzEGGdoqwIsXpyLUrWSTb', 'oRRSKYjlbrqHjthd', 'QnrjrYPCEeSyiNPjWsoEoIDMlrpZSecGCYJcdRbRpHtRGhWNMkJXhcvAmCotbxeaQWksPsAPRaTCYXfOxDnHSJXFSKtT', 'sNczMaxQnbLqWgbZTkZcEOOtvtEfsMpXixAefrsEGSpsHXuzNtRXPtVFckTolRMLgBCwcusALkLZNerIQhb']))
['GRiVKe', 'jtMoGnmBauAtDGoIOPzGdnDwfsfLJslVHsxQMgKUEFouvModghDOmptPSJWCrbiaoXaFrAennsnJuKHeIQTXB', 'mkbDzjQfADEMnWKhEYPGNHSvcTiWjkmSScNuYiBBvdhxLrtbNIltTkhK', 'jTCbFCrckJXtDwWNQrrooJBI', 'kbKbkvuqxrrcQOJNyxWwmUzCkdVjGAuUgtrVvwrsJfhrHzLWuHidKmLnStqRhZgHdmtfgSdZkosXpxatuiHmNWVwtj', 'umfWDFqXAiHwGT', 'BvEtICVHMUMWmzizuliJQBxbSujSRaDzEGGdoqwIsXpyLUrWSTb', 'oRRSKYjlbrqHjthd', 'QnrjrYPCEeSyiNPjWsoEoIDMlrpZSecGCYJcdRbRpHtRGhWNMkJXhcvAmCotbxeaQWksPsAPRaTCYXfOxDnHSJXFSKtT', 'sNczMaxQnbLqWgbZTkZcEOOtvtEfsMpXixAefrsEGSpsHXuzNtRXPtVFckTolRMLgBCwcusALkLZNerIQhb']
```
