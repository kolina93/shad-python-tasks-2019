from dataclasses import dataclass
from inspect import isgeneratorfunction
from typing import List

import pytest

from .uniq import uniq


@dataclass
class Case:
    options: str
    lines: List[str]
    result_lines: List[str]


TEST_CASES = [
    Case(
        '-i --unique',
        [
            'hRcWbYrIDUzKoabTvfyNELjQOBbmQtpkELGuMwMuhSkhjHOUsZEAnANHzMxiKOXXsPSMPPv',
            'HRcwBYRiduzkoaBTvFyNEljQoBbmQTPkeLGuMwMuhSKhJhoUSzeaNANHZmxIKOXXspsMppV',
            'GRiVKe',
            'jtMoGnmBauAtDGoIOPzGdnDwfsfLJslVHsxQMgKUEFouvModghDOmptPSJWCrbiaoXaFrAennsnJuKHeIQTXB',
            'mkbDzjQfADEMnWKhEYPGNHSvcTiWjkmSScNuYiBBvdhxLrtbNIltTkhK',
            'NMqq',
            'nmQq',
            'jTCbFCrckJXtDwWNQrrooJBI',
            'kbKbkvuqxrrcQOJNyxWwmUzCkdVjGAuUgtrVvwrsJfhrHzLWuHidKmLnStqRhZgHdmtfgSdZkosXpxatuiHmNWVwtj',
            'hSgojOuLuVZjjdPrSpXBvcndUCfRhmp',
            'HsgOJouLuVzJjDPRSpXbVCnDUcFRHMP',
            'umfWDFqXAiHwGT',
            'BvEtICVHMUMWmzizuliJQBxbSujSRaDzEGGdoqwIsXpyLUrWSTb',
            'oRRSKYjlbrqHjthd',
            'QnrjrYPCEeSyiNPjWsoEoIDMlrpZSecGCYJcdRbRpHtRGhWNMkJXhcvAmCotbxeaQWksPsAPRaTCYXfOxDnHSJXFSKtT',
            'sNczMaxQnbLqWgbZTkZcEOOtvtEfsMpXixAefrsEGSpsHXuzNtRXPtVFckTolRMLgBCwcusALkLZNerIQhb',
        ],
        [
            'GRiVKe',
            'jtMoGnmBauAtDGoIOPzGdnDwfsfLJslVHsxQMgKUEFouvModghDOmptPSJWCrbiaoXaFrAennsnJuKHeIQTXB',
            'mkbDzjQfADEMnWKhEYPGNHSvcTiWjkmSScNuYiBBvdhxLrtbNIltTkhK',
            'jTCbFCrckJXtDwWNQrrooJBI',
            'kbKbkvuqxrrcQOJNyxWwmUzCkdVjGAuUgtrVvwrsJfhrHzLWuHidKmLnStqRhZgHdmtfgSdZkosXpxatuiHmNWVwtj',
            'umfWDFqXAiHwGT',
            'BvEtICVHMUMWmzizuliJQBxbSujSRaDzEGGdoqwIsXpyLUrWSTb',
            'oRRSKYjlbrqHjthd',
            'QnrjrYPCEeSyiNPjWsoEoIDMlrpZSecGCYJcdRbRpHtRGhWNMkJXhcvAmCotbxeaQWksPsAPRaTCYXfOxDnHSJXFSKtT',
            'sNczMaxQnbLqWgbZTkZcEOOtvtEfsMpXixAefrsEGSpsHXuzNtRXPtVFckTolRMLgBCwcusALkLZNerIQhb',
        ],
    ),
    Case(
        '--count',
        [
            'MtdiFyFQTOZOPpiSIcbFyxoPagWKzihmFRiVcVJLsXJZDJiPtZeOGlxlCy',
            'AjRMbziHSCJEwmVFONiVRiXqWjyCNZghQVvqkFTnoqyQOrhfzl',
            'rRNXvFYimSAuyksleBlesHXFVlJMTSIcEHzihqOplc',
            'gPddRtkPVYuSUWnZwMGqptRXOuBAzzlsKPsHjamRXdQZZNhRLqPUqABrlMccmvRyjgCCRFnJaMofKsPHLqIdKiXVn',
            'PGKGrvkIAcBANqQLYcYyxRMwzwfEweGmDPBJsjUreQtpdniCZmIAEArjGhwmjuReEwMswPoqpbFSRSLJEmXRnBv',
            'rAXGnfM',
            'rAXGnfM',
            'rAXGnfM',
            'ACGqrXDpruLGLkVwKSJVpvEJLXvhYldyZtWZUDPeCHgNzBNIoRWOKJBhcCdCtWroDWCMhMCJfapn',
            'trnCxxJDUcoqRBybwUqXJkKsNsAXqJ',
            'cRbzHxcqUxoOEVHbejDXZCKOIMbbriEAaYVHVNk',
        ],
        [
            '1 MtdiFyFQTOZOPpiSIcbFyxoPagWKzihmFRiVcVJLsXJZDJiPtZeOGlxlCy',
            '1 AjRMbziHSCJEwmVFONiVRiXqWjyCNZghQVvqkFTnoqyQOrhfzl',
            '1 rRNXvFYimSAuyksleBlesHXFVlJMTSIcEHzihqOplc',
            '1 gPddRtkPVYuSUWnZwMGqptRXOuBAzzlsKPsHjamRXdQZZNhRLqPUqABrlMccmvRyjgCCRFnJaMofKsPHLqIdKiXVn',
            '1 PGKGrvkIAcBANqQLYcYyxRMwzwfEweGmDPBJsjUreQtpdniCZmIAEArjGhwmjuReEwMswPoqpbFSRSLJEmXRnBv',
            '3 rAXGnfM',
            '1 ACGqrXDpruLGLkVwKSJVpvEJLXvhYldyZtWZUDPeCHgNzBNIoRWOKJBhcCdCtWroDWCMhMCJfapn',
            '1 trnCxxJDUcoqRBybwUqXJkKsNsAXqJ',
            '1 cRbzHxcqUxoOEVHbejDXZCKOIMbbriEAaYVHVNk',
        ],
    ),
]


def test_function_is_generator() -> None:
    assert isgeneratorfunction(uniq), "uniq must be a generator"


@pytest.mark.parametrize('case', TEST_CASES)
def test_uniq(case: Case) -> None:
    assert list(uniq(case.options, case.lines)) == case.result_lines
