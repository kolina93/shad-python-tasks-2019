from typing import Iterable


def uniq(options: str, lines: Iterable[str]) -> Iterable[str]:
    """
    Filter lines similar to unix uniq utility.
    :param options: options similar to command-line options of uniq
    :param lines: a stream of lines
    :return: generator over all selected lines from the passed stream
    """
