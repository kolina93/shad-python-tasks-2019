## Userdb

### Условие
В этой задаче вам предстоит написать сервер, который предоставляет API по управлению базой данных пользователей.
Сама реализация базы представлена в папке `mockdb`: она представляет из себя массив структур `User`, каждая структура имеет
следующие поля: `id` - уникальный числовой идентификатор, `name`, `team` - имя и команда пользователя, и `age` - возраст
пользователя. Интерфейс взаимодействия с базой предоставлен следующими функциями: `get_all`, `get_by_id`,
`get_by_team`, `create`, `update_by_id` и `delete_by_id`. Реализацию этих функций редактировать не нужно.

Вам нужно написать в файле `userdb_app.py` приложение, которое предоставляет следующее API:

* `GET /users` - получить текущий список всех пользователей из базы. Этот вызов должен возвращать 200 код ответа и
список всех пользователей.

* `GET /users/<id>` - возвращает пользователя с заданным `id` и код ответа 200, если он существует, иначе возвращает
404 код ответа.

* `GET /users?team={team}` - возвращает список пользователей с заданным `team` и 200 ответ.

* `POST /users` - создает нового пользователя. В теле запроса отправляются параметры `name`, `age` и `team`. Нужно
валидировать, что все параметры присутствуют и имеют непустое значение, а для `age` нужно дополнительно проверить, что
это положительное число. Пользователю нужно присвоить `id`, которого еще нет в базе. Если валидация успешна, то нужно
вернуть 201 код и представление нового пользователя, иначе нужно вернуть 422 код.

* `PUT /users/<id>` - изменить информацию о пользователе с данным `id`. Если такого пользователя не существует, нужно
вернуть 404. Параметры в теле запросы аналогичны `POST` с той лишь разницей, что могут присутствовать не все
параметры и для не переданных параметров нужно оставить старые значения. Если валидация переданных параметров успешна,
то нужно обновить пользователя в базе и вернуть 200 и новое представление пользователя, иначе вернуть 422.

* `DELETE /users/<id>` - удалить пользователя из базы с соответствующим `id` и вернуть 200 код, если он существует,
иначе вернуть 404 код.

Ожидаемый формат ответа можно посмотреть в примерах ниже и в тестах `test_public.py`.

### Пример

Запустить сервер можно вот такой командой

```bash
(venv) kolina93@kolina93-Latitude-E7440:~/shad/tasks/userdb$ python userdb_app.py
 * Serving Flask app "userdb_app" (lazy loading)
 * Environment: production
   WARNING: Do not use the development server in a production environment.
   Use a production WSGI server instead.
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 185-038-199
127.0.0.1 - - [16/Nov/2018 07:29:13] "GET /users HTTP/1.1" 200 -
127.0.0.1 - - [16/Nov/2018 07:30:37] "GET /users/2 HTTP/1.1" 200 -
127.0.0.1 - - [16/Nov/2018 07:31:55] "GET /users?team=LWB HTTP/1.1" 200 -
127.0.0.1 - - [16/Nov/2018 07:36:18] "POST /users HTTP/1.1" 201 -
127.0.0.1 - - [16/Nov/2018 07:40:09] "PUT /users/5 HTTP/1.1" 200 -
127.0.0.1 - - [16/Nov/2018 07:41:31] "DELETE /users/5 HTTP/1.1" 200 -
```

```bash
(venv) kolina93@kolina93-Latitude-E7440:~/shad/tasks/userdb$ curl 'http://localhost:5000/users'
[
  {
    "age": 19,
    "id": 1,
    "name": "Aria",
    "team": "LWB"
  },
  {
    "age": 20,
    "id": 2,
    "name": "Tim",
    "team": "LWB"
  },
  {
    "age": 23,
    "id": 3,
    "name": "Varun",
    "team": "NNB"
  },
  {
    "age": 24,
    "id": 4,
    "name": "Alex",
    "team": "C2TC"
  }
]
(venv) kolina93@kolina93-Latitude-E7440:~/shad/tasks/userdb$ curl 'http://localhost:5000/users/2'
{
  "age": 20,
  "id": 2,
  "name": "Tim",
  "team": "LWB"
}
(venv) kolina93@kolina93-Latitude-E7440:~/shad/tasks/userdb$ curl 'http://localhost:5000/users?team=LWB'
[
  {
    "age": 19,
    "id": 1,
    "name": "Aria",
    "team": "LWB"
  },
  {
    "age": 20,
    "id": 2,
    "name": "Tim",
    "team": "LWB"
  }
]
(venv) kolina93@kolina93-Latitude-E7440:~/shad/tasks/userdb$ curl -X POST 'http://localhost:5000/users' --data 'name=Neeraj&age=17&team=TBD'
{
  "age": 17,
  "id": 5,
  "name": "Neeraj",
  "team": "TBD"
}
(venv) kolina93@kolina93-Latitude-E7440:~/shad/tasks/userdb$ curl -X PUT 'http://localhost:5000/users/5' --data 'name=Shreyas&age=18'
{
  "age": 18,
  "id": 5,
  "name": "Shreyas",
  "team": "TBD"
}
(venv) kolina93@kolina93-Latitude-E7440:~/shad/tasks/userdb$ curl -X DELETE 'http://localhost:5000/users/5'
{
  "message": "User with id 5 was successfully deleted."
}
```

### Примечание
Можно пользоваться возможностями библиотек `Flask`, `Flask-WTF` и `WTForms`.
