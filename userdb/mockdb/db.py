from typing import List, Optional, cast

from .dummy_data import initial_db_state, PayloadType


db_state = initial_db_state


def get_all() -> List[PayloadType]:
    """
    :return: list of all current users in database
    """
    return db_state


def get_by_id(id_: int) -> Optional[PayloadType]:
    """
    :param id_: id of user
    :return: fetched user with id_ or None if no such user
    """
    return next((i for i in get_all() if i['id'] == id_), None)


def get_by_team(team: str) -> List[PayloadType]:
    """
    :param team: name of team
    :return: all users from team
    """
    return list(filter(lambda i: i['team'] == team, get_all()))


def create(payload: PayloadType) -> PayloadType:
    """
    Adds id_ field to payload and creates user in the database.
    :param payload: dict with age, team and name keys
    :return: dict with age, team, name and id_ keys
    """
    data = get_all()
    payload['id'] = max(cast(int, i['id']) for i in data) + 1 if data else 0
    db_state.append(payload)
    return payload


def update_by_id(id_: int, update_values: PayloadType) -> Optional[PayloadType]:
    """
    Updates user in the database by id_.
    :param id_: id of user
    :param update_values: payload with keys to update
    :return: payload with updated values
    """
    item = get_by_id(id_)
    if item is None:
        return None
    item.update({k: v for k, v in update_values.items() if k != 'id'})
    return item


def delete_by_id(id_: int) -> None:
    """
    Deletes user from the database by id_
    :param id_: id of user
    """
    global db_state
    db_state = [i for i in get_all() if i['id'] != id_]
