from typing import Generator

import pytest

from flask.testing import FlaskClient


@pytest.fixture('session')
def client() -> Generator[FlaskClient, None, None]:
    from .userdb_app import app

    yield app.test_client()


def test_index(client: FlaskClient) -> None:
    res = client.get('/')
    assert res.status_code == 200
    assert res.json['content'] == 'hello world!'


def test_mirror(client: FlaskClient) -> None:
    res = client.get('/mirror/Tim')
    assert res.status_code == 200
    assert res.json['name'] == 'Tim'


def test_get_users(client: FlaskClient) -> None:
    res = client.get('/users')
    assert res.status_code == 200

    users = res.json
    assert len(users) == 4
    assert users[0]['name'] == 'Aria'


def test_get_users_with_team(client: FlaskClient) -> None:
    res = client.get('/users?team=LWB')
    assert res.status_code == 200

    users = res.json
    assert len(users) == 2
    assert users[1]['name'] == 'Tim'


def test_get_user_id(client: FlaskClient) -> None:
    res = client.get('/users/1')
    assert res.status_code == 200

    user = res.json
    assert user['name'] == 'Aria'
    assert user['age'] == 19


def test_add_user(client: FlaskClient) -> None:
    res = client.post('/users', data={'name': 'Neeraj', 'age': 17, 'team': 'TBD'})
    assert res.status_code == 201

    user = client.get('/users/5').json
    assert user['name'] == 'Neeraj'
    assert user['age'] == 17
    assert user['team'] == 'TBD'
    assert user['id'] is not None

    res = client.post('/users', data={'name': 'Kevin', 'age': 178})
    assert res.status_code == 422


def test_update_user(client: FlaskClient) -> None:
    res = client.put('/users/5', data={'name': 'Shreyas', 'age': 18})
    assert res.status_code == 200

    user = client.get('/users/5').json
    assert user['name'] == 'Shreyas'
    assert user['age'] == 18
    assert user['team'] == 'TBD'
    assert user['id'] is not None


def test_delete_user(client: FlaskClient) -> None:
    res = client.delete('/users/5')
    assert res.status_code == 200

    res = client.get('/users/5')
    assert res.status_code == 404

    res = client.delete('/users/5')
    assert res.status_code == 404
