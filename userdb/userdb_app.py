from typing import Tuple, Optional, Any

from flask import Response, jsonify, Flask


ResponseType = Tuple[Response, int]


def create_response(data: Optional[Any] = None, status: int = 200) -> ResponseType:
    """
    Helper to create flask response
    :param data: response content
    :param status: response status code
    :return: tuple of flask response and status code
    """
    if data is not None and not isinstance(data, (dict, list, str)):
        raise TypeError('Invalid type of data')

    return jsonify(data), status


app = Flask(__name__)
app.config['WTF_CSRF_ENABLED'] = False


@app.route('/')
def hello_world() -> ResponseType:
    """
    Simple hello world route
    """
    return create_response({'content': 'hello world!'})


@app.route('/mirror/<name>')
def mirror(name: str) -> ResponseType:
    """
    Route to mirror passed param
    """
    return create_response({'name': name})


if __name__ == '__main__':
    app.run(debug=True)
