## Yet another парсер википедии

### Условие
В этой задаче вам нужно будет научиться парсить страницы
[Simple Wikipedia](https://simple.wikipedia.org/wiki/Main_Page). На вход вашей функции будет приходить контент html
страницы для какой-то из статей википедии. Надо научиться доставать следующую информацию и оформить ее в виде `dict`
(дальше рассматриваем на примере [страницы](https://simple.wikipedia.org/wiki/Encyclopædia_Britannica)):

* В поле `title` нужно положить заголовок страницы.
![Title](title.png)

* В поле `links` нужно положить отсортированный массив всех исходящиих ссылок на другие статьи Simple Wikipedia,
причем каждая ссылка должна быть указана столько раз, сколько она встречается на странице. Ссылкой на статью будем
считать ссылки, в которых путь подпадает под регулярное выражение `/wiki/.+`, но нужно будет отфильтровать служебные
страницы (картинки, файлы, комментарии к статье и т.д.), для фильтрации можно использовать следующий список регулярок:

    * `/wiki/User:.*`

    * `/wiki/File:.*`

    * `/wiki/Category:.*`

    * `/wiki/Special:.*`

    * `/wiki/Wikipedia:.*`

    * `/wiki/Talk:.*`

    * `/wiki/Template:.*`

    * `/wiki/Template_talk:.*`

    * `/wiki/Media:.*`

    * `/wiki/User_talk:.*`

    * `/wiki/Help:.*`

    * `/wiki/Module:.*`

    * `/wiki/Free_Content:.*`

    * `/wiki/Portal:.*`

* В поле `related_pages` нужно записать массив связанных страниц, если они есть, иначе пустой список.
![Related pages](related_pages.png)

* В поле `contents` нужно записать оглавление страницы в таком же иерархическом виде, как на самой странице, если его
нет, то нужно записать пустой список.
![Contents](contents.png)
Для страницы на скриншоте должно быть
```json
[
        {
            "name": "History",
            "children": [
                {
                    "name": "Editions",
                    "children": [
                        {
                            "name": "First era"
                        },
                        {
                            "name": "Second era"
                        },
                        {
                            "name": "Third era"
                        }
                    ]
                }
            ]
        },
        {
            "name": "The Encyclopedia Now",
            "children": [
                {
                    "name": "2007 print version"
                },
                {
                    "name": "Other Britannicas",
                    "children": [
                        {
                            "name": "Printed"
                        },
                        {
                            "name": "Electronic"
                        }
                    ]
                }
            ]
        },
        {
            "name": "Related pages"
        },
        {
            "name": "Further reading"
        },
        {
            "name": "References"
        },
        {
            "name": "Other websites"
        }
    ]
```

Полные примеры ответа можно посмотреть в тестовых файлах для публичных тестов.

### Пример
```python
In [1]: import requests

In [2]: r = requests.get('https://simple.wikipedia.org/wiki/Encyclopædia_Britannica')

In [3]: from wiki_parser import WikipediaParser

In [4]: parser = WikipediaParser()

In [5]: parser.parse(r.text)
Out[5]:
{'title': 'Encyclopædia Britannica',
 'links': ['https://simple.wikipedia.org/wiki/1911_Encyclop%C3%A6dia_Britannica',
  'https://simple.wikipedia.org/wiki/A_%26_C_Black',
  'https://simple.wikipedia.org/wiki/A_%26_C_Black',
  'https://simple.wikipedia.org/wiki/A_%26_C_Black',
  'https://simple.wikipedia.org/wiki/Adult',
  'https://simple.wikipedia.org/wiki/Age_of_Enlightenment',
  'https://simple.wikipedia.org/wiki/Alphabet',
  'https://simple.wikipedia.org/wiki/American_English',
  'https://simple.wikipedia.org/wiki/American_Library_Association',
  'https://simple.wikipedia.org/wiki/Anniversary',
  'https://simple.wikipedia.org/wiki/Article',
  'https://simple.wikipedia.org/wiki/BBC_News',
  'https://simple.wikipedia.org/wiki/BBC_News',
  'https://simple.wikipedia.org/wiki/Book',
  'https://simple.wikipedia.org/wiki/British_English',
  'https://simple.wikipedia.org/wiki/British_English',
  'https://simple.wikipedia.org/wiki/British_people',
  'https://simple.wikipedia.org/wiki/CD-ROM',
  'https://simple.wikipedia.org/wiki/Children',
  'https://simple.wikipedia.org/wiki/DVD',
  'https://simple.wikipedia.org/wiki/Digital',
  'https://simple.wikipedia.org/wiki/Edinburgh',
  'https://simple.wikipedia.org/wiki/Edinburgh',
  'https://simple.wikipedia.org/wiki/Edinburgh',
  'https://simple.wikipedia.org/wiki/Egyptian_hieroglyphs',
  'https://simple.wikipedia.org/wiki/Encarta',
  'https://simple.wikipedia.org/wiki/Encyclop%C3%A6dia_Britannica',
  'https://simple.wikipedia.org/wiki/Encyclop%C3%A6dia_Britannica',
  'https://simple.wikipedia.org/wiki/Encyclop%C3%A6dia_Britannica,_Inc.',
  'https://simple.wikipedia.org/wiki/Encyclop%C3%A6dia_Britannica,_Inc.',
  'https://simple.wikipedia.org/wiki/Encyclop%C3%A6dia_Britannica,_Inc.',
  'https://simple.wikipedia.org/wiki/Encyclop%C3%A6dia_Britannica,_Inc.',
  'https://simple.wikipedia.org/wiki/Encyclop%C3%A6dia_Britannica,_Inc.',
  'https://simple.wikipedia.org/wiki/Encyclop%C3%A6dia_Britannica,_Inc.',
  'https://simple.wikipedia.org/wiki/Encyclopaedia_Britannica,_Inc',
  'https://simple.wikipedia.org/wiki/Encyclopedia',
  'https://simple.wikipedia.org/wiki/English_language',
  'https://simple.wikipedia.org/wiki/Floral_emblem',
  'https://simple.wikipedia.org/wiki/Information',
  'https://simple.wikipedia.org/wiki/Information_technology',
  'https://simple.wikipedia.org/wiki/International_Standard_Book_Number',
  'https://simple.wikipedia.org/wiki/International_Standard_Book_Number',
  'https://simple.wikipedia.org/wiki/International_Standard_Book_Number',
  'https://simple.wikipedia.org/wiki/International_Standard_Book_Number',
  'https://simple.wikipedia.org/wiki/International_Standard_Book_Number',
  'https://simple.wikipedia.org/wiki/International_Standard_Book_Number',
  'https://simple.wikipedia.org/wiki/International_Standard_Book_Number',
  'https://simple.wikipedia.org/wiki/International_Standard_Book_Number',
  'https://simple.wikipedia.org/wiki/International_Standard_Book_Number',
  'https://simple.wikipedia.org/wiki/Knowledge',
  'https://simple.wikipedia.org/wiki/Library_of_Congress_Control_Number',
  'https://simple.wikipedia.org/wiki/Library_of_Congress_Control_Number',
  'https://simple.wikipedia.org/wiki/Library_of_Congress_Control_Number',
  'https://simple.wikipedia.org/wiki/Logo',
  'https://simple.wikipedia.org/wiki/Macrop%C3%A6dia',
  'https://simple.wikipedia.org/wiki/Macrop%C3%A6dia',
  'https://simple.wikipedia.org/wiki/Macropaedia',
  'https://simple.wikipedia.org/wiki/Main_Page',
  'https://simple.wikipedia.org/wiki/Main_Page',
  'https://simple.wikipedia.org/wiki/Microp%C3%A6dia',
  'https://simple.wikipedia.org/wiki/Microp%C3%A6dia',
  'https://simple.wikipedia.org/wiki/Micropaedia',
  'https://simple.wikipedia.org/wiki/Microsoft',
  'https://simple.wikipedia.org/wiki/Mobile_phone',
  'https://simple.wikipedia.org/wiki/Money',
  'https://simple.wikipedia.org/wiki/New_York_City',
  'https://simple.wikipedia.org/wiki/OCLC#Identifiers_and_linked_data',
  'https://simple.wikipedia.org/wiki/Page',
  'https://simple.wikipedia.org/wiki/Paper',
  'https://simple.wikipedia.org/wiki/Prop%C3%A6dia',
  'https://simple.wikipedia.org/wiki/Prop%C3%A6dia',
  'https://simple.wikipedia.org/wiki/Prop%C3%A6dia',
  'https://simple.wikipedia.org/wiki/Propaedia',
  'https://simple.wikipedia.org/wiki/Public_domain',
  'https://simple.wikipedia.org/wiki/Rosetta_Stone',
  'https://simple.wikipedia.org/wiki/Samuel_Taylor_Coleridge',
  'https://simple.wikipedia.org/wiki/Scotland',
  'https://simple.wikipedia.org/wiki/Scotland',
  'https://simple.wikipedia.org/wiki/Search_engine',
  'https://simple.wikipedia.org/wiki/Sears',
  'https://simple.wikipedia.org/wiki/Spin-off',
  'https://simple.wikipedia.org/wiki/Switzerland',
  'https://simple.wikipedia.org/wiki/Text_message',
  'https://simple.wikipedia.org/wiki/The_New_York_Times',
  'https://simple.wikipedia.org/wiki/U.S._state',
  'https://simple.wikipedia.org/wiki/United_States',
  'https://simple.wikipedia.org/wiki/United_States',
  'https://simple.wikipedia.org/wiki/Web_site',
  'https://simple.wikipedia.org/wiki/Wiki',
  'https://simple.wikipedia.org/wiki/Wikipedia',
  'https://simple.wikipedia.org/wiki/Wikipedia',
  'https://simple.wikipedia.org/wiki/Wikipedia',
  'https://simple.wikipedia.org/wiki/Wikisource',
  'https://simple.wikipedia.org/wiki/Wikisource',
  'https://simple.wikipedia.org/wiki/Wiktionary',
  'https://simple.wikipedia.org/wiki/World_Wide_Web'],
 'related_pages': ['1911 Encyclopædia Britannica',
  'Encyclopædia Britannica, Inc.',
  'Macropædia, Micropædia, Propædia'],
 'contents': [{'name': 'History',
   'children': [{'name': 'Editions',
     'children': [{'name': 'First era'},
      {'name': 'Second era'},
      {'name': 'Third era'}]}]},
  {'name': 'The Encyclopedia Now',
   'children': [{'name': '2007 print version'},
    {'name': 'Other Britannicas',
     'children': [{'name': 'Printed'}, {'name': 'Electronic'}]}]},
  {'name': 'Related pages'},
  {'name': 'Further reading'},
  {'name': 'References'},
  {'name': 'Other websites'}]}

In [6]:
```

### Примечание
Для парсинга нужно использовать библиотеки `BeautifulSoup` или `lxml`.
