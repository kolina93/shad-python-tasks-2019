import json

from pathlib import Path

import pytest

from .wiki_parser import WikipediaParser

TEST_CASES = [
    ('0.html', '0.a'),
    ('1.html', '1.a'),
]


@pytest.mark.parametrize('input_html,expected_output', TEST_CASES)
def test_wiki_parser(input_html: str, expected_output: str) -> None:
    current_directory = Path(__file__).parent
    with open(current_directory / input_html) as input_:
        html = input_.read()

    with open(current_directory / expected_output) as input_:
        expected = json.load(input_)

    parser = WikipediaParser()
    assert parser.parse(html) == expected
