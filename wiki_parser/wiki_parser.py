from typing import Dict, Any


class WikipediaParser:
    @classmethod
    def parse(cls, html: str) -> Dict[str, Any]:
        """
        Html parser for simple wikipedia pages.
        :param html: html content of page
        :return: dict with keys title, links, related_pages, contents
        """
        return {}
